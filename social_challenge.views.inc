<?php

/**
 * @file
 * Provide views data for social_challenge.module.
 *
 * @ingroup views_module_handlers
 */

/**
 * Implements hook_views_data().
 */
function social_challenge_views_data(): array {
  $data = [];

  $data['groups_field_data']['challenge_phases_status'] = [
    'title' => t('Challenge Status'),
    'group' => t('Social'),
    'filter' => [
      'title' => t('Challenge Status'),
      'help' => t('Filter challenges by phases ending date.'),
      'id' => 'challenge_phases_status',
    ],
  ];

  return $data;
}
