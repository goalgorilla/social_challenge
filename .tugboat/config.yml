services:
  # The main proxy that handles requests and routes them between Open Social and
  # other services. This uses Traefik as edge router which is popular
  # with Docker set-ups.
  proxy:
    image: tugboatqa/traefik:v2.4.8
    default: true
    depends:
      - webserver
    commands:
      init:
        - mkdir -p $HOME/.config/
        - ln -s $TUGBOAT_ROOT/.tugboat/traefik.yml $HOME/.config/traefik.yml
        - ln -s $TUGBOAT_ROOT/.tugboat/traefik-dynamic $HOME/traefik-dynamic

  # A service that runs our e2e tests.
  # Reporting is done using Cypress Dashboard.
  tests:
    image: kingdutch/tugboat-cypress:latest
    checkout: true
    depends:
      - proxy
    commands:
      # We only install the dependencies that may be needed in the cypress
      # tests but don't otherwise perform and build steps so that we test the
      # bundled version which hopefully alerts developers who forgot to build for production.
      init: |
        cd tests
        yarn install
      build: |
        cd tests
        yarn install
      online: |
        cd tests
        yarn cypress run --config baseUrl=$TUGBOAT_DEFAULT_SERVICE_URL

  webserver:
    image: kingdutch/social-docker-php:latest
    checkout: true
    depends:
      - db
    commands:
      init:
        - composer config --global github-protocols https
        # Set up authentication for private packagist.
        - composer config --global --auth http-basic.repo.packagist.com token $COMPOSER_AUTH_TOKEN
        # Undo the weird symlink in the default Tugboat image.
        - rm -rf $DOCROOT/html.original $DOCROOT/html
      # Update is run as single string rather than individual steps since
      # individual steps are executed in separate shell instances.
      update: |
        set -xe
        # Docroot for the PHP image points to /var/www/html so we set-up Open
        # Social one folder up so that our web files are in the html folder.
        cd $DOCROOT/..
        # Find the branch name regardless of preview type. This does fail for
        # non-Bitbucket PRs or branches until Tugboat provdes a better way.
        export BRANCH_NAME=$([ "$TUGBOAT_PREVIEW_TYPE" = "pullrequest" ] && echo "$TUGBOAT_BITBUCKET_SOURCE" || echo "$TUGBOAT_PREVIEW_REF")
        # Find the project name from the root composer.json file
        export EXTENSION_PROJECT_NAME=`cat "$TUGBOAT_ROOT/composer.json" | jq -r '.name'`
        export EXTENSION_MODULE_NAME=`echo $EXTENSION_PROJECT_NAME | cut -d "/" -f2`
        # Copy the composer.json test project file for this extension.
        cp $TUGBOAT_ROOT/tests/composer.json .
        # Install our preview version through private packagist from BitBucket.
        composer require $EXTENSION_PROJECT_NAME:dev-$BRANCH_NAME#$TUGBOAT_PREVIEW_SHA
        # Use the tugboat-specific Drupal settings.
        cat $DOCROOT/sites/default/default.settings.php | \
          sed -z "s@#\n# if (file_exists(\$app_root . '/' . \$site_path . '/settings.local.php')) {\n#   include \$app_root . '/' . \$site_path . '/settings.local.php';\n# }@if (file_exists(__DIR__ . '/settings.local.php')) {\n  include __DIR__ . '/settings.local.php';\n}@" \
          > $DOCROOT/sites/default/settings.php
        cp "${TUGBOAT_ROOT}/.tugboat/settings.local.php" "${DOCROOT}/sites/default/"
        mkdir -p ${DOCROOT}/sites/default/files && chmod -R 755 ${DOCROOT}/sites/default/files
        chown -R www-data:www-data $DOCROOT
        # Install Open Social.
        cd html
        ../vendor/bin/drush \
          --yes \
          --site-name="Open Social QA" \
          --account-pass=${ADMIN_PASSWORD} \
          site:install social
        # dblog is enabled first so we can easily see errors that occur while
        # enabling other modules or while running tests.
        ../vendor/bin/drush en -y dblog
        ../vendor/bin/drush en -y $EXTENSION_MODULE_NAME social_test_helper
      build: |
        set -xe
        cd $DOCROOT/..
        export BRANCH_NAME=$([ "$TUGBOAT_PREVIEW_TYPE" = "pullrequest" ] && echo "$TUGBOAT_BITBUCKET_SOURCE" || echo "$TUGBOAT_PREVIEW_REF")
        export EXTENSION_PROJECT_NAME=`cat "$TUGBOAT_ROOT/composer.json" | jq -r '.name'`
        export EXTENSION_MODULE_NAME=`echo $EXTENSION_PROJECT_NAME | cut -d "/" -f2`
        export TEST_DEPENDENCIES="`jq -r '.require | to_entries | map(.key + ":" + .value) | .[]' $TUGBOAT_ROOT/tests/composer.json`"
        cp $TUGBOAT_ROOT/tests/composer.json .

        echo $TEST_DEPENDENCIES | xargs -- \
          composer require \
            --update-with-all-dependencies \
            --optimize-autoloader \
            $EXTENSION_PROJECT_NAME:dev-$BRANCH_NAME#$TUGBOAT_PREVIEW_SHA

        chown -R www-data:www-data $DOCROOT
        cd html && ../vendor/bin/drush updatedb -y
  # A database for storage of Open Social data.
  db:
    image: tugboatqa/mariadb:latest
