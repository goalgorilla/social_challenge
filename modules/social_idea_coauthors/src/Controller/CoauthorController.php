<?php

namespace Drupal\social_idea_coauthors\Controller;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Link;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\private_message\Entity\PrivateMessage;
use Drupal\private_message\Service\PrivateMessageServiceInterface;
use Drupal\user\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Coauthor controller.
 */
class CoauthorController extends ControllerBase {

  /**
   * Private message.
   */
  protected PrivateMessageServiceInterface $privateMessage;

  /**
   * State.
   */
  protected StateInterface $state;

  /**
   * {@inheritdoc}
   */
  public function __construct(PrivateMessageServiceInterface $private_message, StateInterface $state) {
    $this->privateMessage = $private_message;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('private_message.service'),
      $container->get('state')
    );
  }

  /**
   * Request co-authorship.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The idea I want to become a co-author for.
   * @param \Drupal\user\Entity\User $user
   *   The user that wants to become a coathor.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Bye.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function request(Node $node, User $user): RedirectResponse {
    // First check if the node is an idea and the user a user.
    if (!$node instanceof Node || !$user instanceof User) {
      // Send the message to the messenger, using the messengerTrait.
      $this->messenger()->addMessage($this->t('Please use a valid idea and user for this operation'));
      // And return to the home page.
      return new RedirectResponse('/');
    }

    $node_url = $node->toUrl('canonical')->toString();

    // You can only request it for yourself.
    if ($this->currentUser()->id() !== $user->id()) {
      // Send the message to the messenger, using the messengerTrait.
      $this->messenger()->addMessage($this->t('You cannot request co-authorship for another user'));
      // And return to the about page.
      return new RedirectResponse($node_url);
    }

    // Send private message from requester to idea owner.
    $this->sendMessage($node, $user, $node->getOwner(), 'request');

    // Invalidate node cache.
    Cache::invalidateTags(['node:' . $node->id()]);

    // Store the request has been made, so we can check for it in the block.
    $this->state->set('node/' . $node->id() . '/user/' . $this->currentUser()->id(), 'pending');
    // Show confirmation message to the user.
    $this->messenger()->addMessage($this->t('Your request has been sent to the idea author as a private message.'));
    // And return to the about page.
    return new RedirectResponse($node_url);

  }

  /**
   * Generate private message.
   */
  protected function getMessage(Node $node, User $sender, User $receiver, string $type): ?TranslatableMarkup {
    $message = NULL;
    // Create links.
    $node_url = Url::fromUserInput("/node/{$node->id()}/edit", ['absolute' => TRUE]);
    $node_link = Link::fromTextAndUrl($node->getTitle(), $node_url)->toString();

    switch ($type) {
      case 'request':
        $accept_url = Url::fromRoute('social_idea_coauthors.accept_coauthorship', [
          'user' => $sender->id(),
          'node' => $node->id(),
        ], ['absolute' => TRUE]);
        $accept_link = Link::fromTextAndUrl($this->t('Accept'), $accept_url)->toString();

        $decline_url = Url::fromRoute('social_idea_coauthors.decline_coauthorship', [
          'user' => $sender->id(),
          'node' => $node->id(),
        ], ['absolute' => TRUE]);
        $decline_link = Link::fromTextAndUrl($this->t('Decline'), $decline_url)->toString();

        $message = $this->t("Hello :idea_owner, <br><br> I would like to contribute to the idea @node_link. Please let me know if it's OK. <br><br> @accept_link | @decline_link",
          [
            ':idea_owner' => $receiver->getDisplayName(),
            '@node_link' => $node_link,
            '@accept_link' => $accept_link,
            '@decline_link' => $decline_link,
          ]);
        break;

      case 'accept':
        $message = $this->t("Hello :requester, <br><br> Thank you for your interest. You have been added as a coauthor of the idea @node_link. <br><br> Look forward to collaborating with you.",
          [
            ':requester' => $receiver->getDisplayName(),
            '@node_link' => $node_link,
          ]);
        break;

      case 'decline':
        $message = $this->t("Hello :requester, <br><br> Thank you for your interest. Unfortunately, your request is declined. <br><br> Please feel free to participate by voting for this idea or leave your comments.",
          [
            ':requester' => $receiver->getDisplayName(),
          ]);
        break;
    }
    return $message;
  }

  /**
   * Send private message.
   */
  protected function sendMessage(Node $node, User $sender, User $receiver, string $type): RedirectResponse {
    $recipient[] = $receiver;
    $recipient[] = $sender;

    // Create a pm thread between these users.
    $thread = $this->privateMessage->getThreadForMembers($recipient);
    // Create a single message with the request.
    $private_message = PrivateMessage::create([
      'owner' => $sender->id(),
      'message' => [
        'value' => $this->getMessage($node, $sender, $receiver, $type),
        'format' => 'basic_html',
      ],
    ]);

    $node_url = $node->toUrl()->toString();

    try {
      $private_message->save();
    }
    catch (EntityStorageException $e) {
      $this->messenger()->addMessage($e->getMessage());
      return new RedirectResponse($node_url);
    }

    // Try to add the message to the thread.
    try {
      $thread->addMessage($private_message)->save();
    }
    catch (EntityStorageException $e) {
      $this->messenger()->addMessage($e->getMessage());
      return new RedirectResponse($node_url);
    }

    return new RedirectResponse('/');
  }

  /**
   * Accept co-authorship.
   */
  public function accept(Node $node, User $user): RedirectResponse {
    // First check if the node is a Node and the user a User.
    if (!$node instanceof Node || !$user instanceof User) {
      $this->messenger()->addMessage($this->t('Please use a valid idea and user for this operation'));
      // Return to the home page if operation is not possible.
      return new RedirectResponse('/');
    }

    $node_url = $node->toUrl('canonical')->toString();

    $coauthors = $node->get('field_social_idea_coauthors')->getValue();
    foreach ($coauthors as $co_author) {
      // Redirect to idea if user is already coauthor.
      if ($co_author['target_id'] === $user->id()) {
        return new RedirectResponse($node_url);
      }
    }

    if ($this->state->get('node/' . $node->id() . '/user/' . $user->id()) === 'pending') {
      // Add user to coauthors.
      $coauthors[] = ['target_id' => $user->id()];
      $node->set('field_social_idea_coauthors', $coauthors);
      $node->save();
      // Send private message from idea owner to requester.
      $this->sendMessage($node, $node->getOwner(), $user, 'accept');
      // Show status message for idea owner.
      $this->messenger()->addMessage($this->t('Co-authorship request was accepted. Your response has been sent to the requester as a private message.'));
    }

    // Return to the idea page.
    return new RedirectResponse($node_url);
  }

  /**
   * Decline co-authorship.
   *
   * @param \Drupal\node\Entity\Node $node
   *   The idea I want to become a co-author for.
   * @param \Drupal\user\Entity\User $user
   *   The user that wants to become a coathor.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Bye.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   */
  public function decline(Node $node, User $user): RedirectResponse {
    // First check if the node is a Node and the user a User.
    if (!$node instanceof Node || !$user instanceof User) {
      $this->messenger()->addMessage($this->t('Please use a valid idea and user for this operation'));
      // Return to the home page if operation is not possible.
      return new RedirectResponse('/');
    }

    if ($this->state->get('node/' . $node->id() . '/user/' . $user->id()) === 'pending') {
      // Delete pending request, so user can send it again.
      $this->state->delete('node/' . $node->id() . '/user/' . $user->id());
      // Send private message from idea owner to requester.
      $this->sendMessage($node, $node->getOwner(), $user, 'decline');
      // Show status message for idea owner.
      $this->messenger()->addMessage($this->t('Co-authorship request was declined. Your response has been sent to the requester as a private message.'));
    }

    // Return to the idea page.
    $node_url = $node->toUrl('canonical')->toString();
    return new RedirectResponse($node_url);
  }

}
