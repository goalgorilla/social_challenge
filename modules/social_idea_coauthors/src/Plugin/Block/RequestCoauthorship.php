<?php

namespace Drupal\social_idea_coauthors\Plugin\Block;

use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Link;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Routing\CurrentRouteMatch;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'Coauthors' block.
 *
 * @Block(
 *  id = "request_coauthorship",
 *  admin_label = @Translation("Request co-authorship of an idea"),
 * )
 */
class RequestCoauthorship extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * Current user.
   */
  protected AccountProxyInterface $currentUser;

  /**
   * The module handler.
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Routmatch.
   */
  protected CurrentRouteMatch $routeMatch;

  /**
   * State.
   */
  protected StateInterface $state;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, AccountProxyInterface $current_user, CurrentRouteMatch $route_mantch, StateInterface $state, ModuleHandlerInterface $module_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->currentUser = $current_user;
    $this->moduleHandler = $module_handler;
    $this->routeMatch = $route_mantch;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user'),
      $container->get('current_route_match'),
      $container->get('state'),
      $container->get('module_handler')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account): CacheableDependencyInterface {
    // Check if the node parameter is in the URL.
    $node = $this->routeMatch->getParameter('node');

    // If not try to load it from gid.
    if (!$node instanceof Node) {
      return AccessResult::forbidden();
    }
    // Must be an idea.
    if ($node->getType() !== 'social_idea') {
      return AccessResult::forbidden();
    }

    // Must have the option turned on.
    if (!$node->get('field_social_idea_request')->value === TRUE) {
      return AccessResult::forbidden();
    }

    // Not for the owner.
    if ($node->getOwnerId() === $account->id()) {
      return AccessResult::forbidden();
    }

    // Not for existing co-authors.
    foreach ($node->get('field_social_idea_coauthors')->getValue() as $coauthor) {
      if (isset($coauthor['target_id']) && $this->currentUser->id() == $coauthor['target_id']) {
        return AccessResult::forbidden();
      }
    }

    // Otherwise we'll never show this block.
    return AccessResult::allowed();
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build = [];

    // Do not display the button if the private_message module is disabled,
    // otherwise pressing the button causes an error.
    if (!$this->moduleHandler->moduleExists('private_message')) {
      return $build;
    }

    // Check if the group parameter is in the URL.
    $node = $this->routeMatch->getParameter('node');
    // Clear the state cache, since it has it's own.
    $this->state->resetCache();
    // Check if the current user already made a request.
    $status = $this->state->get('node/' . $node->id() . '/user/' . $this->currentUser->id());

    $url = Url::fromRoute('social_idea_coauthors.request_coauthorship', [
      'user' => $this->currentUser->id(),
      'node' => $node->id(),
    ]);

    // Title can change.
    $title = $this->t('I want to contribute');
    if ($status == 'pending') {
      $title = $this->t('Request sent');
    }

    if ($this->currentUser->isAnonymous()) {
      $url = Url::fromRoute('user.login', ['destination' => 'node/' . $node->id()]);
    }

    $link_options = [
      'attributes' => [
        'class' => [
          'btn',
          'btn-primary',
          'btn-raised',
          'brand-bg-primary',
        ],
        'title' => $title,
      ],
    ];

    if (theme_get_setting('style') === 'sky') {
      $link_options['attributes']['class'] = [
        'btn',
        'btn-default',
        'btn-sm',
      ];
    }

    // Classes can change.
    if ($status == 'pending') {
      $link_options['attributes']['class'][] = 'disabled';
    }

    $url->setOptions($link_options);
    // Add content.
    $build['content'] = Link::fromTextAndUrl($title, $url)->toRenderable();
    // Add cachetags.
    $build['#cache']['tags'][] = 'node:' . $node->id();
    // Disable caching to fix issue FOR-169.
    $build['#cache']['max-age'] = 0;

    return $build;
  }

}
