<?php

namespace Drupal\social_idea_coauthors\Plugin\QueueWorker;

use Drupal\activity_creator\Plugin\ActivityActionManager;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Suggestion worker.
 *
 * @QueueWorker(
 *   id = "social_idea_coauthors.updates",
 *   title = @Translation("Social Ideas Coauthors Updates queue worker"),
 *   cron = {"time" = 30}
 * )
 */
class UpdatesQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The time service.
   */
  protected TimeInterface $time;

  /**
   * The activity action manager.
   */
  protected ActivityActionManager $activityActionManager;

  /**
   * UpdatesQueue constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity manager service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\activity_creator\Plugin\ActivityActionManager $activity_action_manager
   *   The activity action manager.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    TimeInterface $time,
    ActivityActionManager $activity_action_manager
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->time = $time;
    $this->activityActionManager = $activity_action_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('datetime.time'),
      $container->get('plugin.manager.activity_action.processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    // Get node from updates.
    $entity = $this->entityTypeManager->getStorage('node')->load($data['nid']);
    if ($entity instanceof NodeInterface) {
      $entity->setCreatedTime($this->time->getRequestTime());
      // Get user from updates.
      $user = $this->entityTypeManager->getStorage('user')->load($data['uid']);
      if ($user instanceof UserInterface) {
        $entity->setOwner($user);
        // Create new activity.
        // Create new activity.
        /** @var \Drupal\activity_creator\Plugin\ActivityActionInterface $plugin */
        $plugin = $this->activityActionManager->createInstance('update_entity_action');
        $plugin->create($entity);
      }
    }
  }

}
