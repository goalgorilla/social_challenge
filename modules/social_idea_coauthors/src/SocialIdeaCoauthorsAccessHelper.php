<?php

namespace Drupal\social_idea_coauthors;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\node\NodeInterface;

/**
 * Helper class for checking update access on idea coauthors nodes.
 */
class SocialIdeaCoauthorsAccessHelper {

  /**
   * NodeAccessCheck for given operation, node and user account.
   */
  public static function nodeAccessCheck(NodeInterface $node, string $op, AccountInterface $account): int {
    if ($op === 'update') {
      // Only for ideas.
      // Only continue if the user has access to view the idea.
      if (($node->getType() === 'social_idea') && $node->access('view', $account)) {
        // Always allow for owner.
        if ($account->id() === $node->getOwnerId()) {
          return 2;
        }

        $idea_coauthors = $node->get('field_social_idea_coauthors')
          ->getValue();
        foreach ($idea_coauthors as $idea_author) {
          if ($account->id() === $idea_author['target_id']) {
            return 2;
          }
        }

        // No hits, so we assume the user is not an idea coauthor.
        return 1;
      }
    }
    return 0;
  }

  /**
   * Gets the Entity access for the given node.
   */
  public static function getEntityAccessResult(NodeInterface $node, string $op, AccountInterface $account): AccessResultInterface {
    $access = self::nodeAccessCheck($node, $op, $account);

    switch ($access) {
      case 2:
        return AccessResult::allowed()->cachePerPermissions()->addCacheableDependency($node);

      case 1:
        return AccessResult::forbidden();
    }

    return AccessResult::neutral();
  }

}
