<?php

namespace Drupal\social_challenge_phase\Event;

/**
 * Defines events for the Social Challenge Phase module.
 */
final class SocialChallengePhaseEvents {

  /**
   * Name of the event fired after a Phase was ended.
   *
   * This event allows modules to react on the fact that a Phase was ended.
   *
   * @Event
   *
   * @see \Drupal\social_challenge_phase\Event\SocialChallengePhaseEndingEvent
   *
   * @var string
   */
  const COMPLETED = 'social_challenge_phase.phase.completed';

}
