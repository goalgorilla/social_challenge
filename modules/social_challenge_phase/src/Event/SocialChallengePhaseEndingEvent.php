<?php

namespace Drupal\social_challenge_phase\Event;

use Drupal\social_challenge_phase\PhaseInterface;
use Symfony\Contracts\EventDispatcher\Event;

/**
 * Define events for Phase entity.
 */
class SocialChallengePhaseEndingEvent extends Event {

  /**
   * A finished Phase entity.
   */
  protected PhaseInterface $phase;

  /**
   * SocialChallengePhaseEndingEvent constructor.
   *
   * @param \Drupal\social_challenge_phase\PhaseInterface $phase
   *   The Phase entity.
   */
  public function __construct(PhaseInterface $phase) {
    $this->phase = $phase;
  }

  /**
   * Returns parameters from event.
   *
   * @return \Drupal\social_challenge_phase\PhaseInterface
   *   The parameters.
   */
  public function getPhase(): PhaseInterface {
    return $this->phase;
  }

}
