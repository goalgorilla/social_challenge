<?php

namespace Drupal\social_challenge_phase;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Phase entity.
 *
 * We have this interface so, we can join the other interfaces it extends.
 *
 * @ingroup social_challenge_phase
 */
interface PhaseInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Check if phase is going to past.
   *
   * @param int $interval
   *   Interval.
   *
   * @return bool
   *   Returns TRUE if phase is going to past in interval.
   */
  public function isGoingToPast($interval);

  /**
   * Flag if users should receive the statistics about ended phase.
   *
   * @return bool
   *   Status flag.
   */
  public function shouldSendStatisticsToUsers(): bool;

}
