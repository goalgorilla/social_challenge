<?php

namespace Drupal\social_challenge_phase\Entity;

use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\social_challenge_phase\PhaseInterface;
use Drupal\user\UserInterface;
use Drupal\Core\Entity\EntityChangedTrait;

/**
 * Defines the Phase entity.
 *
 * @ingroup social_challenge_phase
 *
 * @ContentEntityType(
 *   id = "phase",
 *   label = @Translation("Phase"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\social_challenge_phase\Entity\Controller\PhaseListBuilder",
 *     "views_data" = "Drupal\social_challenge_phase\Entity\PhaseViewsData",
 *     "form" = {
 *       "add" = "Drupal\social_challenge_phase\Form\PhaseForm",
 *       "edit" = "Drupal\social_challenge_phase\Form\PhaseForm",
 *       "delete" = "Drupal\social_challenge_phase\Form\PhaseDeleteForm",
 *     },
 *     "access" = "Drupal\social_challenge_phase\PhaseAccessControlHandler",
 *     "translation" = "Drupal\content_translation\ContentTranslationHandler",
 *   },
 *   base_table = "phase",
 *   data_table = "phase_field_data",
 *   translatable = TRUE,
 *   admin_permission = "administer phase entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "uuid" = "uuid",
 *     "langcode" = "langcode"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/phase/{phase}",
 *     "edit-form" = "/admin/structure/phase/{phase}/edit",
 *     "delete-form" = "/admin/structure/phase/{phase}/delete",
 *     "collection" = "/admin/structure/phase/list"
 *   },
 *   field_ui_base_route = "social_challenge_phase.settings",
 * )
 */
class Phase extends ContentEntityBase implements PhaseInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   *
   * When a new entity instance is added, set the user_id entity reference to
   * the current user as the creator of the instance.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values): void {
    parent::preCreate($storage_controller, $values);
    $values += [
      'user_id' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): string {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getChangedTime(): int {
    return (int) $this->get('changed')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner(): UserInterface {
    return $this->get('user_id')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId(): ?int {
    return $this->get('user_id')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid): self {
    $this->set('user_id', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account): self {
    $this->set('user_id', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   *
   * Define the field properties here.
   *
   * Field name, type and size determine the table structure.
   *
   * In addition, we can define how the field and its content can be manipulated
   * in the GUI. The behaviour of the widgets used can be determined here.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    // Standard field, used as unique if primary index.
    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Phase entity.'))
      ->setReadOnly(TRUE);

    // Standard field, unique outside of the scope of the current project.
    $fields['uuid'] = BaseFieldDefinition::create('uuid')
      ->setLabel(t('UUID'))
      ->setDescription(t('The UUID of the Phase entity.'))
      ->setReadOnly(TRUE);

    // Owner field of the phase.
    // Entity reference field, holds the reference to the user object.
    // The view shows the user name field of the user.
    // The form presents a auto complete field for the user name.
    $fields['user_id'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('User Name'))
      ->setDescription(t('The Name of the associated user.'))
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => -3,
      ])
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => -3,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Publishing status'))
      ->setDescription(t('A boolean indicating whether the Phase is published.'))
      ->setDefaultValue(TRUE);

    $fields['langcode'] = BaseFieldDefinition::create('language')
      ->setLabel(t('Language code'))
      ->setDescription(t('The language code of Phase entity.'))
      ->setDisplayOptions('form', [
        'type' => 'language_select',
        'weight' => 2,
      ])
      ->setTranslatable(TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created'))
      ->setDescription(t('The time that the entity was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the entity was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    /** @var \Drupal\taxonomy\TermInterface $term_title */
    $term_title = $this->get('field_phase_title')->entity;
    if ($term_title->hasTranslation($langcode)) {
      /** @var \Drupal\taxonomy\TermInterface $term_title_tr */
      $term_title_tr = $term_title->getTranslation($langcode);
      return $term_title_tr->getName();
    }
    return $term_title->getName();
  }

  /**
   * Check if phase is active.
   *
   *   Returns TRUE if current date is between start and end date.
   */
  public function isActive(): bool {
    $start_date = $this->get('field_phase_date')->start_date->getTimestamp();
    $end_date = $this->get('field_phase_date')->end_date->getTimestamp();
    $current_date = \Drupal::time()->getRequestTime();

    return $start_date < $current_date && $current_date < $end_date;
  }

  /**
   * Check if phase is past.
   *
   *   Returns TRUE if current date after end date.
   */
  public function isPast(): bool {
    $end_date = $this->get('field_phase_date')->end_date->getTimestamp();
    $current_date = \Drupal::time()->getRequestTime();

    return $current_date > $end_date;
  }

  /**
   * {@inheritdoc}
   */
  public function isGoingToPast($interval): bool {
    if ($this->isPast()) {
      return FALSE;
    }

    $end_date = $this->get('field_phase_date')->end_date->getTimestamp();
    $current_date = \Drupal::time()->getRequestTime();

    return $end_date - $current_date < $interval;
  }

  /**
   * Check if phase is active recently.
   *
   * @return bool
   *   Returns TRUE if current phase is active recently (less than 2 hours).
   */
  public function isActiveRecently($interval): bool {
    if ($this->isPast()) {
      return FALSE;
    }

    $start_date = $this->get('field_phase_date')->start_date->getTimestamp();
    $current_date = \Drupal::time()->getRequestTime();
    $from_start_to_now = $current_date - $start_date;
    // Check if phase is active recently, but not too long,
    // to avoid sending notifications for existing active  phases.
    return ($from_start_to_now > $interval) && ($from_start_to_now < 7200);
  }

  /**
   * {@inheritdoc}
   */
  public function shouldSendStatisticsToUsers(): bool {
    return $this->hasField('field_phase_stat_send_status')
      && $this->get('field_phase_stat_send_status')->value;
  }

}
