<?php

namespace Drupal\social_challenge_phase\Entity;

use Drupal\views\EntityViewsData;
use Drupal\views\EntityViewsDataInterface;

/**
 * Provides Views data for Event enrollment entities.
 */
class PhaseViewsData extends EntityViewsData implements EntityViewsDataInterface {

}
