<?php

namespace Drupal\social_challenge_phase;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a helper class, sets permissions.
 */
class SocialChallengePhaseInstallHelper implements ContainerInjectionInterface {

  /**
   * Entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs SocialChallengePhaseInstallHelper.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Set default permissions.
   */
  public function setPermissions(): void {
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();

    /** @var \Drupal\user\Entity\Role $role */
    foreach ($roles as $role) {
      if ($role->id() === 'administrator') {
        continue;
      }

      $permissions = $this->getPermissions((string) $role->id());
      user_role_grant_permissions($role->id(), $permissions);
    }
  }

  /**
   * Get default permissions.
   */
  public function getPermissions(string $role): array {
    // Anonymous.
    $permissions[RoleInterface::ANONYMOUS_ID] = [
      'view phase entity',
      'view terms in phase_title',
    ];

    // Authenticated.
    $permissions[RoleInterface::AUTHENTICATED_ID] = array_merge($permissions[RoleInterface::ANONYMOUS_ID], [
      'add phase entity',
      'edit phase entity',
      'delete phase entity',
    ]);

    // Content manager.
    $permissions['contentmanager'] = array_merge($permissions[RoleInterface::AUTHENTICATED_ID], []);

    // Site manager.
    $permissions['sitemanager'] = array_merge($permissions['contentmanager'], [
      'administer phase entity',
    ]);

    return $permissions[$role] ?? [];
  }

}
