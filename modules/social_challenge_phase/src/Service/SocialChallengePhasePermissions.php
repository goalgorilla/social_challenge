<?php

namespace Drupal\social_challenge_phase\Service;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\social_challenge_phase\Entity\Phase;
use Drupal\social_challenge_phase\PhaseInterface;
use Drupal\user\Entity\Role;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Social challenge phase permissions service.
 *
 * @package Drupal\social_challenge\Services
 */
class SocialChallengePhasePermissions {

  /**
   * Current user.
   */
  protected AccountInterface $currentUser;

  /**
   * Entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * PhasePermissions constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(AccountInterface $account, EntityTypeManagerInterface $entity_type_manager) {
    $this->currentUser = $account;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('current_user'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Checks if a certain phase has a permission set.
   *
   * @param string $permission
   *   The permissions name.
   * @param \Drupal\social_challenge_phase\Entity\Phase $phase
   *   The phase you're trying to get the permission for.
   *
   * @return bool
   *   Wether it's set of not.
   */
  public function hasPermissionValue(string $permission, Phase $phase): bool {
    // Define the exact permission to search for.
    $permString = $permission . ' ' . $phase->id();
    // Load the authenticated user role.
    $role = $this->entityTypeManager->getStorage('user_role')->load('authenticated');

    // If the role exists return if it has access.
    if ($role instanceof Role) {
      return $role->hasPermission($permString);
    }

    // Otherwise we're going to check based on the phaseOwner.
    return $phase->getOwner()->hasPermission($permString);
  }

  /**
   * Sets and saves the permission.
   *
   * @param string $permission
   *   The permissions name.
   * @param \Drupal\social_challenge_phase\Entity\Phase $phase
   *   The phase you're trying to set the permission for.
   * @param mixed $permissionValue
   *   The value of the permission.
   */
  public function setPermissionValue(string $permission, Phase $phase, $permissionValue): void {
    // Define the exact permission to search for.
    $permString = $permission . ' ' . $phase->id();

    /** @var \Drupal\user\Entity\Role $role */
    foreach ($this->entityTypeManager->getStorage('user_role')->loadMultiple() as $role) {
      if ($role->id() === 'anonymous') {
        continue;
      }
      // Set 'n save the permission.
      if ($permissionValue === TRUE) {
        $role->grantPermission($permString)->save();
      }
      else {
        $role->revokePermission($permString)->save();
      }
    }
  }

  /**
   * Static function to return the defined group permissions.
   *
   *   An array of permissions.
   */
  public function getDefinedPermissions(): array {
    return [
      'comment in phase' => [
        'permission' => 'comment in phase',
        'description' => 'Comment on ideas',
      ],
      'vote in phase' => [
        'permission' => 'vote in phase',
        'description' => 'Vote on ideas',
      ],
      'create idea in phase' => [
        'permission' => 'create idea in phase',
        'description' => 'Create ideas',
      ],
      'edit idea in phase' => [
        'permission' => 'edit idea in phase',
        'description' => 'Edit ideas',
      ],
      'view idea in phase' => [
        'permission' => 'view idea in phase',
        'description' => 'View ideas',
      ],
    ];
  }

  /**
   * Generate a dynamic permission name.
   */
  public function getPermissionName(string $permission, Phase $phase): string {
    return $permission . ' ' . $phase->id();
  }

  /**
   * Get permissions allowed by current Phase.
   *
   * @param \Drupal\social_challenge_phase\PhaseInterface $phase
   *   The phase your generating the permission name for.
   * @param bool $human_readable
   *   Flag to indicate what should be return - permissions ids or descriptions.
   *
   * @return string[]
   *   An array of the permissions.
   */
  public function getPhaseAllowedPermissions(PhaseInterface $phase, bool $human_readable = TRUE): array {
    foreach ($this->getDefinedPermissions() as $perm_array) {
      // The permission field name in the entity.
      $perm_name_field = str_replace(' ', '_', $perm_array['permission']);
      // Get the permission value from the entity.
      if (!$phase->hasField('field_phase_' . $perm_name_field)) {
        continue;
      }

      $permission_allowed = $phase->get('field_phase_' . $perm_name_field)->value;
      if ($permission_allowed) {
        $permissions[] = $human_readable
          ? $perm_array['description']
          : $perm_array['permission'];
      }
    }

    return $permissions ?? [];
  }

}
