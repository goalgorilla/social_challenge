<?php

namespace Drupal\social_challenge_phase\Service;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\node\NodeStorageInterface;
use Drupal\social_challenge_phase\PhaseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Social challenge phase advancing service.
 *
 * @package Drupal\social_challenge\Services
 */
class SocialChallengePhaseAdvancing implements SocialChallengePhaseAdvancingInterface, ContainerInjectionInterface {

  /**
   * Entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The node storage.
   */
  protected NodeStorageInterface $nodeStorage;

  /**
   * SocialChallengePhaseAdvancing constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
    $this->nodeStorage = $entity_type_manager->getStorage('node');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function advanceIdea(NodeInterface $idea, $currentPhase = NULL): bool {
    // Check if $node is Social idea.
    if ($idea->bundle() !== 'social_idea') {
      return FALSE;
    }

    // Try to get current phase from idea if it's not added.
    if (empty($currentPhase) || !$currentPhase instanceof PhaseInterface) {
      $currentPhase = $this->getCurrentPhase($idea);
    }

    // If current phase is still unset than return FALSE.
    if (empty($currentPhase) || !$currentPhase instanceof PhaseInterface) {
      return FALSE;
    }

    // Get the next phase.
    $nextPhase = $this->getNextPhase($currentPhase);
    if (empty($nextPhase) || !$nextPhase instanceof PhaseInterface) {
      return FALSE;
    }

    // Check if idea is already there.
    if ($this->hasPhase($idea, $nextPhase)) {
      return FALSE;
    }

    // Add phase to idea.
    $idea->field_social_idea_phases[] = $nextPhase->id();
    $idea->save();

    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getNextPhase(PhaseInterface $phase): ?PhaseInterface {
    $challenges = $this->entityTypeManager
      ->getStorage('group')
      ->loadByProperties([
        'field_challenge_phases' => $phase->id(),
      ]);

    if (empty($challenges)) {
      return NULL;
    }

    /** @var \Drupal\group\Entity\GroupInterface $challenge */
    // Expects that the phase can't be in more than one challenge.
    $challenge = array_shift($challenges);

    $all_phases = $challenge->get('field_challenge_phases');

    // It doesn't need to continue if challenge has only one phase.
    if (is_null($all_phases) || $all_phases->count() == 1) {
      return NULL;
    }

    // Get the array of phases date_start values.
    $phases_date_start = [];
    $phase_entities = $all_phases->referencedEntities();
    foreach ($phase_entities as $key => $phase_entity) {
      $phases_date_start[$phase_entity->id()] = $phase_entity->get('field_phase_date')->start_date->getTimestamp();

      $phase_entities['id:' . $phase_entity->id()] = $phase_entity;
      unset($phase_entities[$key]);
    }

    // Get the array of diffs between investigated phases date_start
    // and given phase date_end.
    $end_date = $phase->get('field_phase_date')->end_date->getTimestamp();
    $phases_date_diff = [];
    foreach ($phases_date_start as $phase_id => $phase_date_start) {
      // Skip if investigated phase is going before given phase.
      if ($phase_date_start < $end_date) {
        continue;
      }

      $phases_date_diff[$phase_id] = $phase_date_start - $end_date;
    }

    if (empty($phases_date_diff)) {
      return NULL;
    }

    // Get the nearest phase to given one.
    $nearest_phase_id = array_keys($phases_date_diff, min($phases_date_diff))[0];

    return $phase_entities['id:' . $nearest_phase_id] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getPromotingCriteria(PhaseInterface $phase): string {
    // Automatic promoting isn't allowed.
    if (!$phase->get('field_phase_auto_advance_ideas')->getString()) {
      return '';
    }

    return $phase->get('field_phase_auto_advance_source')->getString();
  }

  /**
   * {@inheritdoc}
   */
  public function getPromotingCategory(PhaseInterface $phase): string {
    // Automatic promoting isn't allowed.
    if (!$phase->get('field_phase_auto_advance_ideas')->getString()) {
      return '';
    }

    // If field is empty then we make "all" as a default value.
    return !$phase->get('field_phase_auto_advance_cat')->isEmpty()
      ? $phase->get('field_phase_auto_advance_cat')->getString()
      : 'all';
  }

  /**
   * {@inheritdoc}
   */
  public function getPromotedIdeas(PhaseInterface $phase): array {
    // Automatic promoting isn't allowed.
    if (!$phase->get('field_phase_auto_advance_ideas')->getString()) {
      return [];
    }

    // Get all ideas from the given phase.
    /** @var \Drupal\node\NodeInterface[] $ideas */
    $ideas = $this->nodeStorage->loadByProperties([
      'type' => 'social_idea',
      'status' => TRUE,
      'field_social_idea_phases' => $phase->id(),
    ]);

    $auto_promoting_category = $this->getPromotingCategory($phase);

    // All ideas are promoted to next phase.
    if ($auto_promoting_category === 'all') {
      return $ideas;
    }

    // If some parameters are missed than advance all ideas.
    if (
      $phase->get('field_phase_auto_advance_count')->isEmpty() ||
      $phase->get('field_phase_auto_advance_param')->isEmpty() ||
      $phase->get('field_phase_auto_advance_source')->isEmpty()
    ) {
      return $ideas;
    }

    $auto_advance_source = $this->getPromotingCriteria($phase);

    // Sort ideas by count of source.
    usort($ideas, static function (NodeInterface $idea_1, NodeInterface $idea_2) use ($auto_advance_source) {
      switch ($auto_advance_source) {
        case 'votes_number':
          $a = social_vote_is_enabled($idea_1) ? social_vote_get_votes($idea_1) : 0;
          $b = social_vote_is_enabled($idea_2) ? social_vote_get_votes($idea_2) : 0;
          break;

        case 'comments_number':
          $a = $idea_1->get('field_social_idea_comments')->comment_count;
          $b = $idea_2->get('field_social_idea_comments')->comment_count;
          break;

        case 'contributors_number':
          $a = $idea_1->get('field_social_idea_coauthors')->count();
          $b = $idea_2->get('field_social_idea_coauthors')->count();
          break;

        default:
          $a = 0;
          $b = 0;
      }

      return $b <=> $a;
    });

    $auto_advance_count = $phase->get('field_phase_auto_advance_count')->value;
    $auto_advance_param = $phase->get('field_phase_auto_advance_param')->value;

    $get_places = (int) ($auto_advance_param === 'percents') ? ceil($auto_advance_count * count($ideas) / 100) : $auto_advance_count;

    // Proceed only with top ideas.
    return array_slice($ideas, 0, $get_places);
  }

  /**
   * Get the current phase.
   *
   * @param \Drupal\node\NodeInterface $idea
   *   Idea node.
   *
   * @return \Drupal\social_challenge_phase\PhaseInterface|null
   *   Current phase.
   */
  private function getCurrentPhase(NodeInterface $idea): ?PhaseInterface {
    if (!isset($idea->field_social_idea_phases)) {
      return NULL;
    }

    $field_idea_phases = $idea->get('field_social_idea_phases');
    if ($field_idea_phases->isEmpty()) {
      return NULL;
    }

    $idea_phases = $field_idea_phases->referencedEntities();
    $currentPhase = $idea_phases[0];
    foreach ($idea_phases as $phaseEntity) {
      $current_phase_date_start = $currentPhase->get('field_phase_date')->start_date->getTimestamp();
      $checking_phase_date_start = $phaseEntity->get('field_phase_date')->start_date->getTimestamp();
      if ($checking_phase_date_start > $current_phase_date_start) {
        $currentPhase = $phaseEntity;
      }
    }

    return $currentPhase;
  }

  /**
   * Check if idea node has phase.
   *
   * @param \Drupal\node\NodeInterface $node
   *   Checked node.
   * @param \Drupal\social_challenge_phase\PhaseInterface $phase
   *   Phase to check.
   *
   * @return bool
   *   Has phase or not.
   */
  private function hasPhase(NodeInterface $node, PhaseInterface $phase): bool {
    if (!$node->hasField('field_social_idea_phases')) {
      return FALSE;
    }

    $idea_phases = $node->get('field_social_idea_phases');
    if ($idea_phases->isEmpty()) {
      return FALSE;
    }

    $idea_phases_values = $idea_phases->getValue();

    return in_array(['target_id' => $phase->id()], $idea_phases_values);
  }

}
