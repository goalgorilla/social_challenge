<?php

namespace Drupal\social_challenge_phase\Service;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\social_challenge_phase\Event\SocialChallengePhaseEndingEvent;
use Drupal\social_challenge_phase\Event\SocialChallengePhaseEvents;
use Drupal\social_challenge_phase\PhaseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Defines a helper class for Social Challenge Phase module.
 */
class SocialChallengePhaseHelper implements ContainerInjectionInterface {

  /**
   * State data id with last time run Phase dispatching.
   *
   * @const string
   */
  const DISPATCH_PHASE_ENDING_LAST_RUN = 'social_challenge_phase.statistics.last_run';

  /**
   * The event dispatcher.
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * Entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The date formatter.
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The time service.
   */
  protected TimeInterface $time;

  /**
   * The state key value store.
   */
  protected StateInterface $state;

  /**
   * SocialChallengePhaseHelper constructor.
   *
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $dispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state key value store.
   */
  public function __construct(
    EventDispatcherInterface $dispatcher,
    EntityTypeManagerInterface $entity_type_manager,
    DateFormatterInterface $date_formatter,
    TimeInterface $time,
    StateInterface $state
  ) {
    $this->eventDispatcher = $dispatcher;
    $this->entityTypeManager = $entity_type_manager;
    $this->dateFormatter = $date_formatter;
    $this->time = $time;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('event_dispatcher'),
      $container->get('entity_type.manager'),
      $container->get('date.formatter'),
      $container->get('datetime.time'),
      $container->get('state'),
    );
  }

  /**
   * Dispatch Phases ending event.
   */
  public function dispatchPhaseEnding(): void {
    // Get last run time.
    $last_run = $this->state->get(self::DISPATCH_PHASE_ENDING_LAST_RUN);
    $current_time = $this->time->getCurrentTime();

    // Format dates to use in query.
    $last_run_formatted = $this->dateFormatter->format(
      $last_run,
      'custom',
      DateTimeItemInterface::DATE_STORAGE_FORMAT,
    );

    $current_time_formatted = $this->dateFormatter->format(
      $current_time,
      'custom',
      DateTimeItemInterface::DATE_STORAGE_FORMAT,
    );

    /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $storage */
    $storage = $this->entityTypeManager->getStorage('phase');

    $range = [$last_run_formatted, $current_time_formatted];

    /** @var array $phase_ids */
    $phase_ids = $storage->getQuery()
      ->accessCheck()
      // Load phases with ending date between last run time and current time.
      ->condition('field_phase_date.end_value', $range, 'BETWEEN')
      ->condition('status', TRUE)
      ->accessCheck(FALSE)
      ->execute();

    if (empty($phase_ids)) {
      // Nothing to dispatch.
      $this->state->set(self::DISPATCH_PHASE_ENDING_LAST_RUN, $current_time);
      return;
    }

    foreach ($phase_ids as $phase_id) {
      $phase = $storage->load($phase_id);

      if ($phase instanceof PhaseInterface) {
        $this->eventDispatcher->dispatch(
          new SocialChallengePhaseEndingEvent($phase),
          SocialChallengePhaseEvents::COMPLETED,
        );
      }
    }

    // Update last run.
    $this->state->set(self::DISPATCH_PHASE_ENDING_LAST_RUN, $current_time);
  }

}
