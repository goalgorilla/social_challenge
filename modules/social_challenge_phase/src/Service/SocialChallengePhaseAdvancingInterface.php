<?php

namespace Drupal\social_challenge_phase\Service;

use Drupal\node\NodeInterface;
use Drupal\social_challenge_phase\PhaseInterface;

/**
 * Social challenge phase advancing service interface.
 *
 * @package Drupal\social_challenge\Services
 */
interface SocialChallengePhaseAdvancingInterface {

  /**
   * Advance idea.
   *
   * @param \Drupal\node\NodeInterface $idea
   *   Idea node entity.
   * @param \Drupal\social_challenge_phase\PhaseInterface|null $currentPhase
   *   Current phase or null.
   *
   * @return bool
   *   Advanced or not.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function advanceIdea(NodeInterface $idea, PhaseInterface $currentPhase = NULL): bool;

  /**
   * Get the next phase in the challenge.
   *
   * @param \Drupal\social_challenge_phase\PhaseInterface $phase
   *   Current phase.
   *
   * @return \Drupal\social_challenge_phase\PhaseInterface
   *   Next phase.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getNextPhase(PhaseInterface $phase): ?PhaseInterface;

  /**
   * Get promoting criteria for ideas from current phase.
   *
   * @param \Drupal\social_challenge_phase\PhaseInterface $phase
   *   The Phase entity.
   *
   * @return string
   *   Promoting criteria string.
   */
  public function getPromotingCriteria(PhaseInterface $phase): string;

  /**
   * Get promoting category for the current phase.
   *
   * @param \Drupal\social_challenge_phase\PhaseInterface $phase
   *   The Phase entity.
   *
   * @return string
   *   Promoting category string.
   */
  public function getPromotingCategory(PhaseInterface $phase): string;

  /**
   * Get promoted ideas list for the current phase.
   *
   * @param \Drupal\social_challenge_phase\PhaseInterface $phase
   *   The Phase entity.
   *
   * @return array
   *   Promoted Ideas list.
   */
  public function getPromotedIdeas(PhaseInterface $phase): array;

}
