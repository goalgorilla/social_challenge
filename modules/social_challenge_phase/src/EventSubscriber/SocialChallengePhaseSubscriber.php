<?php

namespace Drupal\social_challenge_phase\EventSubscriber;

use Drupal\Core\Queue\QueueFactory;
use Drupal\social_challenge_phase\Event\SocialChallengePhaseEndingEvent;
use Drupal\social_challenge_phase\Event\SocialChallengePhaseEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event listener for Social Challenge Phase module.
 */
class SocialChallengePhaseSubscriber implements EventSubscriberInterface {

  /**
   * The queue service.
   */
  protected QueueFactory $queueFactory;

  /**
   * Constructs a new SocialChallengePhaseSubscriber.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queue_factory
   *   The queue service.
   */
  public function __construct(QueueFactory $queue_factory) {
    $this->queueFactory = $queue_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[SocialChallengePhaseEvents::COMPLETED][] = ['sendPhaseStatisticsToUsers'];
    return $events;
  }

  /**
   * Send phase statistics to users as phase was ended.
   *
   * @param \Drupal\social_challenge_phase\Event\SocialChallengePhaseEndingEvent $event
   *   The event to process.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function sendPhaseStatisticsToUsers(SocialChallengePhaseEndingEvent $event): void {
    $phase = $event->getPhase();
    if (!$phase->shouldSendStatisticsToUsers()) {
      // CM+ doesn't allow sending statistics for the current phase.
      return;
    }

    $queueItem = [
      'phase_id' => $phase->id(),
    ];

    // Add the item to the queue.
    $queue = $this->queueFactory->get('phase_statistics_emails_queue');
    $queue->createItem($queueItem);
  }

}
