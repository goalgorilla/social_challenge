<?php

namespace Drupal\social_challenge_phase\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Phase settings form.
 *
 * @ingroup social_challenge_phase
 */
class PhaseSettingsForm extends FormBase {

  /**
   * Returns a unique string identifying the form.
   *
   *   The unique string identifying the form.
   */
  public function getFormId(): string {
    return 'social_challenge_phase_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Empty implementation of the abstract submit class.
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['phase_settings']['#markup'] = 'Settings form for Phase entity. Manage field settings here.';
    return $form;
  }

}
