<?php

namespace Drupal\social_challenge_phase\Access;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\social_challenge_phase\Entity\Phase;
use Drupal\social_challenge_phase\Service\SocialChallengePhasePermissions;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Phase permissions.
 *
 * @package Drupal\social_challenge_phase\Access
 */
class PhasePermissions implements ContainerInjectionInterface {

  /**
   * The entity manager.
   */
  protected SocialChallengePhasePermissions $phasePermissions;

  /**
   * Constructs a new PhasePermissions instance.
   *
   * @param \Drupal\social_challenge_phase\Service\SocialChallengePhasePermissions $phase_permissions
   *   The entity manager.
   */
  public function __construct(SocialChallengePhasePermissions $phase_permissions) {
    $this->phasePermissions = $phase_permissions;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('social_challenge_phase.permissions')
    );
  }

  /**
   * Defines permissions for each phase.
   *
   *   An array of permissions.
   */
  public function permissions(): array {
    $permissions = [];

    foreach ($this->phasePermissions->getDefinedPermissions() as $perm) {
      /** @var \Drupal\social_challenge_phase\Entity\Phase $phase */
      foreach (Phase::loadMultiple() as $phase) {
        $permissionName = $this->phasePermissions->getPermissionName($perm['permission'], $phase);
        $permissions += [
          $permissionName => [
            'title' => $perm['permission'] . " '" . $phase->label() . "'",
            'description' => $perm['description'],
          ],
        ];
      }
    }

    return $permissions;
  }

}
