<?php

namespace Drupal\social_challenge_phase\Plugin\Action;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Action\ActionBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\social_challenge_phase\Service\SocialChallengePhaseAdvancing;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Advance an idea.
 *
 * @Action(
 *   id = "idea_advance_action",
 *   label = @Translation("Advance selected ideas"),
 * )
 */
class AdvanceIdea extends ActionBase implements ContainerFactoryPluginInterface {

  /**
   * Phase advancing service.
   */
  protected SocialChallengePhaseAdvancing $phaseAdvancingService;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SocialChallengePhaseAdvancing $phaseAdvancingService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->phaseAdvancingService = $phaseAdvancingService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('social_challenge_phase.advancing')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function execute($entity = NULL): void {
    $this->phaseAdvancingService->advanceIdea($entity);
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\node\NodeInterface $object */
    $result = AccessResult::allowedIfHasPermission($account, 'edit any social_idea content')
      ->andIf(AccessResult::allowedIf($object->bundle() === 'social_idea'));

    return $return_as_object ? $result : $result->isAllowed();
  }

}
