<?php

namespace Drupal\social_challenge_phase\Plugin\ActivityAction;

use Drupal\activity_creator\Plugin\ActivityActionBase;
use Drupal\social_challenge_phase\PhaseInterface;

/**
 * Provides a 'PhaseEndedActivityAction' activity action.
 *
 * @ActivityAction(
 *   id = "phase_ended_action",
 *   label = @Translation("Action that is triggered when phase was ended"),
 * )
 */
class PhaseEndedActivityAction extends ActivityActionBase {

  /**
   * {@inheritdoc}
   */
  public function isValidEntity($entity): bool {
    // @todo Make this action general (for all entities) and move to distro.
    // For now the action can be applied only to Phases.
    return ($entity instanceof PhaseInterface);
  }

}
