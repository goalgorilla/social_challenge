<?php

namespace Drupal\social_challenge_phase\Plugin\ActivityContext;

use Drupal\activity_creator\ActivityFactory;
use Drupal\activity_creator\Plugin\ActivityContextBase;
use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\Sql\QueryFactory;
use Drupal\group\Entity\GroupInterface;
use Drupal\social_challenge\ChallengeWrapperInterface;
use Drupal\social_challenge_phase\PhaseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a 'PhaseActivityContext' activity context.
 *
 * @ActivityContext(
 *   id = "phase_activity_context",
 *   label = @Translation("Phase context"),
 * )
 */
class PhaseActivityContext extends ActivityContextBase {

  /**
   * The challenge wrapper.
   */
  protected ChallengeWrapperInterface $challengeWrapper;

  /**
   * PhaseActivityContext constructor.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\Query\Sql\QueryFactory $entity_query
   *   The entity query.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\activity_creator\ActivityFactory $activity_factory
   *   The activity factory service.
   * @param \Drupal\social_challenge\ChallengeWrapperInterface $challenge_wrapper
   *   The challenge wrapper.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    QueryFactory $entity_query,
    EntityTypeManagerInterface $entity_type_manager,
    ActivityFactory $activity_factory,
    ChallengeWrapperInterface $challenge_wrapper
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_query, $entity_type_manager, $activity_factory);
    $this->challengeWrapper = $challenge_wrapper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity.query.sql'),
      $container->get('entity_type.manager'),
      $container->get('activity_creator.activity_factory'),
      $container->get('social_challenge.wrapper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getRecipients(array $data, $last_uid, $limit): array {
    if (empty($data['related_object'][0])) {
      return [];
    }

    $related_object = $data['related_object'][0];
    $storage = $this->entityTypeManager->getStorage($related_object['target_type']);
    $phase = $storage->load($related_object['target_id']);

    if (!($phase instanceof PhaseInterface)) {
      return [];
    }

    try {
      $challenges = $this->challengeWrapper->getChallengesByPhase((string) $phase->id());
      $challenge = reset($challenges);

      if ($challenge instanceof GroupInterface) {
        foreach ($challenge->getMembers() as $member) {
          $user = $member->getUser();
          if (!$user->isActive()) {
            continue;
          }

          $recipients[] = [
            'target_type' => 'user',
            'target_id' => $user->id(),
          ];
        }
      }
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      return [];
    }

    return $recipients ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function isValidEntity(EntityInterface $entity): bool {
    // @todo Make this action general (for all entities) and move to distro.
    // For now the context can be applied only to Phases.
    return ($entity instanceof PhaseInterface);
  }

}
