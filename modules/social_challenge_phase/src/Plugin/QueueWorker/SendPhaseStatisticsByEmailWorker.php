<?php

namespace Drupal\social_challenge_phase\Plugin\QueueWorker;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\activity_creator\Plugin\ActivityActionManager;
use Drupal\social_challenge_phase\PhaseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes tasks for sending phase statistics.
 *
 * @QueueWorker(
 *   id = "phase_statistics_emails_queue",
 *   title = @Translation("Phase statistics send worker"),
 *   cron = {"time" = 60}
 * )
 *
 * This QueueWorker is responsible for sending emails from the queue.
 */
class SendPhaseStatisticsByEmailWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The activity action manager.
   */
  protected ActivityActionManager $activityActionManager;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * SendPhaseStatisticsByEmailWorker constructor.
   *
   * @param array $configuration
   *   The configuration.
   * @param string $plugin_id
   *   The plugin id.
   * @param array $plugin_definition
   *   The plugin definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\activity_creator\Plugin\ActivityActionManager $activity_action_manager
   *   The activity action manager.
   */
  public function __construct(array $configuration, $plugin_id, array $plugin_definition, EntityTypeManagerInterface $entity_type_manager, ActivityActionManager $activity_action_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->entityTypeManager = $entity_type_manager;
    $this->activityActionManager = $activity_action_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.activity_action.processor')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    // Check existing required data.
    if (empty($data['phase_id'])) {
      return;
    }

    $phase = $this->entityTypeManager->getStorage('phase')->load($data['phase_id']);

    if (!($phase instanceof PhaseInterface)) {
      return;
    }

    /** @var \Drupal\activity_creator\Plugin\ActivityActionInterface $plugin */
    $plugin = $this->activityActionManager->createInstance('phase_ended_action');
    $plugin->create($phase);
  }

}
