<?php

namespace Drupal\social_challenge_phase\Plugin\QueueWorker;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\node\NodeInterface;
use Drupal\social_challenge_phase\PhaseInterface;
use Drupal\social_challenge_phase\Service\SocialChallengePhaseAdvancing;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Processes tasks for advancing ideas.
 *
 * @QueueWorker(
 *   id = "advance_ideas_queue",
 *   title = @Translation("Advance ideas worker"),
 *   cron = {"time" = 180}
 * )
 */
class AdvanceIdeasWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * Phase advancing service.
   */
  protected SocialChallengePhaseAdvancing $phaseAdvancingService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('social_challenge_phase.advancing')
    );
  }

  /**
   * AdvanceIdeasWorker constructor.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, SocialChallengePhaseAdvancing $phaseAdvancingService) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->phaseAdvancingService = $phaseAdvancingService;
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data): void {
    // Check existing data of idea.
    if (!isset($data['idea']) || empty($data['idea'])) {
      return;
    }

    // Check existing data of phase.
    if (!isset($data['phase']) || empty($data['phase'])) {
      return;
    }

    $idea = $data['idea'];
    $phase = $data['phase'];

    // Check match types of data.
    if (!$idea instanceof NodeInterface || !$phase instanceof PhaseInterface) {
      return;
    }

    $this->phaseAdvancingService->advanceIdea($idea, $phase);
  }

}
