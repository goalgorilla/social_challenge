<?php

namespace Drupal\social_challenge_phase;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides content translation defaults for the Social Challenge Phase module.
 *
 * @package Drupal\social_challenge_phase
 */
class ContentTranslationDefaultsConfigOverride implements ConfigFactoryOverrideInterface {

  /**
   * The module handler.
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Are we in override mode?
   */
  protected bool $inOverride = FALSE;

  /**
   * SocialChallengePhaseConfigOverride constructor.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(ModuleHandlerInterface $module_handler) {
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    // Basically this makes sure we can load overrides without getting stuck
    // in a loop.
    if ($this->inOverride) {
      return [];
    }

    $this->inOverride = TRUE;

    $overrides = [];

    if ($this->moduleHandler->moduleExists('social_content_translation')) {
      $config_names = [
        'language.content_settings.phase.phase',
        'language.content_settings.taxonomy_term.phase_title',
      ];
      foreach ($config_names as $config_name) {
        if (in_array($config_name, $names)) {
          $overrides[$config_name] = [
            'third_party_settings' => [
              'content_translation' => [
                'enabled' => TRUE,
              ],
            ],
          ];
        }
      }

      $config_name = 'core.base_field_override.taxonomy_term.phase_title.name';
      if (in_array($config_name, $names)) {
        $overrides[$config_name] = [
          'translatable' => TRUE,
        ];
      }
    }

    $this->inOverride = FALSE;

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix(): string {
    return 'social_challenge_phase.translation_defaults_config_override';
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name): CacheableMetadata {
    return new CacheableMetadata();
  }

}
