<?php

/**
 * @file
 * Contains Drupal\social_challenge_phase\social_challenge_phase.module.
 */

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\node\NodeInterface;
use Drupal\social_challenge_phase\Entity\Phase;
use Drupal\social_challenge_phase\PhaseInterface;

/**
 * Implements hook_theme().
 */
function social_challenge_phase_theme(): array {
  return [
    'phase' => [
      'render element' => 'elements',
    ],
    'phase__small_teaser' => [
      'base hook' => 'phase',
    ],
    'phase__active__small_teaser' => [
      'base hook' => 'phase',
    ],
  ];
}

/**
 * Implements hook_theme_suggestions_HOOK().
 */
function social_challenge_phase_theme_suggestions_phase(array $variables): array {
  $suggestions = [];
  $phase = $variables['elements']['#phase'];
  $sanitized_view_mode = str_replace('.', '_', $variables['elements']['#view_mode']);

  $suggestions[] = 'phase__' . $sanitized_view_mode;

  if ($phase->isActive()) {
    $suggestions[] = 'phase__active';
    $suggestions[] = 'phase__active__' . $sanitized_view_mode;
  }

  return $suggestions;
}

/**
 * Prepares variables for Phase templates.
 *
 * Default template: phase.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_phase(array &$variables): void {
  /** @var \Drupal\social_challenge_phase\Entity\Phase $phase */
  $phase = $variables['elements']['#phase'];

  if ($phase->isPast()) {
    $variables['attributes']['class'][] = 'phase-past';
    $variables['is_past'] = TRUE;
  }

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }

  // Only check permissions when the phase is active.
  if ($phase->isActive()) {
    // Load the service.
    $phase_permissions = Drupal::getContainer()->get('social_challenge_phase.permissions');
    // Add the permission key/values to the variables.
    foreach ($phase_permissions->getDefinedPermissions() as $permArray) {
      $key = str_replace(' ', '_', $permArray['permission']);
      $value = $phase_permissions->hasPermissionValue($permArray['permission'], $phase);
      $variables['permission'][$key] = $value;
    }
  }
}

/**
 * Implements hook_inline_entity_form_entity_form_alter().
 */
function social_challenge_phase_inline_entity_form_entity_form_alter(array &$entity_form, FormStateInterface &$form_state): void {
  if ($entity_form['#entity_type'] === 'phase') {
    $group = _social_group_get_current_group();
    $current_user = \Drupal::currentUser();
    // Must be a group.
    if (($group instanceof GroupInterface) && !$group->hasPermission('administer phase permissions', $current_user)) {
      // Deny access from the permission fields.
      $entity_form['field_phase_create_idea_in_phase']['#access'] = FALSE;
      $entity_form['field_phase_edit_idea_in_phase']['#access'] = FALSE;
      $entity_form['field_phase_comment_in_phase']['#access'] = FALSE;
      $entity_form['field_phase_view_idea_in_phase']['#access'] = FALSE;
      $entity_form['field_phase_vote_in_phase']['#access'] = FALSE;
    }
    // Validate that phase dates don't overlap.
    array_unshift($entity_form['field_phase_date']['widget'][0]['#element_validate'], '_social_challenge_phase_date_validate');

    if (isset($entity_form['#default_value']) &&
      !empty($entity_form['#default_value']) &&
      $entity_form['#default_value'] instanceof PhaseInterface
    ) {
      $phase = $entity_form['#default_value'];

      if ($entity_form['#translating']) {
        $entity_form['title_translation'] = [
          '#type' => 'textfield',
          '#title' => $entity_form['field_phase_title']['widget']['#title'],
          '#weight' => $entity_form['field_phase_title']['#weight'],
          '#default_value' => $phase->label(),
          '#required' => $entity_form['field_phase_title']['widget']['#required'],
        ];
        $entity_form['#element_validate'][] = '_social_challenge_phase_save_title_translation';
      }

      if ($phase->isPast()) {
        // Disable ideas advancement fields when the phase is past.
        $entity_form['field_phase_auto_advance_ideas']['widget']['value']['#disabled'] = TRUE;
        $entity_form['field_phase_auto_advance_cat']['widget']['#disabled'] = TRUE;
        $entity_form['field_phase_auto_advance_count']['widget'][0]['value']['#disabled'] = TRUE;
        $entity_form['field_phase_auto_advance_param']['widget']['#disabled'] = TRUE;
        $entity_form['field_phase_auto_advance_source']['widget']['#disabled'] = TRUE;

        // Add warning text when the phase is past.
        $entity_form['warning_text'] = [
          '#type' => 'html_tag',
          '#tag' => 'p',
          '#value' => t("You can't change these values because the phase is past."),
        ];
        $entity_form['#group_children']['warning_text'] = 'group_main_ideas_advancement_set';
        $entity_form['#fieldgroups']['group_main_ideas_advancement_set']->children[] = 'warning_text';
      }
    }

    // Get field's names based on form operation and row delta.
    if ($entity_form['#op'] === 'edit') {
      $field_phase_auto_advance_ideas_name = sprintf('field_challenge_phases[form][inline_entity_form][entities][%s][form][field_phase_auto_advance_ideas][value]', $entity_form['#ief_row_delta']);
      $field_phase_auto_advance_cat_name = sprintf('field_challenge_phases[form][inline_entity_form][entities][%s][form][field_phase_auto_advance_cat]', $entity_form['#ief_row_delta']);
      $field_phase_stat_send_status = sprintf('field_challenge_phases[form][inline_entity_form][entities][%s][form][field_phase_stat_send_status][value]', $entity_form['#ief_row_delta']);
    }
    else {
      $field_phase_auto_advance_ideas_name = sprintf('field_challenge_phases[form][%s][field_phase_auto_advance_ideas][value]', $entity_form['#ief_row_delta']);
      $field_phase_auto_advance_cat_name = sprintf('field_challenge_phases[form][%s][field_phase_auto_advance_cat]', $entity_form['#ief_row_delta']);
      $field_phase_stat_send_status = sprintf('field_challenge_phases[form][%s][field_phase_stat_send_status][value]', $entity_form['#ief_row_delta']);
    }

    // Set #states to ideas advancement fields.
    $entity_form['field_phase_auto_advance_cat']['widget']['#states'] = [
      'visible' => [
        ':input[name="' . $field_phase_auto_advance_ideas_name . '"]' => ['checked' => TRUE],
      ],
    ];
    $entity_form['field_phase_auto_advance_count']['widget'][0]['value']['#states'] = [
      'visible' => [
        ':input[name="' . $field_phase_auto_advance_cat_name . '"]' => ['value' => 'top'],
        ':input[name="' . $field_phase_auto_advance_ideas_name . '"]' => ['checked' => TRUE],
      ],
    ];
    $entity_form['field_phase_auto_advance_param']['widget']['#states'] = [
      'visible' => [
        ':input[name="' . $field_phase_auto_advance_cat_name . '"]' => ['value' => 'top'],
        ':input[name="' . $field_phase_auto_advance_ideas_name . '"]' => ['checked' => TRUE],
      ],
    ];
    $entity_form['field_phase_auto_advance_source']['widget']['#states'] = [
      'visible' => [
        ':input[name="' . $field_phase_auto_advance_cat_name . '"]' => ['value' => 'top'],
        ':input[name="' . $field_phase_auto_advance_ideas_name . '"]' => ['checked' => TRUE],
      ],
    ];

    // Change titles of ideas advancement fields.
    $entity_form['field_phase_auto_advance_cat']['widget']['#title_display'] = 'invisible';
    $entity_form['field_phase_auto_advance_param']['widget']['#title_display'] = 'invisible';
    $entity_form['field_phase_auto_advance_count']['widget'][0]['value']['#title'] = t('Best');
    $entity_form['field_phase_auto_advance_source']['widget']['#title'] = t('in');

    // Add title to additional settings section of ideas advancement.
    $entity_form['additional_ideas_advancement_setting_title'] = [
      '#type' => 'item',
      '#markup' => sprintf('<p class="ideas-adv-settings-title">%s</p>', t('Automatically advance the ideas based on the following criteria:')),
      '#states' => [
        'visible' => [
          ':input[name="' . $field_phase_auto_advance_cat_name . '"]' => ['value' => 'top'],
          ':input[name="' . $field_phase_auto_advance_ideas_name . '"]' => ['checked' => TRUE],
        ],
      ],
    ];
    $entity_form['#group_children']['additional_ideas_advancement_setting_title'] = 'group_add_ideas_advance_settings';
    $entity_form['#fieldgroups']['group_add_ideas_advance_settings']->children[] = 'additional_ideas_advancement_setting_title';

    // Unset _none options if they isset.
    if (isset($entity_form['field_phase_auto_advance_cat']['widget']['#options']['_none'])) {
      unset($entity_form['field_phase_auto_advance_cat']['widget']['#options']['_none']);
    }
    if (isset($entity_form['field_phase_auto_advance_param']['widget']['#options']['_none'])) {
      unset($entity_form['field_phase_auto_advance_param']['widget']['#options']['_none']);
    }
    if (isset($entity_form['field_phase_auto_advance_source']['widget']['#options']['_none'])) {
      unset($entity_form['field_phase_auto_advance_source']['widget']['#options']['_none']);
    }

    // Set settings for Phase Statistics fields.
    $entity_form['field_phase_stat_message']['#states'] = [
      'visible' => [
        ':input[name="' . $field_phase_stat_send_status . '"]' => ['checked' => TRUE],
      ],
    ];
  }
}

/**
 * Validate that phase dates don't overlap.
 */
function _social_challenge_phase_date_validate(array &$element, FormStateInterface $form_state, array &$form): void {
  $input_exists = FALSE;
  $field_name = implode('][', $element['#parents']);
  $input = NestedArray::getValue($form_state->getValues(), $element['#parents'], $input_exists);
  if ($input_exists && $input['value'] instanceof DrupalDateTime && $input['end_value'] instanceof DrupalDateTime) {
    $start_date = $input['value']->getTimestamp();
    $end_date = $input['end_value']->getTimestamp();
    $phases = $form['field_challenge_phases']['widget']['entities'];

    foreach ($phases as $i => $entity) {
      if (is_numeric($i)) {
        $phase = $entity['#entity'];
        $other_start_date = $phase->get('field_phase_date')->start_date->getTimestamp();
        $other_end_date = $phase->get('field_phase_date')->end_date->getTimestamp();
        if (($start_date !== $other_start_date && $end_date !== $other_end_date) && $start_date < $other_end_date && $other_start_date < $end_date) {
          $form_state->setErrorByName($field_name, t('Only one phase can be active at a time.'));
        }
      }
    }
  }
}

/**
 * Save a title translation.
 *
 * @param array $element
 *   The form element.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The form state.
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityStorageException
 */
function _social_challenge_phase_save_title_translation(array &$element, FormStateInterface $form_state): void {
  $old_value = $element['title_translation']['#default_value'];
  $new_value = $element['title_translation']['#value'];
  if ($new_value !== $old_value) {
    $entity_manager = \Drupal::entityTypeManager();
    $phase_values = $form_state->getValue($element['#parents']);
    $phase_title_id = $phase_values['field_phase_title'][0]['target_id'];
    /** @var \Drupal\taxonomy\TermInterface $phase_title_term */
    $phase_title_term = $entity_manager->getStorage('taxonomy_term')->load($phase_title_id);
    $langcode = $phase_values['langcode'][0]['value'];
    if ($phase_title_term->hasTranslation($langcode)) {
      $phase_title_term_tr = $phase_title_term->getTranslation($langcode);
    }
    else {
      $phase_title_term_tr = $phase_title_term->addTranslation($langcode);
    }
    /** @var \Drupal\taxonomy\TermInterface $phase_title_term_tr */
    $phase_title_term_tr->setName($new_value);
    $phase_title_term_tr->save();
  }
}

/**
 * Implements hook_inline_entity_form_table_fields_alter().
 */
function social_challenge_phase_inline_entity_form_table_fields_alter(array &$fields, array $context): void {
  // Add Date field to phase IEF table.
  if ($context['entity_type'] === 'phase') {
    $fields['field_phase_date'] = [
      'type' => 'field',
      'label' => t('Date'),
      'weight' => 2,
    ];
  }
}

/**
 * Implements hook_ENTITY_TYPE_update().
 */
function social_challenge_phase_phase_update(EntityInterface $entity): void {
  /** @var Drupal\social_challenge_phase\Entity\Phase $entity */
  _social_challenge_phase_save_permissions($entity);

  // Send notifications if phase status was changed from non-active to active.
  $old_value = $entity->original->get('field_phase_active_status')->value;
  $new_value = $entity->get('field_phase_active_status')->value;

  if ($old_value == 0 && $new_value == 1) {
    $ideas = \Drupal::entityTypeManager()->getStorage('node')
      ->loadByProperties([
        'status' => 1,
        'type' => 'social_idea',
        'field_social_idea_phases' => $entity->id(),
      ]);

    foreach ($ideas as $idea) {
      $idea->setCreatedTime(\Drupal::time()->getRequestTime());

      /** @var \Drupal\activity_creator\Plugin\ActivityActionInterface $plugin */
      $plugin = \Drupal::service('plugin.manager.activity_action.processor')->createInstance('move_entity_action');
      $plugin->create($idea);
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function social_challenge_phase_phase_insert(EntityInterface $entity): void {
  /** @var Drupal\social_challenge_phase\Entity\Phase $entity */
  _social_challenge_phase_save_permissions($entity);
}

/**
 * Set permissions for a phase.
 *
 * @param \Drupal\social_challenge_phase\Entity\Phase $phase
 *   The phase.
 */
function _social_challenge_phase_save_permissions(Phase $phase): void {
  // Load the service.
  $phase_permissions = Drupal::getContainer()->get('social_challenge_phase.permissions');
  // Add the permission key/values to the variables.
  foreach ($phase_permissions->getDefinedPermissions() as $perm_array) {
    // The permissions name.
    $perm_name = $perm_array['permission'];
    // The permission field name in the entity.
    $perm_name_field = str_replace(' ', '_', $perm_array['permission']);
    // Get the permission value from the entity.
    $perm_value = $phase->get('field_phase_' . $perm_name_field)->value;
    $phase_permissions->setPermissionValue($perm_name, $phase, (bool) $perm_value);
  }
}

/**
 * Implements hook_node_access().
 *
 * Remember: if any module returns forbidden and denies access to certain node
 * and operation it will not allow the user to do the operation on the node.
 */
function social_challenge_phase_node_access(NodeInterface $node, string $op, AccountInterface $account): AccessResultInterface {
  // Leave the ability to edit idea for SM/CM.
  if (
    $op === 'update' && !$node->isNew() &&
    $node->getType() === 'social_idea' &&
    !_social_challenge_has_permission('edit idea in phase', NULL, $node) &&
    !$account->hasPermission('edit any social_idea content')
  ) {
    return AccessResult::forbidden();
  }

  return AccessResult::neutral();
}

/**
 * Implements hook_cron().
 */
function social_challenge_phase_cron(): void {
  // Automatically advance ideas to the next phase.
  _social_challenge_phase_advance();
  // Send notifications after ideas was advanced to the next phase.
  _social_challenge_phase_notifications();
  // Dispatch Phases that were ended.
  \Drupal::service('social_challenge_phase.helper')->dispatchPhaseEnding();
}

/**
 * Automatically advance ideas to the next phase.
 */
function _social_challenge_phase_advance(): void {
  // Check ultimate cron exists due to in that case we need to ensure the
  // schedule rule is configured for each 5 minutes to avoid loose ideas when
  // pass to next phase.
  if (\Drupal::service('module_handler')->moduleExists('ultimate_cron')) {
    $five_minutes_rule = '*/5+@ * * * *';
    /** @var \Drupal\ultimate_cron\CronJobInterface $cronjob */
    $cronjob = \Drupal::entityTypeManager()
      ->getStorage('ultimate_cron_job')
      ->load('social_challenge_phase_cron');
    $scheduler = $cronjob->getSchedulerId();

    if (!empty($scheduler) || (isset($scheduler['configuration']['rules'][0]) && $scheduler['configuration']['rules'][0] !== $five_minutes_rule)) {
      $scheduler['configuration']['rules'][0] = $five_minutes_rule;
      $cronjob->setConfiguration('scheduler', $scheduler['configuration']);
      $cronjob->save();
    }
  }
  // We usually have cron run each 5 min.
  $interval = 300;

  $phases = \Drupal::entityTypeManager()
    ->getStorage('phase')
    ->loadByProperties([
      'status' => 1,
      'field_phase_auto_advance_ideas' => 1,
    ]);

  /** @var \Drupal\Core\Queue\QueueInterface $advance_ideas_queue */
  $advance_ideas_queue = \Drupal::service('queue')->get('advance_ideas_queue');

  /** @var \Drupal\social_challenge_phase\PhaseInterface $phase */
  foreach ($phases as $phase) {
    if (!$phase->isGoingToPast($interval)) {
      continue;
    }

    $ideas = \Drupal::service('social_challenge_phase.advancing')->getPromotedIdeas($phase);

    foreach ($ideas as $idea) {
      $advance_ideas_queue->createItem([
        'idea' => $idea,
        'phase' => $phase,
      ]);
    }
  }
}

/**
 * Send notifications after ideas was advanced to the next phase.
 */
function _social_challenge_phase_notifications(): void {
  // We usually have cron run each 5 min.
  $interval = 300;
  $phases = \Drupal::entityTypeManager()
    ->getStorage('phase')
    ->loadByProperties([
      'status' => 1,
    ]);

  /** @var \Drupal\social_challenge_phase\PhaseInterface $phase */
  foreach ($phases as $phase) {
    $phase_active_status = $phase->field_phase_active_status->value;

    if ($phase->isActiveRecently($interval) && $phase_active_status == 0) {
      $phase->field_phase_active_status = 1;
      $phase->save();
    }
    elseif (!$phase->isActive() && $phase_active_status == 1) {
      $phase->field_phase_active_status = 0;
      $phase->save();
    }
  }
}

/**
 * Get ideas for advancement from phase.
 *
 * @param \Drupal\social_challenge_phase\PhaseInterface $phase
 *   Phase entity.
 *
 * @return \Drupal\node\NodeInterface[]
 *   Array of ideas for advancement.
 *
 *  @deprecated in social_challenge:3.2.0 and is removed
 *   from social_challenge:4.0.0.
 *   Use \Drupal::service('social_challenge_phase.advancing')
 *    ->getPromotedIdeas() instead.
 *
 * @phpcs:disable
 * @see https://bitbucket.org/goalgorilla/social_challenge/pull-requests/144
 * @phpcs:enable
 */
function _social_challenge_phase_get_ideas_to_advance(PhaseInterface $phase): array {
  return \Drupal::service('social_challenge_phase.advancing')->getPromotedIdeas($phase);
}

/**
 * Implements hook_form_alter().
 */
function social_challenge_phase_form_alter(array &$form, FormStateInterface $form_state): void {
  $form_object = $form_state->getFormObject();
  if (isset($form_object) && $form_object instanceof ContentEntityFormInterface) {
    $form_entity = $form_object->getEntity();
    if (isset($form_entity) && $form_entity instanceof GroupInterface && in_array($form_entity->bundle(), [
      'challenge',
      'cc',
      'sc',
    ])) {
      $form['#attached']['library'][] = 'social_challenge_phase/admin';
      // @todo Fix it in the distro.
      $form['#attached']['library'][] = 'jquery_ui_datepicker/datepicker';
      $form['#attached']['library'][] = 'socialbase/form--datepicker';
    }
  }
}

/**
 * Implements hook_activity_send_email_notifications_alter().
 */
function social_challenge_phase_activity_send_email_notifications_alter(array &$items, array $email_message_templates): void {
  if (isset($email_message_templates['update_challenge_phase_followers'])) {
    $items['what_follow']['templates'][] = 'update_challenge_phase_followers';
  }
  if (isset($email_message_templates['update_challenge_phase_authors'])) {
    $items['what_manage']['templates'][] = 'update_challenge_phase_authors';
  }
  if (isset($email_message_templates['completed_phase'])) {
    $items['what_manage']['templates'][] = 'completed_phase';
  }
}

/**
 * Implements hook_activity_allowed_duplicates_alter().
 */
function social_challenge_phase_activity_allowed_duplicates_alter(array &$allowed_duplicates): void {
  $allowed_duplicates[] = 'update_challenge_phase_followers';
  $allowed_duplicates[] = 'update_challenge_phase_authors';
  $allowed_duplicates[] = 'completed_phase';
}
