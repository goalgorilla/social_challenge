<?php

namespace Drupal\Tests\social_challenge_phase\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\social_challenge_phase\Service\SocialChallengePhaseAdvancing;

/**
 * Social challenge phase advancing test.
 *
 * @coversDefaultClass \Drupal\social_challenge_phase\SocialChallengePhaseAdvancing
 * @group social_challenge_phase
 */
class SocialChallengePhaseAdvancingTest extends UnitTestCase {

  /**
   * The mocked entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The social challenge phase advancing.
   *
   * @var \Drupal\social_challenge_phase\Service\SocialChallengePhaseAdvancing
   */
  protected $socialChallengePhaseAdvancing;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->entityTypeManager = $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface');

    $this->socialChallengePhaseAdvancing = new SocialChallengePhaseAdvancing($this->entityTypeManager);
    $container = new ContainerBuilder();
    \Drupal::setContainer($container);
  }

  /**
   * @covers ::advanceIdea
   */
  public function testCannotAdvanceIdea() {
    $idea = $this->createMock('\Drupal\node\NodeInterface');

    $this->assertFalse($this->socialChallengePhaseAdvancing->advanceIdea($idea));
  }

  /**
   * @covers ::advanceIdea
   */
  public function testCannotAdvanceIdeaWithoutCurrentPhase() {
    $idea = $this->createMock('\Drupal\node\NodeInterface');
    $idea
      ->expects($this->once())
      ->method('bundle')
      ->willReturn('social_idea');

    $this->assertFalse($this->socialChallengePhaseAdvancing->advanceIdea($idea));
  }

  /**
   * @covers ::advanceIdea
   */
  public function testCannotAdvanceIdeaWithoutNextPhase() {
    $idea = $this->createMock('\Drupal\node\NodeInterface');
    $idea
      ->expects($this->once())
      ->method('bundle')
      ->willReturn('social_idea');

    $current_phase = $this->createMock('Drupal\social_challenge_phase\Entity\Phase');
    $current_phase
      ->expects($this->once())
      ->method('id')
      ->willReturn('dummy_phase_id');

    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->once())
      ->method('loadByProperties')
      ->willReturn([]);

    $this->entityTypeManager
      ->expects($this->once())
      ->method('getStorage')
      ->with('group')
      ->willReturn($storage);

    $this->assertFalse($this->socialChallengePhaseAdvancing->advanceIdea($idea, $current_phase));
  }

  // @todo complete these test cases:
  // testCannotAdvanceIdeaWhenIsOnLastPhase()
  // testAdvanceIdea
}
