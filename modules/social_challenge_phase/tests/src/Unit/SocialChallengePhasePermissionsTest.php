<?php

namespace Drupal\Tests\social_challenge_phase\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\social_challenge_phase\Service\SocialChallengePhasePermissions;

/**
 * Social challenge phase permissions test.
 *
 * @coversDefaultClass \Drupal\social_challenge_phase\SocialChallengePhasePermissions
 * @group social_challenge_phase
 */
class SocialChallengePhasePermissionsTest extends UnitTestCase {

  /**
   * The dummy phase ID.
   *
   * @var string
   */
  protected $phaseId;

  /**
   * The dummy permission.
   *
   * @var string
   */
  protected $permission;

  /**
   * The mocked phase.
   *
   * @var \Drupal\social_challenge_phase\Entity\Phase|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $phase;

  /**
   * The mocked account.
   *
   * @var \Drupal\Core\Session\AccountInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $account;

  /**
   * The mocked entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The social challenge phase permissions.
   *
   * @var \Drupal\social_challenge_phase\Service\SocialChallengePhasePermissions
   */
  protected $socialChallengePhasePermissions;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->account = $this->createMock('Drupal\Core\Session\AccountInterface');
    $this->entityTypeManager = $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface');

    $this->socialChallengePhasePermissions = new SocialChallengePhasePermissions($this->account, $this->entityTypeManager);
    $container = new ContainerBuilder();
    \Drupal::setContainer($container);

    $this->phaseId = 'dummy_phase_id';
    $this->permission = 'has permission for phase';

    $this->phase = $this->createMock('Drupal\social_challenge_phase\Entity\Phase');
    $this->phase
      ->expects($this->any())
      ->method('id')
      ->willReturn($this->phaseId);
    $this->phase
      ->expects($this->any())
      ->method('getOwner')
      ->willReturn($this->account);
  }

  /**
   * @covers ::hasPermissionValue
   */
  public function testPhaseOwnerHasNotPermission() {
    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->once())
      ->method('load')
      ->willReturn([]);

    $this->entityTypeManager
      ->expects($this->once())
      ->method('getStorage')
      ->willReturn($storage);

    $this->account
      ->expects($this->once())
      ->method('hasPermission')
      ->with($this->permission . ' ' . $this->phaseId)
      ->willReturn(FALSE);

    $this->assertFalse($this->socialChallengePhasePermissions->hasPermissionValue($this->permission, $this->phase));
  }

  /**
   * @covers ::hasPermissionValue
   */
  public function testPhaseOwnerHasPermission() {
    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->once())
      ->method('load')
      ->willReturn([]);

    $this->entityTypeManager
      ->expects($this->once())
      ->method('getStorage')
      ->willReturn($storage);

    $this->account
      ->expects($this->once())
      ->method('hasPermission')
      ->with($this->permission . ' ' . $this->phaseId)
      ->willReturn(TRUE);

    $this->assertTrue($this->socialChallengePhasePermissions->hasPermissionValue($this->permission, $this->phase));
  }

  /**
   * @covers ::hasPermissionValue
   */
  public function testAuthenticateRoleHasNotPermission() {
    $role = $this->createMock('Drupal\user\Entity\Role');
    $role
      ->expects($this->once())
      ->method('hasPermission')
      ->with($this->permission . ' ' . $this->phaseId)
      ->willReturn(FALSE);

    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->once())
      ->method('load')
      ->willReturn($role);

    $this->entityTypeManager
      ->expects($this->once())
      ->method('getStorage')
      ->willReturn($storage);

    $this->assertFalse($this->socialChallengePhasePermissions->hasPermissionValue($this->permission, $this->phase));
  }

  /**
   * @covers ::hasPermissionValue
   */
  public function testAuthenticateRoleHasPermission() {
    $role = $this->createMock('Drupal\user\Entity\Role');
    $role
      ->expects($this->once())
      ->method('hasPermission')
      ->with($this->permission . ' ' . $this->phaseId)
      ->willReturn(TRUE);

    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->once())
      ->method('load')
      ->willReturn($role);

    $this->entityTypeManager
      ->expects($this->once())
      ->method('getStorage')
      ->willReturn($storage);

    $this->assertTrue($this->socialChallengePhasePermissions->hasPermissionValue($this->permission, $this->phase));
  }

  /**
   * @covers ::getPermissionName
   */
  public function testGetPermissionName() {
    $this->assertEquals(
      $this->permission . ' ' . $this->phaseId,
      $this->socialChallengePhasePermissions->getPermissionName($this->permission, $this->phase)
    );
  }

}
