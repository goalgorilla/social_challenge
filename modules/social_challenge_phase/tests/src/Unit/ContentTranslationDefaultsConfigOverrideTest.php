<?php

namespace Drupal\Tests\social_challenge_phase\Unit;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\social_challenge_phase\ContentTranslationDefaultsConfigOverride;

/**
 * Social challenge phase config override test.
 *
 * @coversDefaultClass \Drupal\social_challenge_phase\ContentTranslationDefaultsConfigOverride
 * @group social_challenge_phase
 */
class ContentTranslationDefaultsConfigOverrideTest extends UnitTestCase {

  use ProphecyTrait;
  /**
   * ContentTranslationDefaultsConfigOverride.
   */
  protected ContentTranslationDefaultsConfigOverride $configOverrides;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $module_handler = $this->prophesize(ModuleHandlerInterface::class);
    $module_handler->moduleExists('social_content_translation')->willReturn(TRUE);
    $this->configOverrides = new ContentTranslationDefaultsConfigOverride($module_handler->reveal());
  }

  /**
   * @covers ::loadOverrides
   */
  public function testLoadOverrides(): void {
    // There is no override if the config name is wrong.
    $names = [
      'test',
    ];

    $this->assertArrayEquals([], $this->configOverrides->loadOverrides($names));

    // Assert content settings the config gets overriden correctly.
    $names = [
      'language.content_settings.phase.phase',
      'language.content_settings.taxonomy_term.phase_title',
    ];
    foreach ($names as $name) {
      $this->assertArrayEquals(
        [
          $name => [
            'third_party_settings' => [
              'content_translation' => [
                'enabled' => TRUE,
              ],
            ],
          ],
        ],
        $this->configOverrides->loadOverrides([$name])
      );
    }

    $names = [
      'core.base_field_override.taxonomy_term.phase_title.name',
    ];

    // Assert taxonomy term phase title the config gets overriden correctly.
    $this->assertArrayEquals(
      [
        'core.base_field_override.taxonomy_term.phase_title.name' => [
          'translatable' => TRUE,
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

  }

}
