<?php

/**
 * @file
 * Builds placeholder replacement tokens for Social Challenge Phase module.
 */

use Drupal\Core\Link;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\social_challenge_phase\PhaseInterface;
use Drupal\Core\Render\Markup;

/**
 * Implements hook_token_info().
 */
function social_challenge_phase_token_info(): array {
  $type = [
    'name' => t('Social Challenge Phase'),
    'description' => t('Tokens from the Social Challenge Phase module.'),
  ];

  $social_challenge_phase['title'] = [
    'name' => t('Phase title'),
    'description' => t('Phase title.'),
  ];

  $social_challenge_phase['phase-statistics-message-header-section'] = [
    'name' => t('Phase statistics message header'),
    'description' => t('Intro for phase statistics message.'),
  ];

  $social_challenge_phase['phase-statistics-message-custom-message'] = [
    'name' => t('Phase statistic message'),
    'description' => t('The custom message added by CM+ for the Phase.'),
  ];

  $social_challenge_phase['phase-statistics-message-ideas-section'] = [
    'name' => t('Phase statistics message ideas section'),
    'description' => t('The section with ideas info.'),
  ];

  $social_challenge_phase['phase-statistics-message-phase-section'] = [
    'name' => t('Phase statistics message phase section'),
    'description' => t('The section with phase info.'),
  ];

  $social_challenge_phase['phase-statistics-message-next-phase-section'] = [
    'name' => t('Phase statistics message next phase section'),
    'description' => t('The section with next phase info.'),
  ];

  $social_challenge_phase['phase-statistics-message-challenge-section'] = [
    'name' => t('Challenge statistics section'),
    'description' => t('The section with overall challenge info.'),
  ];

  $social_challenge_phase['cta_button'] = [
    'name' => t('CTA button'),
    'description' => t('A call to action button.'),
  ];

  return [
    'types' => ['social_challenge_phase' => $type],
    'tokens' => [
      'social_challenge_phase' => $social_challenge_phase,
    ],
  ];
}

/**
 * Implements hook_tokens().
 *
 * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
 * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
 * @throws \Drupal\Core\Entity\EntityMalformedException
 * @throws \Exception
 */
function social_challenge_phase_tokens(string $type, array $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata): array {
  $replacements = [];

  if ($type === 'social_challenge_phase' && !empty($data['message'])) {
    foreach ($tokens as $name => $original) {
      if ($name === 'title') {
        $object_id = $data['message']->get('field_message_related_object')->target_id;
        if (!empty($object_id)) {
          $entity_storage = \Drupal::entityTypeManager()->getStorage('node');
          /** @var \Drupal\node\Entity\Node|null $idea */
          $idea = $entity_storage->load($object_id);
          if ($idea) {
            $phase = _social_idea_get_phase($idea);
            if ($phase && $phase->label()) {
              $replacements[$original] = $phase->label();
            }
          }
        }
      }
    }
  }

  if ($type === 'social_challenge_phase' && !empty($data['message'])) {
    $phase_id = $data['message']->get('field_message_related_object')->target_id;
    $phase_type = $data['message']->get('field_message_related_object')->target_type;
    if (empty($phase_id) && $phase_type !== 'phase') {
      // We can't replace tokens as Phase can't be loaded.
      return [];
    }

    /** @var \Drupal\social_challenge_phase\Entity\Phase $phase */
    $phase = \Drupal::entityTypeManager()->getStorage('phase')->load($phase_id);
    if (!($phase instanceof PhaseInterface)) {
      // We can't replace tokens as Phase can't be loaded.
      return [];
    }

    /** @var \Drupal\social_challenge\ChallengeWrapper $challenge_wrapper */
    $challenge_wrapper = \Drupal::service('social_challenge.wrapper');

    $challenges = $challenge_wrapper->getChallengesByPhase((string) $phase->id());
    if (empty($challenges)) {
      // We can't replace tokens as Challenge can't be loaded.
      return [];
    }
    /** @var \Drupal\group\Entity\Group $challenge */
    $challenge = reset($challenges);
    $challenge_wrapper->setChallenge($challenge);

    // Get all needed services.
    /** @var \Drupal\Core\Render\RendererInterface $renderer */
    $renderer = \Drupal::service('renderer');
    /** @var \Drupal\Core\Datetime\DateFormatterInterface $date_formatter */
    $date_formatter = \Drupal::service('date.formatter');
    /** @var \Drupal\social_activity\EmailTokenServices $email_token_services */
    $email_token_services = \Drupal::service('social_activity.email_token_services');
    /** @var \Drupal\social_idea\Service\SocialIdeaService $social_idea_service */
    $social_idea_service = \Drupal::service('social_idea.helper');
    /** @var \Drupal\social_challenge_phase\Service\SocialChallengePhaseAdvancingInterface $promoting_service */
    $promoting_service = \Drupal::service('social_challenge_phase.advancing');
    /** @var \Drupal\social_challenge_phase\Service\SocialChallengePhasePermissions $permissions_service */
    $permissions_service = \Drupal::service('social_challenge_phase.permissions');

    // Get next phase if available.
    /** @var \Drupal\social_challenge_phase\Entity\Phase|null $next_phase */
    $next_phase = $promoting_service->getNextPhase($phase);
    $ideas = $promoting_service->getPromotedIdeas($phase);

    foreach ($tokens as $name => $original) {
      switch ($name) {
        case 'phase-statistics-message-header-section':
          $challenge_link ??= Link::fromTextAndUrl((string) $challenge->label(), $challenge->toUrl()->setAbsolute())
            ->toString();

          // Case 1: Challenge continues.
          if ($next_phase) {
            $header_section = t("<p><strong><u>@phase-title</u> of the @challenge-link challenge has ended. Take a look at the new ideas!</strong></p>",
              [
                '@phase-title' => $phase->label(),
                '@challenge-link' => $challenge_link,
              ]);
          }
          // Case 2: Challenge finished.
          else {
            $header_section = t("<p><strong><u>@phase-title</u> of the @challenge-link challenge has ended. This was the last phase of the challenge. @ending</strong></p>",
            [
              '@phase-title' => $phase->label(),
              '@challenge-link' => $challenge_link,
              // @codingStandardsIgnoreStart
              '@ending' => !empty($ideas)
                ? t('Take a look at all the new ideas from the last phase.')
                : '',
              // @codingStandardsIgnoreEnd
            ]);
          }

          $replacements[$original] = Markup::create($header_section);
          break;

        case 'phase-statistics-message-ideas-section':
          // Case 1: Winning Ideas exist.
          if (!empty($ideas)) {
            // Build a list of winning ideas links.
            foreach ($ideas as $idea) {
              $ideas_links[] = [
                '#type' => 'container',
                [
                  '#type' => 'link',
                  '#url' => $idea->toUrl(),
                  '#title' => '- ' . $idea->label(),
                  '#options' => [
                    'absolute' => TRUE,
                  ],
                ],
              ];
            }
            // If all ideas are winning or this is the last phase.
            if ($promoting_service->getPromotingCategory($phase) === 'all' || is_null($next_phase)) {
              // All ideas are winning.
              $ideas_section = t("<p>The selected idea(s) of @phase-title are: @ideas-list</p>",
                [
                  '@phase-title' => $phase->label(),
                  '@ideas-list' => $renderer->renderPlain($ideas_links),
                ]);
            }
            else {
              // Create a message depending on what criteria was used for
              // promoting the ideas to next phase.
              switch ($promoting_service->getPromotingCriteria($phase)) {
                case 'votes_number':
                  $ideas_section = t("<p>The @count most voted ideas were moved to the next phase: @ideas-list</p>",
                    [
                      '@count' => count($ideas),
                      '@ideas-list' => $renderer->renderPlain($ideas_links),
                    ]);
                  break;

                case 'comments_number':
                  $ideas_section = t("<p>The @count most commented ideas were moved to the next phase: @ideas-list</p>",
                    [
                      '@count' => count($ideas),
                      '@ideas-list' => $renderer->renderPlain($ideas_links),
                    ]);
                  break;

                case 'contributors_number':
                  $ideas_section = t("<p>The @count ideas with the most contributors were moved to the next phase: @ideas-list</p>",
                    [
                      '@count' => count($ideas),
                      '@ideas-list' => $renderer->renderPlain($ideas_links),
                    ]);
                  break;

                default:
                  $ideas_section = '';
                  break;
              }
            }
          }
          // Case 2: Winning Ideas don't exist.
          else {
            $ideas_section = '';
          }

          $replacements[$original] = Markup::create($ideas_section);
          break;

        case 'phase-statistics-message-phase-section':
          $challenge_link ??= Link::fromTextAndUrl((string) $challenge->label(), $challenge->toUrl()->setAbsolute())->toString();
          $default_timezone = \Drupal::config('system.date')->get('timezone')['default'] ?? 'UTC';

          // Get placeholders for translatable string.
          $placeholders = [
            '@challenge-link' => $challenge_link,
            '@challenge-members' => count($challenge->getMembers()),
            '@phase-start-date' => $date_formatter
              ->format($phase->get('field_phase_date')->start_date->getTimestamp(), 'social_medium_date', $default_timezone),
            '@phase-end-date' => $date_formatter
              ->format($phase->get('field_phase_date')->end_date->getTimestamp(), 'social_medium_date', $default_timezone),
            // Get time formatted string like "UTC +2:00".
            '@timezone' => $date_formatter
              ->format(time(), 'custom', 'e P', $default_timezone),
            '@ideas-total' => $social_idea_service->getIdeasByPhaseCount($phase),
            '@comments-total' => $social_idea_service->getIdeasCommentsByPhaseCount($phase),
          ];

          // Case 1: Challenge continues.
          if ($next_phase) {
            $phase_section = t("<p><strong>Facts about the challenge @challenge-link:</strong></p><p>@challenge-members people are participating in this challenge.</p><p>This Phase was active from @phase-start-date to @phase-end-date (@timezone).</p><p>@ideas-total ideas were shared.</p><p>@comments-total comments were exchanged.</p>", $placeholders);
          }
          // Case 2: Challenge finished.
          else {
            $phase_section = t("<p><strong>Summary of the phase:</strong></p><p>@challenge-members people are participated in this phase.</p><p>This Phase was active from @phase-start-date to @phase-end-date (@timezone).</p><p>@ideas-total ideas were shared.</p><p>@comments-total comments were exchanged.</p>", $placeholders);
          }

          $replacements[$original] = Markup::create($phase_section);
          break;

        case 'phase-statistics-message-next-phase-section':
          // Case 1: The next phase exists.
          if ($next_phase) {

            $next_phase_permissions = $permissions_service->getPhaseAllowedPermissions($next_phase);
            foreach ($next_phase_permissions as $permission) {
              $next_phase_permissions_render_array[] = [
                '#type' => 'html_tag',
                '#tag' => 'div',
                '#value' => '- ' . $permission,
              ];
            }

            $next_phase_section = t("<p>______</p><p><strong>The Next Phase: @next-phase</strong></p><p>In this next phase, you’ll be able to:</p><p>@next-phase-permissions</p>", [
              '@next-phase' => $next_phase->label(),
              // @codingStandardsIgnoreStart
              '@next-phase-permissions' => !empty($next_phase_permissions_render_array)
                ? $renderer->render($next_phase_permissions_render_array)
                : '',
              // @codingStandardsIgnoreEnd
            ]);
          }
          // Case 2: The next phase doesn't exist.
          else {
            $next_phase_section = '';
          }

          $replacements[$original] = Markup::create($next_phase_section);
          break;

        case 'phase-statistics-message-custom-message':
          $render_data = $phase->get('field_phase_stat_message')->value;
          $replacements[$original] = Markup::create($render_data);
          break;

        case 'phase-statistics-message-challenge-section':
          // Case 1: Challenge continues.
          if ($next_phase) {
            $challenge_section = '';
          }
          // Case 2: Challenge finished.
          else {
            /** @var \Drupal\social_challenge_phase\Entity\Phase[] $phases */
            $phases = $challenge_wrapper->getPhases();
            // Reindex array keys.
            $phases = array_values($phases);

            $winning_ideas_by_phase = [
              '#type' => 'container',
            ];

            foreach ($phases as $key => $phase) {
              $ideas = $promoting_service->getPromotedIdeas($phase);
              if (empty($ideas)) {
                // Probably, automate promotion for ideas in phase was disabled
                // and ideas were promoted manually by challenge managers.
                continue;
              }

              $winning_ideas_by_phase[] = [
                '#type' => 'html_tag',
                '#tag' => 'span',
                '#value' => $key + 1 . '. ' . $phase->label(),
              ];

              foreach ($ideas as $idea) {
                $winning_ideas_by_phase[] = [
                  [
                    '#type' => 'container',
                    [
                      '#type' => 'link',
                      '#url' => $idea->toUrl(),
                      '#title' => '- ' . $idea->label(),
                      '#options' => [
                        'absolute' => TRUE,
                      ],
                    ],
                  ],
                ];
              }
            }

            $challenge_section = t("<p>______</p><p><strong>Facts about the challenge:</strong><br><p>This challenge had @phase-total phases</p><div>The selected ideas from each phase were:</div>@winning-ideas-by-phase<p></p><p>@challenge-members people are participated in this challenge.</p><p>@ideas-total ideas were shared.</p><p>@comments-total comments were exchanged.</p><p>@votes-total total votes were cast</p></p>",
              [
                '@phase-total' => count($challenge_wrapper->getPhases()),
                '@winning-ideas-by-phase' => $renderer->renderPlain($winning_ideas_by_phase),
                '@challenge-members' => count($challenge->getMembers()),
                '@ideas-total' => $social_idea_service->getIdeasByChallengeCount($challenge),
                '@comments-total' => $social_idea_service->getIdeasCommentsByChallengeCount($challenge),
                '@votes-total' => $social_idea_service->getTotalIdeasVotesByChallengeCount($challenge),
              ]);
          }

          $replacements[$original] = Markup::create($challenge_section);
          break;

        case 'cta_button':
          // Case 1: Challenge continues.
          if ($next_phase) {
            $cta_button_text = '';
          }
          // Case 2: Challenge finished.
          else {
            $link = $challenge->toUrl()->setAbsolute();
            $cta_button = $email_token_services->getCtaButton($link, t('See completed challenge'));
            $cta_button_text = $renderer->renderPlain($cta_button);
          }

          $replacements[$original] = $cta_button_text;
          break;
      }
    }
  }

  return $replacements;
}
