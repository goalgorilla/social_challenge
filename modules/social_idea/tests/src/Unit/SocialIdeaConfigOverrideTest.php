<?php

namespace Drupal\Tests\social_challenge\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\social_idea\SocialIdeaConfigOverride;

/**
 * Social idea config override test.
 *
 * @coversDefaultClass \Drupal\social_idea\SocialIdeaConfigOverride
 * @group social_idea
 */
class SocialIdeaConfigOverrideTest extends UnitTestCase {

  /**
   * Default configuration.
   *
   * @var array[]
   */
  protected array $defaultConfig = [
    'block.block.socialblue_groupheroblock' => [
      'visibility' => [
        'request_path' => [
          'pages' => '/test-page',
        ],
      ],
    ],
    'social_vote.settings' => [
      'enabled_types' => [
        'node' => [
          'test_bundle',
        ],
      ],
      'allow_cancel_vote' => FALSE,
      'hide_vote_widget' => TRUE,
    ],
    'views.view.search_all' => [
      'display' => [
        'default' => [
          'display_options' => [
            'row' => [
              'options' => [
                'view_modes' => [
                  'entity:node' => [
                    'test_group' => 'teaser',
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
    ],
    'views.view.search_content' => [
      'display' => [
        'default' => [
          'display_options' => [
            'row' => [
              'options' => [
                'view_modes' => [
                  'entity:node' => [
                    'test_group' => 'teaser',
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
    ],
    'search_api.index.social_all' => [
      'field_settings' => [
        'rendered_item' => [
          'configuration' => [
            'view_mode' => [
              'entity:node' => [
                'test_bundle' => 'search_index',
              ],
            ],
          ],
        ],
      ],
    ],
    'search_api.index.social_content' => [
      'field_settings' => [
        'rendered_item' => [
          'configuration' => [
            'view_mode' => [
              'entity:node' => [
                'test_bundle' => 'search_index',
              ],
            ],
          ],
        ],
      ],
    ],
  ];

  /**
   * SocialIdeaConfigOverride.
   */
  protected SocialIdeaConfigOverride $configOverrides;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->configOverrides = new SocialIdeaConfigOverride($this->getConfigFactoryStub($this->defaultConfig));
  }

  /**
   * @covers ::loadOverrides
   */
  public function testLoadOverrides(): void {
    // There is no override if the config name is wrong.
    $names = [
      'test',
    ];

    $this->assertArrayEquals([], $this->configOverrides->loadOverrides($names));

    // Assert page visibility for block socialblue_profile_hero_block gets
    // overriden correctly.
    $names = [
      'block.block.socialblue_groupheroblock',
    ];
    $expected_pages = "/test-page\r\n/group/*/ideas";

    $this->assertArrayEquals(
      [
        'block.block.socialblue_groupheroblock' => [
          'visibility' => [
            'request_path' => [
              'pages' => $expected_pages,
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    // Assert social vote settings gets overriden correctly.
    $names = [
      'social_vote.settings',
    ];
    $expected_bundles = [
      'test_bundle',
      'social_idea',
    ];

    $this->assertArrayEquals(
      [
        'social_vote.settings' => [
          'enabled_types' => [
            'node' => $expected_bundles,
          ],
          'allow_cancel_vote' => TRUE,
          'hide_vote_widget' => FALSE,
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    // Assert view mode for search_all and search_groups views the config gets
    // overriden correctly.
    $names = [
      'views.view.search_all',
      'views.view.search_content',
    ];
    foreach ($names as $name) {
      $expected_bundles = [
        'test_group' => 'teaser',
        'social_idea' => 'teaser',
      ];

      $this->assertArrayEquals(
        [
          $name => [
            'display' => [
              'default' => [
                'display_options' => [
                  'row' => [
                    'options' => [
                      'view_modes' => [
                        'entity:node' => $expected_bundles,
                      ],
                    ],
                  ],
                ],
              ],
            ],
          ],
        ],
        $this->configOverrides->loadOverrides([$name])
      );
    }

    // Assert search api index for social_all and search_groups the config gets
    // overriden correctly.
    $names = [
      'search_api.index.social_all',
      'search_api.index.social_content',
    ];
    $expected_bundles = [
      'test_bundle' => 'search_index',
      'social_idea' => 'search_index',
    ];

    foreach ($names as $name) {
      $this->assertArrayEquals(
        [
          $name => [
            'field_settings' => [
              'rendered_item' => [
                'configuration' => [
                  'view_mode' => [
                    'entity:node' => $expected_bundles,
                  ],
                ],
              ],
            ],
          ],
        ],
        $this->configOverrides->loadOverrides([$name])
      );
    }
  }

}
