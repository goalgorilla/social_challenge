<?php

namespace Drupal\Tests\social_idea\Unit;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\social_idea\ContentTranslationDefaultsConfigOverride;

/**
 * Content translation defaults config override test.
 *
 * @coversDefaultClass \Drupal\social_idea\ContentTranslationDefaultsConfigOverride
 * @group social_idea
 */
class ContentTranslationDefaultsConfigOverrideTest extends UnitTestCase {

  /**
   * ContentTranslationDefaultsConfigOverride.
   *
   * @var \Drupal\social_idea\ContentTranslationDefaultsConfigOverride|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configOverrides;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $module_handler = $this->prophesize(ModuleHandlerInterface::class);
    $module_handler->moduleExists('social_content_translation')->willReturn(TRUE);
    $this->configOverrides = new ContentTranslationDefaultsConfigOverride($module_handler->reveal());
  }

  /**
   * @covers ::loadOverrides
   */
  public function testLoadOverrides() {
    // There is no override if the config name is wrong.
    $names = [
      'test',
    ];

    $this->assertArrayEquals([], $this->configOverrides->loadOverrides($names));

    $names = [
      'language.content_settings.node.social_idea',
    ];

    // Assert content settings the config gets overriden correctly.
    $this->assertArrayEquals(
      [
        'language.content_settings.node.social_idea' => [
          'third_party_settings' => [
            'content_translation' => [
              'enabled' => TRUE,
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    $names = [
      'field.field.node.social_idea.field_social_idea_image',
    ];

    // Assert idea fields config gets overriden correctly.
    $this->assertArrayEquals(
      [
        'field.field.node.social_idea.field_social_idea_image' => [
          'third_party_settings' => [
            'content_translation' => [
              'translation_sync' => [
                'file' => 'file',
                'alt' => '0',
                'title' => '0',
              ],
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    $names = [
      'core.base_field_override.node.social_idea.path',
      'core.base_field_override.node.social_idea.title',
      'core.base_field_override.node.social_idea.menu_link',
    ];
    foreach ($names as $name) {
      // Assert idea fields config gets overriden correctly.
      $this->assertArrayEquals(
        [
          $name => [
            'translatable' => TRUE,
          ],
        ],
        $this->configOverrides->loadOverrides([$name])
      );
    }

  }

}
