<?php

namespace Drupal\social_idea;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a helper class, sets permissions.
 */
class SocialIdeaInstallHelper implements ContainerInjectionInterface {

  /**
   * Entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs SocialChallengeInstallHelper.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Set default permissions.
   */
  public function setPermissions(): void {
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();

    /** @var \Drupal\user\Entity\Role $role */
    foreach ($roles as $role) {
      if ($role->id() === 'administrator') {
        continue;
      }

      $permissions = $this->getPermissions($role->id());
      user_role_grant_permissions($role->id(), $permissions);
    }
  }

  /**
   * Get default permissions.
   */
  public function getPermissions($role) {
    // Anonymous.
    $permissions[RoleInterface::ANONYMOUS_ID] = [
      'view node.social_idea.field_content_visibility:public content',
    ];

    // Authenticated.
    $permissions[RoleInterface::AUTHENTICATED_ID] = array_merge($permissions[RoleInterface::ANONYMOUS_ID], [
      'edit own social_idea content',
      'delete own social_idea content',
      'override social_idea published option',
      'add or remove vote votes on social_idea of node',
      'view node.social_idea.field_content_visibility:community content',
      'view node.social_idea.field_content_visibility:group content',
    ]);

    // Content manager.
    $permissions['contentmanager'] = array_merge($permissions[RoleInterface::AUTHENTICATED_ID], [
      'delete any social_idea content',
      'edit any social_idea content',
      'view social_idea revisions',
      'delete social_idea revisions',
      'revert social_idea revisions',
      'override social_idea revision log entry',
      'override social_idea authored by option',
      'override social_idea authored on option',
      'override social_idea promote to front social_idea option',
      'override social_idea revision option',
      'override social_idea sticky option',
    ]);

    // Site manager.
    $permissions['sitemanager'] = array_merge($permissions['contentmanager'], []);

    return $permissions[$role] ?? [];
  }

}
