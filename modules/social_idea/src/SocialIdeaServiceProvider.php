<?php

namespace Drupal\social_idea;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Symfony\Component\DependencyInjection\Reference;
use Drupal\social_idea\Service\SocialIdeaService;

/**
 * Implements the SocialIdeaServiceProvider class.
 *
 * @package Drupal\social_idea
 */
class SocialIdeaServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {
    $modules = $container->getParameter('container.modules');
    // If inject the service without checking on module existing
    // the fatal error with rise when installing "Social Challenge" module.
    if ($modules['social_challenge']) {
      $container->getDefinition('social_idea.helper')
        ->addArgument(new Reference('social_challenge.wrapper'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container): void {
    $container->register('social_idea.helper', SocialIdeaService::class)->addArgument(new Reference('entity_type.manager'));
  }

}
