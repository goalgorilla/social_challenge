<?php

namespace Drupal\social_idea;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class SocialLandingPageConfigOverride.
 *
 * Example configuration override.
 *
 * @package Drupal\social_idea
 */
class SocialIdeaConfigOverride implements ConfigFactoryOverrideInterface {

  /**
   * The config factory service.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructs SocialResourceLibrarySuggestInstallHelper.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config storage.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('config.factory'),
    );
  }

  /**
   * Load overrides.
   */
  public function loadOverrides($names): array {
    $overrides = [];

    // Show group hero block on Ideas page.
    $config_name = 'block.block.socialblue_groupheroblock';

    $config = $this->configFactory->getEditable($config_name);
    $pages = $config->get('visibility.request_path.pages');
    $pages .= "\r\n/group/*/ideas";

    if (in_array($config_name, $names)) {
      $overrides[$config_name] = [
        'visibility' => [
          'request_path' => [
            'pages' => $pages,
          ],
        ],
      ];
    }

    // Add Idea to Social Vote settings.
    $config_name = 'social_vote.settings';

    if (in_array($config_name, $names)) {
      $config = $this->configFactory->getEditable($config_name);
      $bundles = is_array($config->get('enabled_types.node')) ? $config->get('enabled_types.node') : [];
      if (!in_array('social_idea', $bundles)) {
        array_push($bundles, 'social_idea');
      }

      $overrides[$config_name] = [
        'enabled_types' => [
          'node' => $bundles,
        ],
        'allow_cancel_vote' => TRUE,
        'hide_vote_widget' => FALSE,
      ];
    }

    // Add teaser view mode for Idea to Search views.
    $config_names = [
      'views.view.search_all',
      'views.view.search_content',
    ];

    foreach ($config_names as $config_name) {
      if (in_array($config_name, $names)) {
        $config = $this->configFactory->getEditable($config_name);
        $bundles = $config->get('display.default.display_options.row.options.view_modes.entity:node');
        $bundles['social_idea'] = 'teaser';

        $overrides[$config_name] = [
          'display' => [
            'default' => [
              'display_options' => [
                'row' => [
                  'options' => [
                    'view_modes' => [
                      'entity:node' => $bundles,
                    ],
                  ],
                ],
              ],
            ],
          ],
        ];
      }
    }

    // Add Social Ideas to Search indexes.
    $config_names = [
      'search_api.index.social_all',
      'search_api.index.social_content',
    ];

    foreach ($config_names as $config_name) {
      if (in_array($config_name, $names)) {
        $config = $this->configFactory->getEditable($config_name);
        $bundles = $config->get('field_settings.rendered_item.configuration.view_mode.entity:node');
        $bundles['social_idea'] = 'search_index';

        $overrides[$config_name] = [
          'field_settings' => [
            'rendered_item' => [
              'configuration' => [
                'view_mode' => [
                  'entity:node' => $bundles,
                ],
              ],
            ],
          ],
        ];
      }
    }

    // Add compatibility with social grid search.
    if ($this->configFactory->getEditable('social_search_grid.settings')->get('entities.node.bundles.event.status')) {
      $config_name = 'views.view.search_all_grid';

      $overrides[$config_name] = [
        'display' => [
          'default' => [
            'display_options' => [
              'fields' => [
                'search_api_rendered_item' => [
                  'view_modes' => [
                    'entity:node' => [
                      'social_idea' => 'small_teaser',
                    ],
                  ],
                ],
              ],
            ],
          ],
        ],
      ];
    }

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix(): string {
    return 'SocialIdeaConfigOverride';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name): CacheableMetadata {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}
