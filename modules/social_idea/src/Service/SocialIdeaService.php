<?php

namespace Drupal\social_idea\Service;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\social_challenge\ChallengeWrapper;
use Drupal\social_challenge_phase\PhaseInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Helper service for Social Idea.
 *
 * @package Drupal\social_idea\Service
 */
class SocialIdeaService implements ContainerInjectionInterface {

  /**
   * Entity type manager.
   */
  private EntityTypeManagerInterface $entityTypeManager;

  /**
   * The challenge wrapper.
   */
  private ChallengeWrapper $challengeWrapper;

  /**
   * SocialIdeaService constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\social_challenge\ChallengeWrapper $challenge_wrapper
   *   The challenge wrapper.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ChallengeWrapper $challenge_wrapper) {
    $this->entityTypeManager = $entity_type_manager;
    $this->challengeWrapper = $challenge_wrapper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('social_challenge.wrapper')
    );
  }

  /**
   * Get ideas belongs to the phase.
   *
   * @param \Drupal\social_challenge_phase\PhaseInterface $phase
   *   Current phase or null.
   *
   * @return \Drupal\node\NodeInterface[]
   *   An array of Ideas node entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getIdeasByPhase(PhaseInterface $phase): array {
    /** @var \Drupal\node\NodeInterface[] $ideas */
    $ideas = $this->entityTypeManager
      ->getStorage('node')
      ->loadByProperties([
        'type' => 'social_idea',
        'status' => TRUE,
        'field_social_idea_phases' => $phase->id(),
      ]);

    return $ideas ?: [];
  }

  /**
   * Count ideas belongs to the phase.
   *
   * @param \Drupal\social_challenge_phase\PhaseInterface $phase
   *   Current phase.
   *
   * @return int
   *   A number of ideas.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getIdeasByPhaseCount(PhaseInterface $phase): int {
    return count($this->getIdeasByPhase($phase));
  }

  /**
   * Count ideas comments belongs to the phase.
   *
   * @param \Drupal\social_challenge_phase\PhaseInterface $phase
   *   Current phase.
   *
   * @return int
   *   A number of ideas comments.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getIdeasCommentsByPhaseCount(PhaseInterface $phase): int {
    $count = 0;

    foreach ($this->getIdeasByPhase($phase) as $idea) {
      $count += $idea->get('field_social_idea_comments')->comment_count;
    }

    return $count;
  }

  /**
   * Get ideas belongs to the challenge.
   *
   * @param \Drupal\group\Entity\GroupInterface $challenge
   *   Challenge entity.
   *
   * @return \Drupal\node\NodeInterface[]
   *   An array of Ideas node entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getIdeasByChallenge(GroupInterface $challenge): array {
    $this->challengeWrapper->setChallenge($challenge);

    $ideas = [];
    foreach ($this->challengeWrapper->getPhases() as $phase) {
      /** @var \Drupal\social_challenge_phase\PhaseInterface $phase */
      $ideas += $this->getIdeasByPhase($phase);
    }

    return $ideas;
  }

  /**
   * Count ideas belongs to the challenge.
   *
   * @param \Drupal\group\Entity\GroupInterface $challenge
   *   Challenge entity.
   *
   * @return int
   *   A number of ideas.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getIdeasByChallengeCount(GroupInterface $challenge): int {
    return count($this->getIdeasByChallenge($challenge));
  }

  /**
   * Count ideas comments belongs to the challenge.
   *
   * @param \Drupal\group\Entity\GroupInterface $challenge
   *   Challenge entity.
   *
   * @return int
   *   A number of ideas comments.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getIdeasCommentsByChallengeCount(GroupInterface $challenge): int {
    $count = 0;

    foreach ($this->getIdeasByChallenge($challenge) as $idea) {
      $count += $idea->get('field_social_idea_comments')->comment_count;
    }

    return $count;
  }

  /**
   * Count votes for ideas belongs to the challenge.
   *
   * @param \Drupal\group\Entity\GroupInterface $challenge
   *   Challenge entity.
   *
   * @return int
   *   A number of votes.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getTotalIdeasVotesByChallengeCount(GroupInterface $challenge): int {
    $totalVotes = 0;
    foreach ($this->getIdeasByChallenge($challenge) as $idea) {
      if (social_vote_is_enabled($idea)) {
        $totalVotes += social_vote_get_votes($idea);
      }
    }

    return $totalVotes;
  }

  /**
   * Get most voted ideas belongs to the phase.
   *
   * @param \Drupal\social_challenge_phase\PhaseInterface $phase
   *   Current phase or null.
   *
   * @return \Drupal\node\NodeInterface[]
   *   An array Idea node entities.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getMostVotedIdeasByPhase(PhaseInterface $phase): array {
    $ideas = $this->getIdeasByPhase($phase);
    $previousIdeasVotesCount = 0;
    foreach ($ideas as $idea) {
      $currentIdeaVotesCount = social_vote_is_enabled($idea)
        ? social_vote_get_votes($idea)
        : 0;

      if ($currentIdeaVotesCount > $previousIdeasVotesCount) {
        $bestIdeas = [$idea];
      }
      // There can be multiple Ideas with the same votes number and there are
      // no criteria to select the best one.
      elseif ($currentIdeaVotesCount === $previousIdeasVotesCount) {
        $bestIdeas[] = $idea;
      }

      $previousIdeasVotesCount = $currentIdeaVotesCount;
    }

    return $bestIdeas ?? [];
  }

}
