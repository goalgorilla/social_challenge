<?php

namespace Drupal\social_idea\Plugin\UserExportPlugin;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\social_user_export\Plugin\UserExportPluginBase;
use Drupal\user\UserInterface;

/**
 * Provides a 'UserAnalyticsIdeasCreated' user export row.
 *
 * @UserExportPlugin(
 *  id = "user_ideas_created",
 *  label = @Translation("Ideas created"),
 *  weight = -237,
 * )
 */
class UserAnalyticsIdeasCreated extends UserExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader(): TranslatableMarkup {
    return $this->t('Ideas created');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(UserInterface $entity): int {
    $query = $this->database->select('node', 'n');
    $query->join('node_field_data', 'nfd', 'nfd.nid = n.nid');
    $query
      ->condition('nfd.uid', $entity->id())
      ->condition('nfd.type', 'social_idea');

    return (int) $query
      ->countQuery()
      ->execute()
      ->fetchField();
  }

}
