<?php

namespace Drupal\social_idea\Plugin\ActivityContext;

use Drupal\activity_creator\Plugin\ActivityContextBase;
use Drupal\node\NodeInterface;
use Drupal\user\UserInterface;

/**
 * Provides a 'SocialIdeaActivityContext' activity context.
 *
 * @ActivityContext(
 *   id = "social_idea_activity_context",
 *   label = @Translation("Challenge phase activity context"),
 * )
 */
class SocialIdeaActivityContext extends ActivityContextBase {

  /**
   * {@inheritdoc}
   */
  public function getRecipients(array $data, $last_uid, $limit): array {
    $recipients = [];

    // We only know the context if there is a related object.
    if (isset($data['related_object']) && !empty($data['related_object'])) {
      if ($data['related_object'][0]['target_type'] === 'node') {

        switch ($data['message_template']) {
          case 'update_challenge_phase_followers':
            $recipients = $this->getRecipientsWhoFollowContent($data);
            break;

          case 'update_challenge_phase_authors':
            $recipients = $this->getRecipientsCoauthors($data);
            break;

          case 'update_challenge_idea_authors':
            $recipients = $this->getRecipientsCoauthors($data);
            // Remove the actor (user performing action) from recipients list.
            if (!empty($data['actor'])) {
              $key = array_search($data['actor'], array_column($recipients, 'target_id'), FALSE);
              if ($key !== FALSE) {
                unset($recipients[$key]);
              }
            }
            break;
        }

      }
    }

    return $recipients;
  }

  /**
   * Returns idea coauthors.
   *
   * @param array $data
   *   The data.
   *
   *   An associative array of recipients, containing the following key-value
   *   pairs:
   *   - target_type: The entity type ID.
   *   - target_id: The entity ID.
   */
  public function getRecipientsCoauthors(array $data): array {
    $recipients = [];

    foreach ($this->getCoauthors($data) as $co_author) {
      $recipients[] = [
        'target_type' => 'user',
        'target_id' => $co_author,
      ];
    }

    return $recipients;
  }

  /**
   * Returns idea coauthors.
   *
   * @param array $data
   *   The data.
   *
   *   An array of coauthors ids.
   */
  public function getCoauthors(array $data): array {
    $related_object = $data['related_object'][0];
    $storage = $this->entityTypeManager->getStorage($related_object['target_type']);
    /** @var \Drupal\node\NodeInterface $node **/
    $node = $storage->load($related_object['target_id']);
    if (empty($node)) {
      return [];
    }

    $owner_id = $node->getOwner()->id();
    $coauthors[] = $owner_id;

    if ($node instanceof NodeInterface && $node->hasField('field_social_idea_coauthors')) {
      $coauthors_field = $node->get('field_social_idea_coauthors')->getValue();
      if (!empty($coauthors_field)) {
        foreach ($coauthors_field as $co_author) {
          if ($co_author['target_id'] !== $owner_id) {
            $coauthors[] = $co_author['target_id'];
          }
        }
      }
    }

    return $coauthors;
  }

  /**
   * Returns recipient who follow content.
   *
   * @param array $data
   *   The data.
   *
   *   An associative array of recipients, containing the following key-value
   *   pairs:
   *   - target_type: The entity type ID.
   *   - target_id: The entity ID.
   */
  public function getRecipientsWhoFollowContent(array $data): array {
    $recipients = [];
    $original_related_object = $data['related_object'][0];
    $storage = $this->entityTypeManager->getStorage('flagging');
    $flaggings = $storage->loadByProperties([
      'flag_id' => 'follow_content',
      'entity_type' => $original_related_object['target_type'],
      'entity_id' => $original_related_object['target_id'],
    ]);

    $original_related_object = $data['related_object'][0];
    $storage = $this->entityTypeManager->getStorage($original_related_object['target_type']);
    $node = $storage->load($original_related_object['target_id']);

    if ($node instanceof NodeInterface) {
      foreach ($flaggings as $flagging) {
        /** @var \Drupal\flag\FlaggingInterface $flagging */
        $recipient = $flagging->getOwner();

        if (!$recipient instanceof UserInterface) {
          break;
        }

        $coauthors_ids = $this->getCoauthors($data);

        if (!in_array($recipient->id(), $coauthors_ids) && $node->access('view', $recipient)) {
          $recipients[] = [
            'target_type' => 'user',
            'target_id' => $recipient->id(),
          ];
        }

        return $recipients;
      }
    }

    return $recipients;
  }

}
