<?php

/**
 * @file
 * The Social Idea module.
 */

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Form\FormStateInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\node\NodeInterface;
use Drupal\social_challenge_phase\Entity\Phase;

/**
 * Implements hook_theme().
 */
function social_idea_theme(): array {
  $theme_templates['node__social_idea__activity'] = [
    'base hook' => 'node',
  ];
  $theme_templates['node__social_idea__activity_comment'] = [
    'base hook' => 'node',
  ];
  $theme_templates['node__social_idea__featured'] = [
    'base hook' => 'node',
  ];
  $theme_templates['node__social_idea__full'] = [
    'base hook' => 'node',
  ];
  $theme_templates['node__social_idea__teaser'] = [
    'base hook' => 'node',
  ];

  // Sky.
  $theme_templates['node__social_idea__activity__sky'] = [
    'base hook' => 'node',
  ];
  $theme_templates['node__social_idea__activity_comment__sky'] = [
    'base hook' => 'node',
  ];
  $theme_templates['node__social_idea__featured__sky'] = [
    'base hook' => 'node',
  ];
  $theme_templates['node__social_idea__full__sky'] = [
    'base hook' => 'node',
  ];
  $theme_templates['node__social_idea__hero__sky'] = [
    'base hook' => 'node',
  ];
  $theme_templates['node__social_idea__teaser__sky'] = [
    'base hook' => 'node',
  ];

  return $theme_templates;
}

/**
 * Implements hook_node_links_alter().
 */
function social_idea_node_links_alter(array &$links, NodeInterface $entity, array &$context) {
  unset(
    $links['comment__field_social_idea_comments']['#links']['comment-add'],
    $links['comment__field_social_idea_comments']['#links']['comment-comments'],
    $links['comment__field_social_idea_comments']['#links']['comment-forbidden']
  );
}

/**
 * Implements hook_form_alter().
 */
function social_idea_form_alter(array &$form, FormStateInterface $form_state, string $form_id): void {
  switch ($form_id) {
    case 'node_social_idea_form':
    case 'node_social_idea_edit_form':
      // Set max length for summary to 200 characters.
      if (isset($form['field_social_idea_summary']['widget'][0]['value']['#attributes'])) {
        $form['field_social_idea_summary']['widget'][0]['value']['#attributes']['maxlength'] = 200;
      }
      // Disable comment settings for all users. Should be controlled by phases.
      $form['field_social_idea_comments']['#access'] = FALSE;

      $group = _social_group_get_current_group();
      if ($group instanceof GroupInterface) {
        /** @var \Drupal\social_challenge\ChallengeWrapper $challenge_wrapper */
        $challenge_wrapper = \Drupal::service('social_challenge.wrapper');
        $challenge_wrapper->setChallenge($group);

        // Only CM+ can edit idea phase tags.
        $current_user = \Drupal::currentUser();
        if (!$group->hasPermission('administer phase permissions', $current_user)) {
          $form['field_social_idea_phases']['#access'] = FALSE;
        }
        else {
          $challenge_phases = $challenge_wrapper->getPhases();
          if (isset($form['field_social_idea_phases']) && is_array($form['field_social_idea_phases']['widget']['#options']) && is_array($challenge_phases)) {
            $allowed_options = array_intersect_key($form['field_social_idea_phases']['widget']['#options'], $challenge_phases);
            $form['field_social_idea_phases']['widget']['#options'] = $allowed_options;
          }
        }

        // Ideas automatically get a phase they are created in.
        if ($form_id === 'node_social_idea_form') {
          $active_phase = $challenge_wrapper->getActivePhase();
          if ($active_phase instanceof Phase && isset($form['field_social_idea_phases']['widget'])) {
            $form['field_social_idea_phases']['widget']['#default_value'] = [$active_phase->id()];
          }
        }

        if (
          \Drupal::configFactory()
            ->get('social_tagging.settings')
            ->get('relating_challenge_tags') &&
          isset($form['tagging'])
        ) {
          foreach ($form['tagging'] as &$tagging_item) {
            if (
              !is_array($tagging_item) ||
              !isset($tagging_item['#options']) || !isset($tagging_item['#type']) ||
              ($tagging_item['#type'] !== 'select' && $tagging_item['#type'] !== 'select2')
            ) {
              continue;
            }

            $options = [];
            $all_options = $tagging_item['#options'];
            if (!$group->get('social_tagging')->isEmpty()) {
              $parent_options = $group->get('social_tagging')->getValue();
              array_walk($parent_options, static function (&$item) {
                $item = $item['target_id'];
              });

              foreach ($all_options as $key => $item) {
                if (in_array($key, $parent_options) && array_key_exists($key, $tagging_item['#options'])) {
                  $options[$key] = $item;
                }
              }
            }

            $default_values = $tagging_item['#default_value'];
            if (!empty($default_values)) {
              foreach ($default_values as $key) {
                if (!array_key_exists($key, $options) && array_key_exists($key, $tagging_item['#options'])) {
                  $options[$key] = $all_options[$key];
                }
              }
            }

            $tagging_item['#options'] = $options;
          }
        }
      }
      if (!empty($form['field_social_idea_phases'])) {
        $form['field_social_idea_phases']['widget']['#title'] = t('Phases settings');
        $form['field_social_idea_phases']['widget']['#description']
          = t('Select the phases this idea should belong to');
      }
      break;

    case 'social_tagging_settings':
      if (isset($form['node_type_settings']['tag_node_type_social_idea'])) {
        $form['node_type_settings']['idea_wrapper']['tag_node_type_social_idea'] = $form['node_type_settings']['tag_node_type_social_idea'];
        unset($form['node_type_settings']['tag_node_type_social_idea']);

        $form['node_type_settings']['idea_wrapper']['idea_settings'] = [
          '#type' => 'details',
          '#title' => t('Idea settings'),
          '#open' => TRUE,
          '#states' => [
            'visible' => [
              ':input[name="tag_node_type_social_idea"]' => ['checked' => TRUE],
            ],
          ],
        ];

        $config = \Drupal::configFactory()->get('social_tagging.settings');
        $form['node_type_settings']['idea_wrapper']['idea_settings']['relating_challenge_tags'] = [
          '#type' => 'checkbox',
          '#title' => t('Users will be able to add only tags added to related challenge'),
          '#default_value' => $config->get('relating_challenge_tags'),
        ];

        $form['#submit'][] = '_social_idea_save_additional_tagging_settings';
      }
      break;
  }

  $forms = [];
  foreach (_social_group_default_route_group_types() as $type) {
    $forms[] = 'group_' . $type . '_edit_form';
    $forms[] = 'group_' . $type . '_add_form';
  }

  // Make sure we only run this code on group add/edit forms.
  if (in_array($form_id, $forms)) {
    $forms_ideas_tab = [
      'group_challenge_edit_form',
      'group_challenge_add_form',
      'group_sc_add_form',
      'group_sc_edit_form',
      'group_cc_add_form',
      'group_cc_edit_form',
    ];
    // Only allow the ideas tab on challenge groups.
    if (!in_array($form_id, $forms_ideas_tab)) {
      unset($form['tab_settings']['default_route']['#options']['view.challenge_ideas.page'], $form['tab_settings']['default_route_an']['#options']['view.challenge_ideas.page']);
    }
  }

}

/**
 * Save additional tagging settings for the idea configuration.
 */
function _social_idea_save_additional_tagging_settings(array $form, FormStateInterface $form_state): void {
  $idea_setting = $form_state->getValue('tag_node_type_social_idea');
  \Drupal::configFactory()->getEditable('social_tagging.settings')
    ->set('relating_challenge_tags', $idea_setting ? $form_state->getValue('relating_challenge_tags') : FALSE)
    ->save();
}

/**
 * Implements hook_module_implements_alter().
 */
function social_idea_module_implements_alter(array &$implementations, string $hook): void {
  if ($hook === 'form_alter') {
    $group = $implementations['social_idea'];
    unset($implementations['social_idea']);
    $implementations['social_idea'] = $group;
  }
}

/**
 * Implements hook_menu_local_tasks_alter().
 */
function social_idea_menu_local_tasks_alter(array &$data, string $route_name): void {
  $group = _social_group_get_current_group();

  if ($group instanceof GroupInterface) {
    /** @var \Drupal\social_challenge\ChallengeWrapper $challenge_wrapper */
    $challenge_wrapper = \Drupal::service('social_challenge.wrapper');
    $bundles = $challenge_wrapper::getAvailableBundles();

    if (!in_array($group->bundle(), $bundles) && isset($data['tabs'][0]['social_idea.challenge_ideas'])) {
      $data['tabs'][0]['social_idea.challenge_ideas']['#access'] = AccessResult::forbidden();
    }
  }
}

/**
 * Implements hook_preprocess_flag().
 */
function social_idea_preprocess_flag(array &$variables): void {
  // Rename 'Follow content' to 'Follow Idea'.
  $node = $variables['flaggable'];
  if ($node instanceof NodeInterface && $node->getType() === 'social_idea') {
    $action = $variables['action'] === 'unflag' ? t('Unfollow') : t('Follow');
    $variables['title'] = t('%action idea', ['%action' => $action]);
  }
}

/**
 * Implements hook_preprocess_node().
 */
function social_idea_preprocess_node(array &$variables): void {
  $node = $variables['node'];
  if ($node->getType() === 'social_idea') {
    // Visibility is always public.
    $variables['visibility_icon'] = 'public';
    $variables['visibility_label'] = t('public');
    // Prepare phase variables.
    $phase = _social_idea_get_phase($node);
    if ($phase) {
      $variables['idea_phase_name'] = $phase->label();
      $variables['idea_phase_class'] = $phase->isActive() ? 'badge-active' : 'badge-default';
    }
    // Attach extra CSS styles.
    $variables['#attached']['library'][] = 'social_idea/social_idea';
  }
}

/**
 * Get last past or active phase of Idea.
 */
function _social_idea_get_phase($node) {
  $last_phase = NULL;
  if (!$node->get('field_social_idea_phases')->isEmpty()) {
    foreach ($node->get('field_social_idea_phases')->getValue() as $item) {
      $ids[] = $item['target_id'];
    }
    $phases = Phase::loadMultiple($ids);
    foreach ($phases as $phase) {
      if ($phase->isActive()) {
        return $phase;
      }
      elseif ($phase->isPast()) {
        if ($last_phase) {
          $last_phase_date = $last_phase->get('field_phase_date')->start_date->getTimestamp();
          $phase_date = $phase->get('field_phase_date')->start_date->getTimestamp();
          if ($last_phase_date < $phase_date) {
            $last_phase = $phase;
          }
        }
        else {
          $last_phase = $phase;
        }
      }
    }
  }

  return $last_phase;
}

/**
 * Implements hook_social_follow_content_types_alter().
 */
function social_idea_social_follow_content_types_alter(array &$types): void {
  $types[] = 'social_idea';
}

/**
 * Implements hook_social_core_compatible_content_forms_alter().
 */
function social_idea_social_core_compatible_content_forms_alter(array &$compatible_content_type_forms): void {
  $compatible_content_type_forms[] = 'node_social_idea_form';
  $compatible_content_type_forms[] = 'node_social_idea_edit_form';
}

/**
 * Implements hook_ENTITY_TYPE_delete() for node entities.
 */
function social_idea_node_delete(NodeInterface $node): void {
  // Only for social ideas.
  if ($node->bundle() !== 'social_idea') {
    return;
  }

  // Check if idea have group.
  $group = _social_group_get_current_group($node);
  if (!($group instanceof GroupInterface)) {
    return;
  }

  // Clear cache tag.
  Drupal::service('cache_tags.invalidator')->invalidateTags([
    'group:' . $group->id(),
    'group_block:' . $group->id(),
  ]);
}

/**
 * Implements hook_social_scroll_allowed_views_alter().
 */
function social_idea_social_scroll_allowed_views_alter(array &$view_ids): void {
  $view_ids[] = 'ideas_user';
}

/**
 * Implements hook_social_organization_allowed_content_type_list_alter().
 */
function social_idea_social_organization_allowed_content_type_list_alter(array &$node_types): void {
  /** @var \Drupal\node\Entity\NodeType $node_type */
  $node_type = \Drupal::entityTypeManager()
    ->getStorage('node_type')
    ->load('social_idea');

  $node_types['social_idea'] = $node_type->label();
}

/**
 * Implements hook_social_search_grid_allowed_entity_type_bundles_alter().
 */
function social_idea_social_search_grid_allowed_entity_type_bundles_alter(array &$bundles): void {
  $bundles['node'][] = 'social_idea';
}

/**
 * Implements hook_preprocess_HOOK() for "html".
 */
function social_idea_preprocess_html(array &$variables): void {
  // Make social idea icon to be available for themes.
  $icon = file_get_contents(\Drupal::service('extension.list.module')->getPath('social_idea') . '/images/idea_icon.svg');

  // Add a new render array to page_bottom so the icon get added to the page.
  $variables['page_bottom']['idea_icon'] = [
    '#type' => 'inline_template',
    '#template' => '<span class="hidden">' . $icon . '</span>',
  ];
}
