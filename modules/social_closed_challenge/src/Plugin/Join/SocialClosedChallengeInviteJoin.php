<?php

namespace Drupal\social_closed_challenge\Plugin\Join;

use Drupal\social_challenge\Plugin\Join\SocialChallengeJoinTrait;
use Drupal\social_group\EntityMemberInterface;
use Drupal\social_group_invite\Plugin\Join\SocialGroupInviteJoin;

/**
 * Provides a join plugin instance for joining after invitation.
 */
class SocialClosedChallengeInviteJoin extends SocialGroupInviteJoin {

  use SocialChallengeJoinTrait;

  /**
   * {@inheritdoc}
   */
  public function actions(EntityMemberInterface $entity, array &$variables): array {
    if ($entity->getEntityTypeId() !== 'group' || $entity->bundle() !== 'cc') {
      $items = parent::actions($entity, $variables);
    }
    else {
      $items = [
        [
          'label' => $this->t('Invitation only'),
          'attributes' => [
            'title' => $this->t('Invitation only.'),
            'class' => ['dropdown-toggle', 'waves-btn', 'waves-light'],
          ],
        ],
      ];
    }

    /** @var \Drupal\social_group\SocialGroupInterface $entity */
    return $this->alter($entity, $items);
  }

}
