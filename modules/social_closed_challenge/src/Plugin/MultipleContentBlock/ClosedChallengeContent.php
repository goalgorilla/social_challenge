<?php

namespace Drupal\social_closed_challenge\Plugin\MultipleContentBlock;

use Drupal\social_content_block\MultipleContentBlockBase;

/**
 * Provides a content block for closed challenges.
 *
 * @MultipleContentBlock(
 *   id = "closed_challenge_content",
 *   label = @Translation("Closed challenge"),
 *   entity_type = "group",
 *   bundle = "cc"
 * )
 */
class ClosedChallengeContent extends MultipleContentBlockBase {

}
