<?php

namespace Drupal\social_closed_challenge;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Social closed challenge config override.
 *
 * @package Drupal\social_closed_challenge
 */
class SocialClosedChallengeConfigOverride implements ConfigFactoryOverrideInterface {

  /**
   * The config.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The module handler.
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Are we in override mode?
   */
  protected bool $inOverride = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    // Basically this makes sure we can load overrides without getting stuck
    // in a loop.
    if ($this->inOverride) {
      return [];
    }

    $this->inOverride = TRUE;

    $overrides = [];

    $config_name = 'views.view.challenges_all';

    if (in_array($config_name, $names)) {
      $config = $this->configFactory->get($config_name);
      $filters_value = $config->get('display.page_all_challenges.display_options.filters.type.value');
      $filters_value['cc'] = 'cc';

      $overrides[$config_name] = [
        'display' => [
          'page_all_challenges' => [
            'display_options' => [
              'filters' => [
                'type' => [
                  'value' => $filters_value,
                ],
              ],
            ],
          ],
        ],
      ];
    }

    $config_name = 'views.view.challenges_user';

    if (in_array($config_name, $names)) {
      $config = $this->configFactory->get($config_name);
      $filters_value = $config->get('display.default.display_options.filters.type.value');
      $filters_value['cc'] = 'cc';

      foreach (['default', 'page'] as $display) {
        $overrides[$config_name]['display'][$display] = [
          'display_options' => [
            'filters' => [
              'type' => [
                'value' => $filters_value,
              ],
            ],
          ],
        ];
      }
    }

    // Set view mode "Teaser" for "Challenge" group in Search All and Groups.
    $config_names = [
      'views.view.search_all',
      'views.view.search_groups',
    ];

    foreach ($config_names as $config_name) {
      if (in_array($config_name, $names)) {
        $config = $this->configFactory->get($config_name);
        $bundles = $config->get('display.default.display_options.row.options.view_modes.entity:group');
        $bundles['cc'] = 'teaser';

        $overrides[$config_name] = [
          'display' => [
            'default' => [
              'display_options' => [
                'row' => [
                  'options' => [
                    'view_modes' => [
                      'entity:group' => $bundles,
                    ],
                  ],
                ],
              ],
            ],
          ],
        ];
      }
    }

    $config_name = 'views.view.challenge_ideas';

    if (in_array($config_name, $names)) {
      $config = $this->configFactory->get($config_name);
      $types = $config->get('display.default.display_options.filters.type.value');
      $types['cc-group_node-social_idea'] = 'cc-group_node-social_idea';
      $bundles = $config->get('display.default.display_options.arguments.gid.validate_options.bundles');
      $bundles['cc'] = 'cc';
      $dependencies = $config->get('dependencies.config');
      $dependencies[] = 'group.content_type.cc-group_node-social_idea';
      $dependencies[] = 'group.type.cc';

      $overrides[$config_name] = [
        'dependencies' => [
          'config' => $dependencies,
        ],
        'display' => [
          'default' => [
            'display_options' => [
              'filters' => [
                'type' => [
                  'value' => $types,
                ],
              ],
              'arguments' => [
                'gid' => [
                  'validate_options' => [
                    'bundles' => $bundles,
                  ],
                ],
              ],
            ],
          ],
          'block' => [
            'display_options' => [
              'filters' => [
                'type' => [
                  'value' => $types,
                ],
              ],
            ],
          ],
        ],
      ];
    }

    // Show Phases block on closed challenges.
    $config_name = 'block.block.views_block__challenge_phases_block';

    if (in_array($config_name, $names)) {
      $config = $this->configFactory->get($config_name);
      $group_types = $config->get('visibility.group_type.group_types');
      $group_types['cc'] = 'cc';
      $overrides[$config_name] = [
        'visibility' => [
          'group_type' => [
            'group_types' => $group_types,
          ],
        ],
      ];
    }

    // Show Latest Ideas block on closed challenges.
    $config_name = 'block.block.views_block__challenge_ideas_block';

    if (in_array($config_name, $names)) {
      $config = $this->configFactory->get($config_name);
      $group_types = $config->get('visibility.group_type.group_types');
      $group_types['cc'] = 'cc';
      $overrides[$config_name] = [
        'visibility' => [
          'group_type' => [
            'group_types' => $group_types,
          ],
        ],
      ];
    }

    $blocks = [
      'group_add_idea_block',
    ];
    foreach ($names as $name) {
      if (strpos($name, 'block.block.') === 0) {
        $config = $this->configFactory->getEditable($name);

        if ($config->get('settings.provider') === 'social_idea' && in_array($config->get('settings.id'), $blocks)) {
          $group_types = $config->get('visibility.group_type.group_types');
          $group_types['cc'] = 'cc';
          $overrides[$name] = [
            'visibility' => [
              'group_type' => [
                'group_types' => $group_types,
              ],
            ],
          ];
        }
      }
    }

    $blocks = [
      'views_exposed_filter_block:challenge_ideas-page',
    ];
    foreach ($names as $name) {
      if (strpos($name, 'block.block.') === 0) {
        $config = $this->configFactory->getEditable($name);

        if ($config->get('settings.provider') === 'views' && in_array($config->get('settings.id'), $blocks)) {
          $group_types = $config->get('visibility.group_type.group_types');
          $group_types['cc'] = 'cc';
          $overrides[$name] = [
            'visibility' => [
              'group_type' => [
                'group_types' => $group_types,
              ],
            ],
          ];
        }
      }
    }

    $config_name = 'message.template.create_topic_gc';
    if (in_array($config_name, $names, FALSE)) {
      $overrides[$config_name]['third_party_settings']['activity_logger']['activity_bundle_entities'] =
        [
          'group_content-cc-group_node-topic' => 'group_content-cc-group_node-topic',
        ];
    }

    $config_name = 'message.template.create_event_gc';
    if (in_array($config_name, $names, FALSE)) {
      $overrides[$config_name]['third_party_settings']['activity_logger']['activity_bundle_entities'] =
        [
          'group_content-cc-group_node-event' => 'group_content-cc-group_node-event',
        ];
    }

    $config_name = 'message.template.create_idea_gc';
    if (in_array($config_name, $names, FALSE)) {
      $overrides[$config_name]['third_party_settings']['activity_logger']['activity_bundle_entities'] =
        [
          'group_content-cc-group_node-social_idea' => 'group_content-cc-group_node-social_idea',
        ];
    }

    $this->inOverride = FALSE;

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix(): string {
    return 'SocialChallengeConfigOverride';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name): CacheableMetadata {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}
