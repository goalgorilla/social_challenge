<?php

namespace Drupal\social_closed_challenge;

use Drupal\social_challenge\SocialChallengeInstallHelperBase;
use Drupal\user\RoleInterface;

/**
 * Defines a helper class, sets permissions.
 */
class SocialClosedChallengeInstallHelper extends SocialChallengeInstallHelperBase {

  /**
   * Get default permissions.
   */
  public function getPermissions(string $role): array {
    // Anonymous.
    $permissions[RoleInterface::ANONYMOUS_ID] = [];

    // Authenticated.
    $permissions[RoleInterface::AUTHENTICATED_ID] = array_merge($permissions[RoleInterface::ANONYMOUS_ID], []);

    // Content manager.
    $permissions['contentmanager'] = array_merge($permissions[RoleInterface::AUTHENTICATED_ID], [
      'create cc group',
    ]);

    // Site manager.
    $permissions['sitemanager'] = array_merge($permissions['contentmanager'], []);

    return $permissions[$role] ?? [];
  }

}
