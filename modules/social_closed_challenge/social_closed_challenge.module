<?php

/**
 * @file
 * Contains Drupal\social_closed_challenge\social_closed_challenge.module.
 */

use Drupal\block\Entity\Block;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\social_closed_challenge\Plugin\Join\SocialClosedChallengeInviteJoin;

/**
 * Implements hook_social_group_join_info_alter().
 */
function social_closed_challenge_social_group_join_info_alter(array &$info): void {
  $info['social_group_invite_join']['class'] = SocialClosedChallengeInviteJoin::class;
}

/**
 * Implements hook_social_group_join_method_usage().
 */
function social_closed_challenge_social_group_join_method_usage(): array {
  return [
    [
      'entity_type' => 'group',
      'bundle' => 'cc',
      'method' => 'added',
    ],
  ];
}

/**
 * Implements hook_social_group_types_alter().
 */
function social_closed_challenge_social_group_types_alter(array &$social_group_types): void {
  $social_group_types[] = 'cc';
}

/**
 * Implements hook_social_group_overview_route_alter().
 */
function social_closed_challenge_social_group_overview_route_alter(array &$route, GroupInterface $group): void {
  if ($group->bundle() === 'cc') {
    $route = [
      'name' => 'view.challenges_user.page',
      'parameters' => ['user' => \Drupal::currentUser()->id()],
    ];
  }
}

/**
 * Implements hook_theme().
 */
function social_closed_challenge_theme(): array {
  return [
    'group__cc__default' => [
      'base hook' => 'group',
    ],
    'group__cc__featured' => [
      'base hook' => 'group',
    ],
    'group__cc__hero' => [
      'base hook' => 'group',
    ],
    'group__cc__teaser' => [
      'base hook' => 'group',
    ],
    'group__cc__hero__sky' => [
      'base hook' => 'group',
    ],
    'group__cc__statistic__sky' => [
      'base hook' => 'group',
    ],
  ];
}

/**
 * Alter the visibility field within groups.
 *
 * Implements hook_field_widget_form_alter().
 */
function social_closed_challenge_field_widget_form_alter(array &$element, FormStateInterface $form_state, array $context): void {
  /** @var \Drupal\Core\Field\FieldDefinitionInterface $field_definition */
  $field_definition = $context['items']->getFieldDefinition();
  // Unset the public options on visibility field.
  if ($field_definition->getType() === 'entity_access_field') {
    $closed_challenge = 'cc';
    $current_group = _social_group_get_current_group();
    if (!empty($current_group)) {
      $group_type = $current_group->getGroupType()->id();
      if ($group_type == $closed_challenge) {
        $visibility_options = social_group_get_allowed_visibility_options_per_group_type($closed_challenge);

        foreach ($visibility_options as $visibility => $allowed) {
          $element[$visibility]['#disabled'] = $allowed;
        }
      }
    }
  }
}

/**
 * Implements hook_block_access().
 */
function social_closed_challenge_block_access(Block $block, string $operation, AccountInterface $account): AccessResultInterface {
  // Disllow Social Sharing block on Closed Challenge.
  $share_blocks = [
    'shariffsharebuttons_challenge',
    'shariffsharebuttons_idea',
  ];
  if ($operation === 'view' && in_array($block->id(), $share_blocks, TRUE)) {
    $current_group = _social_group_get_current_group();
    if ($current_group !== NULL) {
      $group_type = $current_group->getGroupType()->id();
      if ($group_type === 'cc') {
        return AccessResult::forbidden();
      }
    }
  }

  // No opinion for other situations really.
  return AccessResult::neutral();
}

/**
 * Implements hook_social_group_hide_types_alter().
 */
function social_closed_challenge_social_group_hide_types_alter(array &$hidden_types): void {
  $hidden_types[] = 'cc';
}

/**
 * Implements hook_social_group_default_visibility_alter().
 */
function social_closed_challenge_social_group_default_visibility_alter(?string &$visibility, string $group_type_id): void {
  if ($group_type_id === 'cc') {
    $visibility = 'group';
  }
}

/**
 * Implements hook_social_group_allowed_visibilities_alter().
 */
function social_closed_challenge_social_group_allowed_visibilities_alter(array &$visibilities, ?string $group_type_id): void {
  if ($group_type_id === 'cc') {
    foreach ($visibilities as $visibility_name => &$is_visible) {
      $is_visible = $visibility_name === 'group';
    }
  }
}
