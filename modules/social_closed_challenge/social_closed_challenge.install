<?php

/**
 * @file
 * Install functions for the social_closed_challenge module.
 */

use Drupal\Core\Config\FileStorage;
use Drupal\social_closed_challenge\SocialClosedChallengeInstallHelper;

/**
 * Implements hook_install().
 *
 * Perform actions related to the installation of social_closed_challenge.
 */
function social_closed_challenge_install(): void {
  \Drupal::classResolver(SocialClosedChallengeInstallHelper::class)->setPermissions();
}

/**
 * Update form display for "Closed Challenge" group type.
 */
function social_closed_challenge_update_8001(): string {
  /** @var \Drupal\update_helper\Updater $updateHelper */
  $updateHelper = \Drupal::service('update_helper.updater');

  // Execute configuration update definitions with logging of success.
  $updateHelper->executeUpdate('social_closed_challenge', 'social_closed_challenge_update_8001');

  // Output logged messages to related channel of update execution.
  return $updateHelper->logger()->output();
}

/**
 * Update "Title" and remove redundant field group.
 */
function social_closed_challenge_update_8002(): void {
  $fields = \Drupal::service('entity_field.manager')->getFieldDefinitions('group', 'cc');
  $title_field = $fields['label'];
  $title_field->getConfig('cc')->setLabel('Name')->save();

  // There can be possibility that someone might have added fields in
  // fieldgroup we are removing. So, we want to skip removal if there are any
  // children present in fieldgroup.
  $group = field_group_load_field_group('group_attachments', 'group', 'cc', 'form', 'default');
  if ($group && empty($group->children)) {
    field_group_delete_field_group($group);
  }
}

/**
 * Add translation for closed challenges.
 */
function social_closed_challenge_update_8003(): string {
  /** @var \Drupal\update_helper\Updater $updateHelper */
  $updateHelper = \Drupal::service('update_helper.updater');

  // Execute configuration update definitions with logging of success.
  $updateHelper->executeUpdate('social_closed_challenge', 'social_closed_challenge_update_8003');

  // Output logged messages to related channel of update execution.
  return $updateHelper->logger()->output();
}

/**
 * Allow CM+ to translate Closed Challenges.
 */
function social_closed_challenge_update_8004(): void {
  $roles = [
    'contentmanager',
    'sitemanager',
  ];
  foreach ($roles as $role) {
    user_role_grant_permissions($role, ['translate cc group']);
  }
}

/**
 * Add new view modes for content in Closed Challenge group type.
 */
function social_closed_challenge_update_8005(): void {
  // Install new configs.
  $config_path = \Drupal::service('extension.list.module')->getPath('social_closed_challenge') . '/config/install';
  $source = new FileStorage($config_path);
  $config_storage = \Drupal::service('config.storage');

  $config_names = [
    'core.entity_view_display.group_content.cc-group_node-event.activity',
    'core.entity_view_display.group_content.cc-group_node-social_idea.activity',
    'core.entity_view_display.group_content.cc-group_node-topic.activity',
  ];

  foreach ($config_names as $name) {
    $config_storage->write($name, (array) $source->read($name));
  }
}
