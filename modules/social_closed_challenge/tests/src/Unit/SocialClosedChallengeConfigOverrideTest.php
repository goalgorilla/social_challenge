<?php

namespace Drupal\Tests\social_secret_challenge\Unit;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\social_closed_challenge\SocialClosedChallengeConfigOverride;

/**
 * Social closed challenge config override test.
 *
 * @coversDefaultClass \Drupal\social_closed_challenge\SocialClosedChallengeConfigOverride
 * @group social_closed_challenge
 */
class SocialClosedChallengeConfigOverrideTest extends UnitTestCase {

  use ProphecyTrait;
  /**
   * Default configuration.
   *
   * @var array[]
   */
  protected array $defaultConfig = [
    'views.view.challenges_all' => [
      'display' => [
        'page_all_challenges' => [
          'display_options' => [
            'filters' => [
              'type' => [
                'value' => [
                  'challenge' => 'challenge',
                ],
              ],
            ],
          ],
        ],
      ],
    ],
    'views.view.challenges_user' => [
      'display' => [
        'default' => [
          'display_options' => [
            'filters' => [
              'type' => [
                'value' => [
                  'challenge' => 'challenge',
                ],
              ],
            ],
          ],
        ],
        'page' => [
          'display_options' => [
            'filters' => [
              'type' => [
                'value' => [
                  'challenge' => 'challenge',
                ],
              ],
            ],
          ],
        ],
      ],
    ],
    'views.view.search_all' => [
      'display' => [
        'default' => [
          'display_options' => [
            'row' => [
              'options' => [
                'view_modes' => [
                  'entity:group' => [
                    'test_group' => 'teaser',
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
    ],
    'views.view.search_groups' => [
      'display' => [
        'default' => [
          'display_options' => [
            'row' => [
              'options' => [
                'view_modes' => [
                  'entity:group' => [
                    'test_group' => 'teaser',
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
    ],
    'views.view.challenge_ideas' => [
      'dependencies' => [
        'config' => [
          'test.dependency',
        ],
      ],
      'display' => [
        'default' => [
          'display_options' => [
            'filters' => [
              'type' => [
                'value' => [
                  'challenge-group_node-social_idea' => 'challenge-group_node-social_idea',
                ],
              ],
            ],
            'arguments' => [
              'gid' => [
                'validate_options' => [
                  'bundles' => [
                    'challenge' => 'challenge',
                  ],
                ],
              ],
            ],
          ],
        ],
        'block' => [
          'display_options' => [
            'filters' => [
              'type' => [
                'value' => [
                  'challenge-group_node-social_idea' => 'challenge-group_node-social_idea',
                ],
              ],
            ],
          ],
        ],
      ],
    ],
    'block.block.views_block__challenge_phases_block'  => [
      'visibility' => [
        'group_type' => [
          'group_types' => [
            'challenge' => 'challenge',
          ],
        ],
      ],
    ],
    'block.block.views_block__challenge_ideas_block'  => [
      'visibility' => [
        'group_type' => [
          'group_types' => [
            'challenge' => 'challenge',
          ],
        ],
      ],
    ],
  ];

  /**
   * SocialClosedChallengeConfigOverride.
   */
  protected SocialClosedChallengeConfigOverride $configOverrides;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $module_handler = $this->prophesize(ModuleHandlerInterface::class)->reveal();
    parent::setUp();
    $this->configOverrides = new SocialClosedChallengeConfigOverride($this->getConfigFactoryStub($this->defaultConfig), $module_handler);
  }

  /**
   * @covers ::loadOverrides
   */
  public function testLoadOverrides(): void {
    // There is no override if the config name is wrong.
    $names = [
      'test',
    ];

    $this->assertArrayEquals([], $this->configOverrides->loadOverrides($names));

    // Prepare view expected filter values.
    $expected_filter_values = [
      'challenge' => 'challenge',
      'cc' => 'cc',
    ];

    // Assert that challenges_all view config gets overridden correctly.
    $names = [
      'views.view.challenges_all',
    ];

    $this->assertArrayEquals(
      [
        'views.view.challenges_all' => [
          'display' => [
            'page_all_challenges' => [
              'display_options' => [
                'filters' => [
                  'type' => [
                    'value' => $expected_filter_values,
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    // Assert that challenges_user view config gets overridden correctly.
    $names = [
      'views.view.challenges_user',
    ];

    $this->assertArrayEquals(
      [
        'views.view.challenges_user' => [
          'display' => [
            'default' => [
              'display_options' => [
                'filters' => [
                  'type' => [
                    'value' => $expected_filter_values,
                  ],
                ],
              ],
            ],
            'page' => [
              'display_options' => [
                'filters' => [
                  'type' => [
                    'value' => $expected_filter_values,
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    // Assert view mode for search_all and search_groups views the config gets
    // overridden correctly.
    $names = [
      'views.view.search_all',
      'views.view.search_groups',
    ];
    foreach ($names as $name) {
      $expected_bundles = [
        'test_group' => 'teaser',
        'cc' => 'teaser',
      ];

      $this->assertArrayEquals(
        [
          $name => [
            'display' => [
              'default' => [
                'display_options' => [
                  'row' => [
                    'options' => [
                      'view_modes' => [
                        'entity:group' => $expected_bundles,
                      ],
                    ],
                  ],
                ],
              ],
            ],
          ],
        ],
        $this->configOverrides->loadOverrides([$name])
      );
    }

    // Assert that challenges_user view config gets overridden correctly.
    $names = [
      'views.view.challenge_ideas',
    ];
    $expected_dependencies = [
      'test.dependency',
      'group.content_type.cc-group_node-social_idea',
      'group.type.cc',
    ];
    $expected_filters_values = [
      'challenge-group_node-social_idea' => 'challenge-group_node-social_idea',
      'cc-group_node-social_idea' => 'cc-group_node-social_idea',
    ];
    $expected_bundles = [
      'challenge' => 'challenge',
      'cc' => 'cc',
    ];

    $this->assertArrayEquals(
      [
        'views.view.challenge_ideas' => [
          'dependencies' => [
            'config' => $expected_dependencies,
          ],
          'display' => [
            'default' => [
              'display_options' => [
                'filters' => [
                  'type' => [
                    'value' => $expected_filters_values,
                  ],
                ],
                'arguments' => [
                  'gid' => [
                    'validate_options' => [
                      'bundles' => $expected_bundles,
                    ],
                  ],
                ],
              ],
            ],
            'block' => [
              'display_options' => [
                'filters' => [
                  'type' => [
                    'value' => $expected_filters_values,
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    // Assert block views for phases and ideas the config gets overridden
    // correctly.
    $names = [
      'block.block.views_block__challenge_phases_block',
      'block.block.views_block__challenge_ideas_block',
    ];
    $expected_group_types = [
      'challenge' => 'challenge',
      'cc' => 'cc',
    ];

    foreach ($names as $name) {
      $this->assertArrayEquals(
        [
          $name => [
            'visibility' => [
              'group_type' => [
                'group_types' => $expected_group_types,
              ],
            ],
          ],
        ],
        $this->configOverrides->loadOverrides([$name])
      );
    }
  }

}
