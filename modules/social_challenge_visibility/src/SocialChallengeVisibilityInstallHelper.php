<?php

namespace Drupal\social_challenge_visibility;

use Drupal\social_challenge\SocialChallengeInstallHelperBase;
use Drupal\user\RoleInterface;

/**
 * Defines a helper class, sets permissions, creates menu links.
 */
class SocialChallengeVisibilityInstallHelper extends SocialChallengeInstallHelperBase {

  /**
   * Set default permissions.
   */
  public function setPermissions(): void {
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();

    /** @var \Drupal\user\Entity\Role $role */
    foreach ($roles as $role) {
      // @todo shouldn't this module also skip the administrator role?
      if ($role->id() === 'administrator') {
        continue;
      }

      $permissions = $this->getPermissions((string) $role->id());
      user_role_grant_permissions($role->id(), $permissions);
      user_role_revoke_permissions(RoleInterface::AUTHENTICATED_ID, ['view node.social_idea.field_content_visibility:group content']);
    }
  }

  /**
   * Get default permissions.
   *
   * @param string $role
   *   The role name.
   */
  public function getPermissions(string $role): array {
    // @todo fix merging of arrays like other modules do?
    // Anonymous.
    $permissions[RoleInterface::ANONYMOUS_ID] = [
      'view node.social_idea.field_content_visibility:public content',
    ];

    // Authenticated.
    $permissions[RoleInterface::AUTHENTICATED_ID] = [
      'view node.social_idea.field_content_visibility:public content',
      'view node.social_idea.field_content_visibility:community content',
    ];

    $permissions['contentmanager'] = [
      'view node.social_idea.field_content_visibility:group content',
    ];
    $permissions['sitemanager'] = $permissions['contentmanager'];

    return $permissions[$role] ?? [];
  }

}
