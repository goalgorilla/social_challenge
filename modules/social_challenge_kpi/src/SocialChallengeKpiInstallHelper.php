<?php

namespace Drupal\social_challenge_kpi;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\kpi_analytics\BlockCreator;
use Drupal\kpi_analytics\BlockContentCreator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a helper class, sets permissions.
 */
class SocialChallengeKpiInstallHelper implements ContainerInjectionInterface {

  /**
   * Block creator.
   */
  protected BlockCreator $blockCreator;

  /**
   * Block content creator.
   */
  protected BlockContentCreator $blockContentCreator;

  /**
   * The module extension list.
   */
  protected ModuleExtensionList $moduleExtensionList;

  /**
   * The module path.
   *
   * @var string
   */
  protected string $modulePath;

  /**
   * Constructs SocialChallengeKpiInstallHelper.
   *
   * @param \Drupal\kpi_analytics\BlockCreator $block_creator
   *   The block creator.
   * @param \Drupal\kpi_analytics\BlockContentCreator $block_content_creator
   *   The block content creator.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extension_list_module
   *   The module extension list.
   */
  public function __construct(BlockCreator $block_creator, BlockContentCreator $block_content_creator, ModuleExtensionList $extension_list_module) {
    $this->blockCreator = $block_creator;
    $this->blockContentCreator = $block_content_creator;
    $this->moduleExtensionList = $extension_list_module;
    $this->getPath();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('kpi_analytics.block_creator'),
      $container->get('kpi_analytics.block_content_creator'),
      $container->get('extension.list.module')
    );
  }

  /**
   * Returns the current module path.
   */
  private function getPath(): self {
    $this->modulePath = $this->moduleExtensionList->getPath('social_challenge_kpi');
    return $this;
  }

  /**
   * Returns a list of KPI Lite blocks.
   */
  public function getBlockList(): array {
    return [
      'social_kpi_lite_ideas_created',
      'social_kpi_lite_ideas_engagement',
      'social_kpi_lite_challenges_created',
      'social_kpi_lite_challenges_engagement',
      'social_kpi_lite_ideas_with_tag_created',
      'social_kpi_lite_ideas_with_tag_engagement',
      'social_kpi_lite_challenges_with_content_tag_created',
      'social_kpi_lite_challenges_with_content_tag_engagement',
    ];
  }

  /**
   * Creates KPI Lite blocks.
   */
  public function createBlockList(array $blocks = []): void {
    // If no blocks are provided we load the default blocklist.
    if (empty($blocks)) {
      $blocks = $this->getBlockList();
    }
    $block_content_path = "{$this->modulePath}/content/block_content";
    $block_path = "{$this->modulePath}/content/block";

    foreach ($blocks as $block_id) {
      $this->blockContentCreator->setSource($block_content_path, $block_id);
      if (file_exists("$block_path/$block_id.yml")) {
        $this->blockContentCreator->create();
        $this->blockContentCreator->createBlockInstance($block_path, $block_id);
      }
    }
  }

  /**
   * Deletes KPI Lite blocks.
   */
  public function deleteBlockList(array $blocks = []): void {
    // If no blocks are provided we load the default blocklist.
    if (empty($blocks)) {
      $blocks = $this->getBlockList();
    }
    $block_content_path = "{$this->modulePath}/content/block_content";
    $block_path = "{$this->modulePath}/content/block";

    foreach ($blocks as $block_id) {
      $this->blockCreator->setSource($block_path, $block_id);
      $this->blockCreator->delete();

      $this->blockContentCreator->setSource($block_content_path, $block_id);
      $this->blockContentCreator->delete();
    }
  }

}
