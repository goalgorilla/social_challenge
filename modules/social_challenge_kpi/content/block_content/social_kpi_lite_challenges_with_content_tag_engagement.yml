values:
  type: kpi_analytics
  info: 'KPI Analytics: Challenges engagement with Content tags'
  uuid: e2d93245-e3bd-40ae-bd42-dee7fa82d7dd
fields:
  field_kpi_datasource: drupal_kpi_term_datasource
  field_kpi_query: >
    SELECT
        created,
        SUM(count_memberships) AS count_memberships,
        COUNT(DISTINCT uid) AS active
    FROM
    (
        SELECT
            FROM_UNIXTIME(gcfd.created, '%Y-%m') as created,
            NULL AS uid,
            COUNT(DISTINCT gcfd.id) AS count_memberships
        FROM group_content_field_data gcfd
        JOIN users_field_data ufd ON gcfd.entity_id = ufd.uid
        JOIN group__social_tagging gst ON gst.entity_id = gcfd.gid
        WHERE
            FROM_UNIXTIME(gcfd.created, '%Y-%m')
            BETWEEN
                FROM_UNIXTIME(UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 3 MONTH)), '%Y-%m')
            AND
                FROM_UNIXTIME(UNIX_TIMESTAMP(CURDATE()), '%Y-%m')
            AND
                (gcfd.type = 'challenge-group_membership' OR gcfd.type = 'cc-group_membership')
            AND
                ufd.status = 1
            AND
                gst.social_tagging_target_id in (:term_ids[])
        GROUP BY FROM_UNIXTIME(gcfd.created, '%Y-%m')

        -- Nodes.
        UNION
        SELECT
            FROM_UNIXTIME(gcfd.created, '%Y-%m') as created,
            gcfd.entity_id AS uid,
            0 AS count_memberships
        FROM group_content_field_data gcfd
        JOIN users_field_data ufd ON gcfd.entity_id = ufd.uid
        JOIN node_field_data nfd ON gcfd.entity_id = nfd.uid
        JOIN group__social_tagging gst ON gst.entity_id = gcfd.gid
        WHERE
            FROM_UNIXTIME(gcfd.created, '%Y-%m')
            BETWEEN
                FROM_UNIXTIME(UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 3 MONTH)), '%Y-%m')
            AND
                FROM_UNIXTIME(UNIX_TIMESTAMP(CURDATE()), '%Y-%m')
            AND
                (gcfd.type = 'challenge-group_membership' OR gcfd.type = 'cc-group_membership')
            AND
                ufd.status = 1
            AND
                gst.social_tagging_target_id in (:term_ids[])
        GROUP BY uid, FROM_UNIXTIME(gcfd.created, '%Y-%m')

        -- Comments.
        UNION
        SELECT
            FROM_UNIXTIME(gcfd.created, '%Y-%m') as created,
            gcfd.entity_id AS uid,
            0 AS count_memberships
        FROM group_content_field_data gcfd
        JOIN users_field_data ufd ON gcfd.entity_id = ufd.uid
        JOIN comment_field_data cfd ON gcfd.entity_id = cfd.uid
        JOIN group__social_tagging gst ON gst.entity_id = gcfd.gid
        WHERE
            FROM_UNIXTIME(gcfd.created, '%Y-%m')
            BETWEEN
                FROM_UNIXTIME(UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 3 MONTH)), '%Y-%m')
            AND
                FROM_UNIXTIME(UNIX_TIMESTAMP(CURDATE()), '%Y-%m')
            AND
                (gcfd.type = 'challenge-group_membership' OR gcfd.type = 'cc-group_membership')
            AND
                ufd.status = 1
            AND
                gst.social_tagging_target_id in (:term_ids[])
        GROUP BY uid, FROM_UNIXTIME(gcfd.created, '%Y-%m')

        -- Posts.
        UNION
        SELECT
            FROM_UNIXTIME(gcfd.created, '%Y-%m') as created,
            gcfd.entity_id AS uid,
            0 AS count_memberships
        FROM group_content_field_data gcfd
        JOIN users_field_data ufd ON gcfd.entity_id = ufd.uid
        JOIN post_field_data pfd ON gcfd.entity_id = pfd.user_id
        JOIN group__social_tagging gst ON gst.entity_id = gcfd.gid
        WHERE
            FROM_UNIXTIME(gcfd.created, '%Y-%m')
            BETWEEN
                FROM_UNIXTIME(UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 3 MONTH)), '%Y-%m')
            AND
                FROM_UNIXTIME(UNIX_TIMESTAMP(CURDATE()), '%Y-%m')
            AND
                (gcfd.type = 'challenge-group_membership' OR gcfd.type = 'cc-group_membership')
            AND
                ufd.status = 1
            AND
                gst.social_tagging_target_id in (:term_ids[])
        GROUP BY uid, FROM_UNIXTIME(gcfd.created, '%Y-%m')

        -- Event enrollment.
        UNION
        SELECT
            FROM_UNIXTIME(gcfd.created, '%Y-%m') as created,
            gcfd.entity_id AS uid,
            0 AS count_memberships
        FROM group_content_field_data gcfd
        JOIN users_field_data ufd ON gcfd.entity_id = ufd.uid
        JOIN event_enrollment_field_data eefd ON gcfd.entity_id = eefd.user_id
        JOIN group__social_tagging gst ON gst.entity_id = gcfd.gid
        WHERE
            FROM_UNIXTIME(gcfd.created, '%Y-%m')
            BETWEEN
                FROM_UNIXTIME(UNIX_TIMESTAMP(DATE_SUB(CURDATE(), INTERVAL 3 MONTH)), '%Y-%m')
            AND
                FROM_UNIXTIME(UNIX_TIMESTAMP(CURDATE()), '%Y-%m')
            AND
                (gcfd.type = 'challenge-group_membership' OR gcfd.type = 'cc-group_membership')
            AND
                ufd.status = 1
            AND
                gst.social_tagging_target_id in (:term_ids[])
        GROUP BY uid, FROM_UNIXTIME(gcfd.created, '%Y-%m')
    ) AS source
    GROUP BY created
    ORDER BY created ASC;
  field_kpi_data_formatter:
    -
      value: three_months_timeline_kpi_data_formatter
  field_kpi_visualization: morris_bar_graph_kpi_visualization
  field_kpi_chart_labels:
    -
      value: 'Total participants'
    -
      value: 'Active participants'
  field_kpi_chart_colors:
    -
      value: '#ffc142'
    -
      value: '#fe8a71'
  body:
    -
      format: basic_html
      value: >
        Distinguish between the types of engagement on challenges.
