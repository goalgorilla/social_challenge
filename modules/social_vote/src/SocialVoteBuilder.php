<?php

namespace Drupal\social_vote;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Template\Attribute;
use Drupal\votingapi\VoteStorageInterface;

/**
 * Provides a lazy builder for user votes.
 */
class SocialVoteBuilder implements SocialVoteBuilderInterface, TrustedCallbackInterface {

  use StringTranslationTrait;

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The voting storage.
   */
  protected VoteStorageInterface $voteStorage;

  /**
   * A config factory for retrieving required config settings.
   */
  protected ConfigFactoryInterface $config;

  /**
   * Current user.
   */
  protected AccountInterface $currentUser;

  /**
   * Constructs a new SocialVoteBuilder.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   A config factory object for retrieving configuration settings.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current active user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config, AccountInterface $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->voteStorage = $entity_type_manager->getStorage('vote');
    $this->config = $config;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public function build($entity_type_id, $entity_id, $view_mode): array {
    // If either the entity type or the entity id is not set,
    // return and empty array.
    if (empty($entity_type_id) || empty($entity_id)) {
      return [];
    }

    // Load the entity for which vote and disvotes icons should be shown.
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->entityTypeManager->getStorage($entity_type_id)->load($entity_id);
    $hide_vote_widget = $this->config->get('social_vote.settings')->get('hide_vote_widget');

    $vote_access = social_vote_can_vote($this->currentUser, 'vote', $entity);
    $votes = social_vote_get_votes($entity);

    $icons = [];
    // Vote icon.
    if (!$hide_vote_widget || $vote_access) {
      $vote_attributes = new Attribute([
        'title' => $this->t('Vote'),
        'data-entity-id' => $entity_id,
        'data-entity-type' => $entity_type_id,
        'data-entity-view-mode' => $view_mode,
      ]);
      if ($view_mode !== 'full') {
        $vote_attributes->addClass('btn', 'btn-default', 'btn-sm');
      }
      if (!$vote_access) {
        $vote_attributes->addClass('disable-status');
      }
      if ($this->voteStorage->getUserVotes($this->currentUser->id(), 'vote', $entity_type_id, $entity_id)) {
        $vote_attributes->addClass('voted');
        if ($view_mode !== 'full') {
          $vote_attributes->addClass('btn-accent');
        }
      }
      $icons['vote'] = [
        'count' => $votes,
        'label' => $this->t('Vote'),
        'attributes' => $vote_attributes,
      ];
    }

    $build['icons'] = [
      '#theme' => $view_mode === 'full' ? 'social_vote_icons' : 'social_vote_teaser',
      '#attached' => ['library' => ['social_vote/icons']],
      '#entity_id' => $entity_id,
      '#entity_type' => $entity_type_id,
      '#icons' => $icons,
    ];

    // Attach JS logic in case user has enough permissions to vote.
    if ($vote_access) {
      $build['icons']['#attached']['library'][] = 'social_vote/behavior';
    }

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks(): array {
    return ['build'];
  }

}
