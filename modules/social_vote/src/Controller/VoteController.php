<?php

namespace Drupal\social_vote\Controller;

use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\votingapi\Entity\Vote;
use Drupal\votingapi\Entity\VoteType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Returns responses for Vote routes.
 */
class VoteController extends ControllerBase {

  /**
   * Creates a vote for a given parameters.
   *
   * @param string $entity_type_id
   *   The entity type ID to vote for.
   * @param string $vote_type_id
   *   The vote type (vote or disvote).
   * @param int $entity_id
   *   The entity ID to vote for.
   */
  public function vote(string $entity_type_id, string $vote_type_id, int $entity_id): JsonResponse {
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->entityTypeManager()->getStorage($entity_type_id)->load($entity_id);

    // Gets the number of votes for the entity.
    $vote_count = social_vote_get_votes($entity);
    $operation = ['vote' => FALSE];

    $vote_storage = $this->entityTypeManager()->getStorage('vote');
    $user_votes = $vote_storage->getUserVotes(
      $this->currentUser()->id(),
      $vote_type_id,
      $entity_type_id,
      $entity_id
    );

    if (empty($user_votes)) {
      // Increment the value for requested vote type.
      $vote_count++;
      $operation[$vote_type_id] = TRUE;

      $vote_type = VoteType::load($vote_type_id);
      $vote = Vote::create(['type' => $vote_type_id]);
      $vote->setVotedEntityId((int) $entity_id);
      $vote->setVotedEntityType($entity_type_id);
      $vote->setValueType($vote_type->getValueType());
      $vote->setValue(1);
      $vote->save();

      // Clear the view builder's cache.
      $this->entityTypeManager()->getViewBuilder($entity_type_id)->resetCache([$entity]);

      return new JsonResponse([
        'votes' => $vote_count,
        'message_type' => 'status',
        'operation' => $operation,
        'message' => $this->t('Your vote was added.'),
      ]);
    }

    if ($this->config('social_vote.settings')->get('allow_cancel_vote')) {
      // Decrement the value for requested vote type.
      $vote_count--;
      $operation[$vote_type_id] = FALSE;

      // Remove the vote.
      $vote_storage->deleteUserVotes(
        $this->currentUser()->id(),
        $vote_type_id,
        $entity_type_id,
        $entity_id
      );
      // Clear the view builder's cache.
      $this->entityTypeManager()->getViewBuilder($entity_type_id)->resetCache([$entity]);

      return new JsonResponse([
        'votes' => $vote_count,
        'operation' => $operation,
        'message_type' => 'status',
        'message' => $this->t('Your vote was canceled.'),
      ]);
    }

    // User is not allowed to cancel the user vote.
    return new JsonResponse([
      'votes' => $vote_count,
      'operation' => $operation,
      'message_type' => 'warning',
      'message' => $this->t('You are not allowed to vote the same way multiple times.'),
    ]);
  }

  /**
   * Checks if the currentUser is allowed to vote.
   */
  public function voteAccess(string $entity_type_id, string $vote_type_id, string $entity_id): AccessResultInterface {
    /** @var \Drupal\Core\Entity\EntityInterface $entity */
    $entity = $this->entityTypeManager()->getStorage($entity_type_id)->load($entity_id);
    // Check if user has permission to vote.
    if (!social_vote_can_vote($this->currentUser(), $vote_type_id, $entity)) {
      return AccessResult::forbidden();
    }

    return AccessResult::allowed();
  }

}
