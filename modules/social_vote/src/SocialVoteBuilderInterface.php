<?php

namespace Drupal\social_vote;

/**
 * Provides a lazy builder interface for user votes.
 */
interface SocialVoteBuilderInterface {

  /**
   * Lazy builder callback for displaying vote icons.
   *
   * @param string $entity_type_id
   *   The entity type ID for which votes icons should be shown.
   * @param string|int $entity_id
   *   The entity ID for which votes icons should be shown.
   * @param string|int $view_mode
   *   The entity view mode for which votes icons should be shown.
   *
   * @return array
   *   A render array for vote icon.
   */
  public function build(string $entity_type_id, $entity_id, $view_mode): array;

}
