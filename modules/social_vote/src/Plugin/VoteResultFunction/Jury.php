<?php

namespace Drupal\social_vote\Plugin\VoteResultFunction;

use Drupal\node\NodeInterface;
use Drupal\votingapi\VoteResultFunctionBase;
use Drupal\group\Entity\GroupInterface;

/**
 * Count votes made by jury.
 *
 * @VoteResultFunction(
 *   id = "vote_jury",
 *   label = @Translation("Jury"),
 *   description = @Translation("The number of votes by jury.")
 * )
 */
class Jury extends VoteResultFunctionBase {

  /**
   * {@inheritdoc}
   */
  public function calculateResult($votes): int {
    $total = 0;

    foreach ($votes as $vote) {
      $storage = \Drupal::entityTypeManager()->getStorage($vote->getVotedEntityType());
      $entity = $storage->load($vote->getVotedEntityId());

      if ($entity instanceof NodeInterface) {
        $group = _social_group_get_current_group($entity);

        if (
          ($group instanceof GroupInterface) &&
          $member = $group->getMember($vote->getOwner())
        ) {
          $roles = $member->getRoles();

          if (isset($roles[$group->bundle() . '-juror'])) {
            $total += $vote->getValue();
          }
        }
      }
    }

    return (int) $total;
  }

}
