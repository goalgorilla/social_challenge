/**
 * @file
 * Vote icon.
 */
(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.socialVote = {
    attach: function(context, settings) {

      $(document).once('social-challenge-vote').on('click', '.vote-widget--social-vote .vote-vote a', function() {

        let entity_id, entity_type, view_mode;

        if (!$(this).hasClass('disable-status')) {
          entity_id = $(this).data('entity-id');
          entity_type = $(this).data('entity-type');
          view_mode = $(this).data('entity-view-mode');
          // Cast the vote.
          socialVoteService.vote(entity_id, entity_type, view_mode, 'vote');
        }
      });

    }
  };

})(jQuery, Drupal);
