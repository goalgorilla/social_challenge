/**
 * @file
 * Vote icon behavior.
 */
(function ($, Drupal) {

  'use strict';

  window.socialVoteService = window.socialVoteService || (function() {
    function socialVoteService() {}
    socialVoteService.vote = function(entity_id, entity_type, view_mode, tag) {
      $.ajax({
        type: "POST",
        url: drupalSettings.path.baseUrl + 'social_vote/' + entity_type + '/' + tag + '/' + entity_id,
        success: function (response) {
          // Expected response is a json object where votes is the new number of votes.
          var selector = '#' + tag + '-container-' + entity_type + '-' + entity_id;
          var $container = $(selector);
          var $aTag = $(selector + ' a');

          if ($aTag.length == 0) {
            return;
          }

          if (view_mode == 'full') {
            if (response.operation[tag]) {
              var countText = Drupal.t('voted');
              $aTag.addClass('voted');

            } else {
              var countText = response.votes == '1' ? Drupal.t('vote') : Drupal.t('votes');
              $aTag.removeClass('voted');
            }

            $container.prevAll('.vote__count').find('a').html(response.votes)
              .attr('data-dialog-options', '{"title":"' + response.votes + countText + '", "width":"auto"}');
            $container.find('.vote__label').html(countText);
          } else {
            if (response.operation[tag]) {
              var btnText = Drupal.t('You have voted');
              $aTag.addClass('voted btn-accent');
            } else {
              var btnText = Drupal.t('Vote');
              $aTag.removeClass('voted btn-accent');
            }
            
            var countText = response.votes == '1' ? Drupal.t('vote') : Drupal.t('votes');
            var ideaCardBar = $container.closest('.card__actionbar');

            ideaCardBar.find('.vote__count').html(response.votes);
            ideaCardBar.find('.vote__label').html(countText);
            $container.find('.vote__label').html(btnText);
          }
        }
      });
    };

    return socialVoteService;
  })();

})(jQuery, Drupal);
