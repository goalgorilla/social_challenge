<?php

namespace Drupal\Tests\social_vote\Unit;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\social_vote\Controller\VoteController;

/**
 * Vote controller test.
 *
 * @coversDefaultClass \Drupal\social_vote\Controller\VoteController
 * @group social_vote
 */
class VoteControllerTest extends UnitTestCase {

  /**
   * The mocked entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The mocked account.
   *
   * @var \Drupal\Core\Session\AccountInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $account;

  /**
   * The mocked vote controller.
   *
   * @var \Drupal\Tests\social_vote\Controller\VoteController|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $controller;

  /**
   * {@inheritdoc}
   */
  public static function setUpBeforeClass() {
    parent::setUpBeforeClass();
    // Necessary for @covers to work.
    require_once __DIR__ . '/../../../social_vote.module';
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->entityTypeManager = $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface');
    $this->account = $this->createMock('Drupal\Core\Session\AccountInterface');

    $container = new ContainerBuilder();
    $container->set('entity_type.manager', $this->entityTypeManager);
    $container->set('current_user', $this->account);
    \Drupal::setContainer($container);

    $this->controller = new VoteController();
  }

  /**
   * @covers ::voteAccess
   */
  public function testVoteAccessForbidden() {
    $entity = $this->createMock('Drupal\Core\Entity\EntityInterface');

    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->any())
      ->method('load')
      ->willReturn($entity);

    $this->entityTypeManager
      ->expects($this->any())
      ->method('getStorage')
      ->willReturn($storage);

    $this->assertInstanceOf(
      AccessResultForbidden::class,
      $this->controller->voteAccess(
        'dummy_entity_type_id',
        'dummy_vote_type_id',
        'dummy_entity_id'
      )
    );
  }

  /**
   * @covers ::voteAccess
   */
  public function testVoteAccessAllowedWithEntityTypePermission() {
    $entity_type_id = 'dummy_entity_type_id';
    $vote_type_id = 'dummy_vote_type_id';

    $entity = $this->createMock('Drupal\Core\Entity\EntityInterface');
    $entity
      ->expects($this->any())
      ->method('getEntityTypeId')
      ->willReturn($entity_type_id);

    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->any())
      ->method('load')
      ->willReturn($entity);

    $this->entityTypeManager
      ->expects($this->any())
      ->method('getStorage')
      ->willReturn($storage);

    $this->account
      ->expects($this->any())
      ->method('hasPermission')
      ->will($this->returnValueMap([
        ["add or remove $vote_type_id votes on $entity_type_id", TRUE],
      ]));

    $this->assertInstanceOf(
      AccessResultAllowed::class,
      $this->controller->voteAccess(
        $entity_type_id,
        $vote_type_id,
        'dummy_entity_id'
      )
    );
  }

  /**
   * @covers ::voteAccess
   */
  public function testVoteAccessAllowedWithEntityBundlePermission() {
    $entity_type_id = 'dummy_entity_type_id';
    $vote_type_id = 'dummy_vote_type_id';
    $entity_bundle = 'dummy_entity_bundle';

    $entity = $this->createMock('Drupal\Core\Entity\EntityInterface');
    $entity
      ->expects($this->any())
      ->method('getEntityTypeId')
      ->willReturn($entity_type_id);
    $entity
      ->expects($this->once())
      ->method('bundle')
      ->willReturn($entity_bundle);

    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->any())
      ->method('load')
      ->willReturn($entity);

    $this->entityTypeManager
      ->expects($this->any())
      ->method('getStorage')
      ->willReturn($storage);

    $this->account
      ->expects($this->any())
      ->method('hasPermission')
      ->will($this->returnValueMap([
        [
          "add or remove $vote_type_id votes on $entity_bundle of $entity_type_id",
          TRUE,
        ],
      ]));

    $this->assertInstanceOf(
      AccessResultAllowed::class,
      $this->controller->voteAccess(
        $entity_type_id,
        $vote_type_id,
        'dummy_entity_id'
      )
    );
  }

}
