<?php

namespace Drupal\Tests\social_secret_challenge\Unit;

use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\social_secret_challenge\ContentTranslationDefaultsConfigOverride;

/**
 * Content translation defaults config override test.
 *
 * @coversDefaultClass \Drupal\social_challenge\ContentTranslationDefaultsConfigOverride
 * @group social_secret_challenge
 */
class ContentTranslationDefaultsConfigOverrideTest extends UnitTestCase {

  /**
   * ContentTranslationDefaultsConfigOverride.
   *
   * @var \Drupal\social_secret_challenge\ContentTranslationDefaultsConfigOverride|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $configOverrides;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $module_handler = $this->prophesize(ModuleHandlerInterface::class);
    $module_handler->moduleExists('social_content_translation')->willReturn(TRUE);
    $this->configOverrides = new ContentTranslationDefaultsConfigOverride($module_handler->reveal());
  }

  /**
   * @covers ::loadOverrides
   */
  public function testLoadOverrides() {
    // There is no override if the config name is wrong.
    $names = [
      'test',
    ];

    $this->assertArrayEquals([], $this->configOverrides->loadOverrides($names));

    $names = [
      'language.content_settings.group.sc',
    ];

    // Assert content settings the config gets overriden correctly.
    $this->assertArrayEquals(
      [
        'language.content_settings.group.sc' => [
          'third_party_settings' => [
            'content_translation' => [
              'enabled' => TRUE,
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    $names = [
      'field.field.group.sc.field_group_image',
    ];

    // Assert group secret challenge fields config gets overriden correctly.
    $this->assertArrayEquals(
      [
        'field.field.group.sc.field_group_image' => [
          'third_party_settings' => [
            'content_translation' => [
              'translation_sync' => [
                'file' => 'file',
                'alt' => '0',
                'title' => '0',
              ],
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    $names = [
      'core.base_field_override.group.sc.path',
      'core.base_field_override.group.sc.label',
      'core.base_field_override.group.sc.menu_link',
      'core.base_field_override.group.sc.private_message_body',
    ];
    foreach ($names as $name) {
      // Assert group secret challenge fields config gets overriden correctly.
      $this->assertArrayEquals(
        [
          $name => [
            'translatable' => TRUE,
          ],
        ],
        $this->configOverrides->loadOverrides([$name])
      );
    }

  }

}
