<?php

namespace Drupal\Tests\social_secret_challenge\Unit;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\social_secret_challenge\SocialSecretChallengeConfigOverride;

/**
 * Social secret challenge config override test.
 *
 * @coversDefaultClass \Drupal\social_secret_challenge\SocialSecretChallengeConfigOverride
 * @group social_secret_challenge
 */
class SocialSecretChallengeConfigOverrideTest extends UnitTestCase {

  use ProphecyTrait;
  /**
   * Default configuration.
   *
   * @var array[]
   */
  protected array $defaultConfig = [
    'search_api.index.social_all' => [
      'datasource_settings' => [
        'entity:group' => [
          'bundles' => [
            'default' => TRUE,
            'selected' => [],
          ],
        ],
      ],
    ],
    'search_api.index.social_groups' => [
      'datasource_settings' => [
        'entity:group' => [
          'bundles' => [
            'default' => TRUE,
            'selected' => [],
          ],
        ],
      ],
    ],
    'views.view.challenges_user' => [
      'display' => [
        'default' => [
          'display_options' => [
            'filters' => [
              'type' => [
                'value' => [
                  'challenge' => 'challenge',
                ],
              ],
            ],
          ],
        ],
        'page' => [
          'display_options' => [
            'filters' => [
              'type' => [
                'value' => [
                  'challenge' => 'challenge',
                ],
              ],
            ],
          ],
        ],
      ],
    ],
    'views.view.challenge_ideas' => [
      'dependencies' => [
        'config' => [
          'test.dependency',
        ],
      ],
      'display' => [
        'default' => [
          'display_options' => [
            'filters' => [
              'type' => [
                'value' => [
                  'challenge-group_node-social_idea' => 'challenge-group_node-social_idea',
                ],
              ],
            ],
            'arguments' => [
              'gid' => [
                'validate_options' => [
                  'bundles' => [
                    'challenge' => 'challenge',
                  ],
                ],
              ],
            ],
          ],
        ],
        'block' => [
          'display_options' => [
            'filters' => [
              'type' => [
                'value' => [
                  'challenge-group_node-social_idea' => 'challenge-group_node-social_idea',
                ],
              ],
            ],
          ],
        ],
      ],
    ],
    'block.block.views_block__challenge_phases_block'  => [
      'visibility' => [
        'group_type' => [
          'group_types' => [
            'challenge' => 'challenge',
          ],
        ],
      ],
    ],
    'block.block.views_block__challenge_ideas_block'  => [
      'visibility' => [
        'group_type' => [
          'group_types' => [
            'challenge' => 'challenge',
          ],
        ],
      ],
    ],
  ];

  /**
   * SocialSecretChallengeConfigOverride.
   */
  protected SocialSecretChallengeConfigOverride $configOverrides;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $module_handler = $this->prophesize(ModuleHandlerInterface::class)->reveal();
    parent::setUp();
    $this->configOverrides = new SocialSecretChallengeConfigOverride($this->getConfigFactoryStub($this->defaultConfig), $module_handler);
  }

  /**
   * @covers ::loadOverrides
   */
  public function testLoadOverrides(): void {
    // There is no override if the config name is wrong.
    $names = [
      'test',
    ];

    $this->assertArrayEquals([], $this->configOverrides->loadOverrides($names));

    // Assert search api index for social_all and search_groups the config gets
    // overridden correctly.
    $names = [
      'search_api.index.social_all',
      'search_api.index.social_groups',
    ];
    foreach ($names as $name) {
      $this->assertArrayEquals(
        [
          $name => [
            'datasource_settings' => [
              'entity:group' => [
                'bundles' => [
                  'default' => TRUE,
                  'selected' => [
                    'sc',
                  ],
                ],
              ],
            ],
          ],
        ],
        $this->configOverrides->loadOverrides([$name])
      );
    }

    // Assert that challenges_user view config gets overridden correctly.
    $names = [
      'views.view.challenges_user',
    ];
    $expected_filters_values = [
      'challenge' => 'challenge',
      'sc' => 'sc',
    ];

    $this->assertArrayEquals(
      [
        'views.view.challenges_user' => [
          'display' => [
            'default' => [
              'display_options' => [
                'filters' => [
                  'type' => [
                    'value' => $expected_filters_values,
                  ],
                ],
              ],
            ],
            'page' => [
              'display_options' => [
                'filters' => [
                  'type' => [
                    'value' => $expected_filters_values,
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    // Assert that challenges_user view config gets overridden correctly.
    $names = [
      'views.view.challenge_ideas',
    ];
    $expected_dependencies = [
      'test.dependency',
      'group.content_type.sc-group_node-social_idea',
      'group.type.sc',
    ];
    $expected_filters_values = [
      'challenge-group_node-social_idea' => 'challenge-group_node-social_idea',
      'sc-group_node-social_idea' => 'sc-group_node-social_idea',
    ];
    $expected_bundles = [
      'challenge' => 'challenge',
      'sc' => 'sc',
    ];

    $this->assertArrayEquals(
      [
        'views.view.challenge_ideas' => [
          'dependencies' => [
            'config' => $expected_dependencies,
          ],
          'display' => [
            'default' => [
              'display_options' => [
                'filters' => [
                  'type' => [
                    'value' => $expected_filters_values,
                  ],
                ],
                'arguments' => [
                  'gid' => [
                    'validate_options' => [
                      'bundles' => $expected_bundles,
                    ],
                  ],
                ],
              ],
            ],
            'block' => [
              'display_options' => [
                'filters' => [
                  'type' => [
                    'value' => $expected_filters_values,
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    // Assert block views for phases and ideas the config gets overridden
    // correctly.
    $names = [
      'block.block.views_block__challenge_phases_block',
      'block.block.views_block__challenge_ideas_block',
    ];
    $expected_group_types = [
      'challenge' => 'challenge',
      'sc' => 'sc',
    ];

    foreach ($names as $name) {
      $this->assertArrayEquals(
        [
          $name => [
            'visibility' => [
              'group_type' => [
                'group_types' => $expected_group_types,
              ],
            ],
          ],
        ],
        $this->configOverrides->loadOverrides([$name])
      );
    }
  }

}
