<?php

namespace Drupal\social_secret_challenge\EventSubscriber;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Drupal\group\Entity\GroupInterface;
use Drupal\social_group_secret\EventSubscriber\SocialGroupSecretSubscriber;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Social secret challenge subscriber.
 *
 * @package Drupal\social_secret_challenge\EventSubscriber
 */
class SocialSecretChallengeSubscriber extends SocialGroupSecretSubscriber {

  /**
   * {@inheritdoc}
   */
  public function on403(RequestEvent $event): void {
    $group = $this->routeMatch->getParameter('group');

    // Show 404 page instead of 403 page for secret groups.
    if ($group instanceof GroupInterface && $group->bundle() === 'sc') {
      // Change the exception to show as 404 instead of 403.
      $event->setThrowable(new NotFoundHttpException());
    }
  }

}
