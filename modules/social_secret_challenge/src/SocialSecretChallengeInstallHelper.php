<?php

namespace Drupal\social_secret_challenge;

/**
 * Defines a helper class, sets permissions.
 */
class SocialSecretChallengeInstallHelper {

  /**
   * Set default permissions.
   */
  public function setPermissions(): void {
    $roles = [
      'contentmanager',
      'sitemanager',
    ];

    foreach ($roles as $role) {
      user_role_grant_permissions($role, ['create sc group']);
    }
  }

}
