<?php

namespace Drupal\social_secret_challenge\Plugin\MultipleContentBlock;

use Drupal\social_content_block\MultipleContentBlockBase;

/**
 * Provides a content block for secret challenges.
 *
 * @MultipleContentBlock(
 *   id = "secret_challenge_content",
 *   label = @Translation("Secret challenge"),
 *   entity_type = "group",
 *   bundle = "sc"
 * )
 */
class SecretChallengeContent extends MultipleContentBlockBase {

}
