CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers
* Translations


INTRODUCTION
------------

Provides Crowd Innovation feature.

- https://www.getopensocial.com/extensions/crowd-innovation/
- https://communitytalks.getopensocial.com/extensions/crowd-innovation-feature

REQUIREMENTS
------------

This module requires the following modules:

* [Inline Entity Form](https://www.drupal.org/project/inline_entity_form)
* Social Comment
* Social Event
* Social Group
* Social Idea Coauthors
* Social Idea
* Social Landing Page
* Social Tagging
* Social Topic
* Social Search
* [Views Data Export](https://www.drupal.org/project/views_data_export)


INSTALLATION
------------

Please use composer to install this module with its requirements.

Once installed you can use `drush` or the Drupal extensions page to enable this
module.


CONFIGURATION
-------------

* No configuration is needed.


MAINTAINERS
-----------

This project has been sponsored by:


* Open Social - https://www.drupal.org/open-social

TRANSLATIONS
-----------

[![Translation status](https://translate.getopensocial.com/widgets/open-social/-/social-challenge/287x66-white.png)](https://translate.getopensocial.com/projects/open-social/social-v/)

