<?php

namespace Drupal\social_challenge\Access;

use Drupal\group\Entity\GroupInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\Routing\Route;
use Drupal\social_challenge\ChallengeWrapper;
use Drupal\social_challenge_phase\Service\SocialChallengePhasePermissions;

/**
 * Determines access for group content target entity creation.
 */
class SocialChallengeCreateIdeaAccessCheck implements AccessInterface {

  /**
   * The challenge wrapper.
   */
  protected ChallengeWrapper $challengeWrapper;

  /**
   * The social challenge phase permissions service.
   */
  protected SocialChallengePhasePermissions $challengePhasePermissions;

  /**
   * Constructs a EntityCreateAccessCheck object.
   *
   * @param \Drupal\social_challenge\ChallengeWrapper $challenge_wrapper
   *   The challenge wrapper.
   * @param \Drupal\social_challenge_phase\Service\SocialChallengePhasePermissions $challenge_phase_permissions
   *   The social challenge phase permissions service.
   */
  public function __construct(ChallengeWrapper $challenge_wrapper, SocialChallengePhasePermissions $challenge_phase_permissions) {
    $this->challengeWrapper = $challenge_wrapper;
    $this->challengePhasePermissions = $challenge_phase_permissions;
  }

  /**
   * Checks access for Challenge Idea entity creation routes.
   *
   * All routes using this access check should have a group and plugin_id
   * parameter and have the _social_challenge_create_idea_access requirement set
   * to either 'TRUE' or 'FALSE'.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route to check against.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The currently logged in account.
   * @param \Drupal\group\Entity\GroupInterface $group
   *   The group in which the content should be created.
   * @param string $plugin_id
   *   The group content enabler ID to use for the group content entity.
   *
   *   The access result.
   */
  public function access(Route $route, AccountInterface $account, GroupInterface $group, string $plugin_id): AccessResult {
    if ($group->bundle() === 'challenge') {
      $access = FALSE;
      $needs_access = $route->getRequirement('_social_challenge_create_idea_access') === 'TRUE';
      $this->challengeWrapper->setChallenge($group);
      if ($group->hasPermission('administer phase permissions', $account)) {
        $access = TRUE;
      }
      elseif ($phase = $this->challengeWrapper->getActivePhase()) {
        $access = $this->challengePhasePermissions->hasPermissionValue('create idea in phase', $phase);
      }
      // Only allow access if the user can create Challenge Idea entities
      // using the provided plugin or if the user doesn't need access to do so.
      return AccessResult::allowedIf($access xor !$needs_access);
    }
    // If a route has multiple access checks, the andIf operation is used to
    // chain them together: all results must be AccessResult::allowed otherwise
    // access will be denied. See: https://www.drupal.org/node/2122195
    return AccessResult::allowed();
  }

}
