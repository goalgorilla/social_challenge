<?php

namespace Drupal\social_challenge;

use Drupal\group\Entity\GroupInterface;

/**
 * Provides an interface defining a Challenge Wrapper.
 *
 * @package Drupal\social_challenge
 */
interface ChallengeWrapperInterface {

  /**
   * Set a group instance.
   *
   * @param \Drupal\group\Entity\GroupInterface $group
   *   Group entity.
   *
   * @return \Drupal\social_challenge\ChallengeWrapperInterface
   *   Group entity.
   */
  public function setChallenge(GroupInterface $group);

  /**
   * Get a group instance.
   *
   * @return \Drupal\group\Entity\GroupInterface|null
   *   Group entity.
   */
  public function getChallenge();

  /**
   * Set list of available to handle bundles.
   *
   * @param array $bundles
   *   Array where each element is a machine name of a bundle.
   */
  public static function setAvailableBundles(array $bundles);

  /**
   * Get list of available to handle bundles.
   *
   * @return array
   *   Array where each element is a machine name of a bundle.
   */
  public static function getAvailableBundles();

  /**
   * Get all phases within a challenge.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   Array with node entities.
   */
  public function getPhases();

  /**
   * Get active phase within a challenge.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   An active phase entity.
   */
  public function getActivePhase();

  /**
   * Get active or last active phase within a challenge.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   An active phase entity.
   */
  public function getActiveOrLastPhase();

  /**
   * Get active phase name.
   *
   * @return string
   *   An active phase name.
   */
  public function getActivePhaseName();

  /**
   * Get active phase date range.
   *
   * @return string
   *   An active phase date range.
   */
  public function getActivePhaseDate();

  /**
   * Get challenges by the phase.
   *
   * @param string $phaseId
   *   The phase id.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array with challenge entities.
   */
  public function getChallengesByPhase(string $phaseId): array;

}
