<?php

namespace Drupal\social_challenge;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\menu_link_content\MenuLinkContentStorageInterface;
use Drupal\user\RoleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a helper class, sets permissions, creates menu links.
 */
class SocialChallengeInstallHelper extends SocialChallengeInstallHelperBase {

  use StringTranslationTrait;

  /**
   * The menu_link_content storage handler.
   */
  protected MenuLinkContentStorageInterface $menuLinkContentStorage;

  /**
   * Constructs SocialChallengeInstallHelper.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   * @param \Drupal\menu_link_content\MenuLinkContentStorageInterface $menu_link_content_storage
   *   The menu link content storage handler.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    MenuLinkContentStorageInterface $menu_link_content_storage
  ) {
    parent::__construct($entity_type_manager);

    $this->menuLinkContentStorage = $menu_link_content_storage;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_type.manager')->getStorage('menu_link_content')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getPermissions(string $role): array {
    // Anonymous.
    $permissions[RoleInterface::ANONYMOUS_ID] = [];

    // Authenticated.
    $permissions[RoleInterface::AUTHENTICATED_ID] = array_merge($permissions[RoleInterface::ANONYMOUS_ID], []);

    // Content manager.
    $permissions['contentmanager'] = array_merge($permissions[RoleInterface::AUTHENTICATED_ID], [
      'create challenge group',
    ]);

    // Site manager.
    $permissions['sitemanager'] = array_merge($permissions['contentmanager'], []);

    return $permissions[$role] ?? [];
  }

  /**
   * Create menu links.
   */
  public function createMenuLinks(): void {
    $menu_links = $this->menuLinkContentStorage->loadMultiple();
    $parent = NULL;

    /** @var \Drupal\menu_link_content\Entity\MenuLinkContent $menu_link */
    foreach ($menu_links as $menu_link) {
      if ($menu_link->label() === 'Explore' && $menu_link->isExpanded()) {
        $parent = 'menu_link_content:' . $menu_link->uuid();
      }
    }

    if (!is_null($parent)) {
      $this->menuLinkContentStorage->create([
        'title' => $this->t('All challenges'),
        'link' => ['uri' => 'internal:/all-challenges'],
        'menu_name' => 'main',
        'expanded' => FALSE,
        'weight' => 19,
        'parent' => $parent,
      ])->save();
    }
  }

}
