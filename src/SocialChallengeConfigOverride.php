<?php

namespace Drupal\social_challenge;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Social challenge config override.
 *
 * @package Drupal\social_challenge
 */
class SocialChallengeConfigOverride implements ConfigFactoryOverrideInterface {

  /**
   * The config.
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The module handler.
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The tagging service.
   *
   * @var \Drupal\social_tagging\SocialTaggingService
   */
  protected $tagService;

  /**
   * Are we in override mode?
   */
  protected bool $inOverride = FALSE;

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $module_handler) {
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Retrieves the social tagging service.
   *
   * @return \Drupal\social_tagging\SocialTaggingService
   *   The social tagging service.
   */
  protected function tagService() {
    // @codingStandardsIgnoreStart
    // We can't inject this service with DI because we got an issue with
    // interdependent services constructing.
    $this->tagService = \Drupal::service('social_tagging.tag_service');
    // @codingStandardsIgnoreEnd

    return $this->tagService;
  }

  /**
   * {@inheritdoc}
   */
  public function loadOverrides($names) {
    // Basically this makes sure we can load overrides without getting stuck
    // in a loop.
    if ($this->inOverride) {
      return [];
    }

    $this->inOverride = TRUE;

    $overrides = [];

    $social_group_types = [
      'open_group',
      'closed_group',
      'public_group',
      'secret_group',
      'flexible_group',
      'sc',
      'cc',
    ];
    $this->moduleHandler->alter('social_group_types', $social_group_types);

    $social_group_types_assoc = array_combine($social_group_types, $social_group_types);

    $arguments = [
      'gid' => [
        'specify_validation' => TRUE,
        'validate' => [
          'type' => 'entity:group',
        ],
        'validate_options' => [
          'access' => '',
          'bundles' => $social_group_types_assoc,
          'multiple' => FALSE,
          'operation' => 'view',
        ],
      ],
    ];

    $config_name = 'views.view.group_events';

    $filters_value = [];
    foreach ($social_group_types as $key) {
      $filters_value["{$key}-group_node-event"] = "{$key}-group_node-event";
    }

    if (in_array($config_name, $names)) {
      $overrides[$config_name] = [
        'display' => [
          'default' => [
            'display_options' => [
              'arguments' => $arguments,
              'filters' => [
                'type' => [
                  'value' => $filters_value,
                ],
              ],
            ],
          ],
        ],
      ];
    }

    $config_name = 'views.view.group_members';

    $filters_value = [];
    foreach ($social_group_types as $key) {
      $filters_value["{$key}-group_membership"] = "{$key}-group_membership";
    }

    if (in_array($config_name, $names)) {
      $overrides[$config_name] = [
        'display' => [
          'default' => [
            'display_options' => [
              'arguments' => $arguments,
              'filters' => [
                'type' => [
                  'value' => $filters_value,
                ],
              ],
            ],
          ],
        ],
      ];
    }

    $config_name = 'views.view.group_manage_members';

    if (in_array($config_name, $names)) {
      $overrides[$config_name] = [
        'display' => [
          'default' => [
            'display_options' => [
              'filters' => [
                'type' => [
                  'value' => [
                    'challenge-group_membership' => 'challenge-group_membership',
                    'cc-group_membership' => 'cc-group_membership',
                    'sc-group_membership' => 'sc-group_membership',
                  ],
                ],
              ],
            ],
          ],
        ],
      ];
    }

    $config_name = 'views.view.group_topics';

    $filters_value = [];
    foreach ($social_group_types as $key) {
      $filters_value["{$key}-group_node-topic"] = "{$key}-group_node-topic";
    }

    if (in_array($config_name, $names)) {
      $overrides[$config_name] = [
        'display' => [
          'default' => [
            'display_options' => [
              'arguments' => $arguments,
              'filters' => [
                'type' => [
                  'value' => $filters_value,
                ],
              ],
            ],
          ],
        ],
      ];
    }

    // Set view mode "Teaser" for "Challenge" group in Search All and Groups.
    $config_names = [
      'views.view.search_all',
      'views.view.search_groups',
    ];

    foreach ($config_names as $config_name) {
      if (in_array($config_name, $names)) {
        $config = $this->configFactory->get($config_name);
        $bundles = $config->get('display.default.display_options.row.options.view_modes.entity:group');
        $bundles['challenge'] = 'teaser';

        if (in_array($config_name, $names)) {
          $overrides[$config_name] = [
            'display' => [
              'default' => [
                'display_options' => [
                  'row' => [
                    'options' => [
                      'view_modes' => [
                        'entity:group' => $bundles,
                      ],
                    ],
                  ],
                ],
              ],
            ],
          ];
        }
      }
    }

    // Show profile hero block on My Challenges page.
    $config_name = 'block.block.socialblue_profile_hero_block';

    if (in_array($config_name, $names)) {
      $config = $this->configFactory->get($config_name);
      $pages = $config->get('visibility.request_path.pages');
      $pages .= "\r\n/user/*/challenges\r\n/user/*/ideas\r\n/user/*/discussions";
      $overrides[$config_name] = [
        'visibility' => [
          'request_path' => [
            'pages' => $pages,
          ],
        ],
      ];
    }

    // Hide page title block on My Challenges page and other pages.
    $config_name = 'block.block.socialblue_pagetitleblock_content';

    if (in_array($config_name, $names)) {
      $config = $this->configFactory->get($config_name);
      $pages = $config->get('visibility.request_path.pages');
      $pages .= "\r\n/user/*/challenges\r\n/user/*/ideas\r\n/user/*/discussions\r\n/user/*/courses\r\n/group/*/moderators\r\n/group/*/ideas";
      $overrides[$config_name] = [
        'visibility' => [
          'request_path' => [
            'pages' => $pages,
          ],
        ],
      ];
    }

    // Change formatter for social_idea comment field.
    $config_name = 'core.entity_view_display.node.social_idea.default';

    if (in_array($config_name, $names)) {
      $overrides[$config_name] = [
        'content' => [
          'field_social_idea_comments' => [
            'type' => 'comment_idea',
          ],
        ],
      ];
    }

    // Add tagging fields to the views filters.
    $config_overviews = [
      'views.view.challenges_all' => 'page_all_challenges',
    ];

    foreach ($config_overviews as $config_name => $display) {
      if (in_array($config_name, $names)) {

        // Prepare fields.
        $fields = [];
        $fields['social_tagging_target_id'] = [
          'identifier' => 'tag',
          'label' => 'Tags',
        ];

        if ($this->tagService()->allowSplit()) {
          $fields = [];
          foreach ($this->tagService()->getCategories() as $tid => $value) {
            if (!empty($this->tagService()->getChildren($tid))) {
              $fields['social_tagging_target_id_' . $tid] = [
                'identifier' => social_tagging_to_machine_name($value),
                'label' => $value,
              ];
            }
          }
        }

        $overrides[$config_name]['dependencies']['config'][] = 'taxonomy.vocabulary.social_tagging';
        $overrides[$config_name]['display'][$display]['cache_metadata']['contexts'][] = 'user';

        $group = 1;

        if (count($fields) > 1) {
          $overrides[$config_name]['display'][$display]['display_options']['filter_groups']['groups'][1] = 'AND';
          $overrides[$config_name]['display'][$display]['display_options']['filter_groups']['groups'][2] = 'OR';
          $group++;
        }

        foreach ($fields as $field => $data) {
          $overrides[$config_name]['display'][$display]['display_options']['filters'][$field] = [
            'id' => $field,
            'table' => 'group__social_tagging',
            'field' => 'social_tagging_target_id',
            'relationship' => 'none',
            'group_type' => 'group',
            'admin_label' => '',
            'operator' => '=',
            'value' => [
              'min' => '',
              'max' => '',
              'value' => '',
            ],
            'group' => $group,
            'exposed' => TRUE,
            'expose' => [
              'operator_id' => $field . '_op',
              'label' => $data['label'],
              'description' => '',
              'use_operator' => FALSE,
              'operator' => $field . '_op',
              'identifier' => $data['identifier'],
              'required' => FALSE,
              'remember' => FALSE,
              'multiple' => FALSE,
              'remember_roles' => [
                'authenticated' => 'authenticated',
                'anonymous' => '0',
                'administrator' => '0',
                'contentmanager' => '0',
                'sitemanager' => '0',
              ],
              'placeholder' => '',
              'min_placeholder' => '',
              'max_placeholder' => '',
            ],
            'is_grouped' => FALSE,
            'group_info' => [
              'label' => '',
              'description' => '',
              'identifier' => '',
              'optional' => TRUE,
              'widget' => 'select',
              'multiple' => FALSE,
              'remember' => FALSE,
              'default_group' => 'All',
              'default_group_multiple' => [],
              'group_items' => [],
            ],
            'entity_type' => 'node',
            'entity_field' => 'social_tagging',
            'plugin_id' => 'numeric',
          ];
        }

      }
    }

    // Add a new field to groups search index.
    $config_name = 'search_api.index.social_groups';

    if (in_array($config_name, $names)) {
      // Search index will not be updated if we only add these overrides.
      // To apply them just disable/enable search index.
      // @see social_challenge_post_update_0001_fix_search_indexes()
      $overrides[$config_name]['field_settings']['challenge_phases_status'] = [
        'label' => 'Challenge Phases Status',
        'property_path' => 'challenge_phases_status',
        'type' => 'boolean',
        'dependencies' => [
          'module' => [
            'search_api',
          ],
        ],
      ];
    }

    // Add a new filter to groups search view.
    $config_name = 'views.view.search_groups';
    if (in_array($config_name, $names)) {
      $overrides[$config_name]['display']['default']['display_options']['filters']['challenge_phases_status'] = [
        'id' => 'challenge_phases_status',
        'table' => 'search_api_index_social_groups',
        'field' => 'challenge_phases_status',
        'relationship' => 'none',
        'group_type' => 'group',
        'admin_label' => '',
        'operator' => '=',
        'value' => 'All',
        'group' => 1,
        'exposed' => TRUE,
        'expose' => [
          'operator_id' => '',
          'label' => 'Challenge Status',
          'description' => '',
          'use_operator' => FALSE,
          'operator' => 'search_api_challenge_status_op',
          'operator_limit_selection' => FALSE,
          'operator_list' => [],
          'identifier' => 'challenge_phases_status',
          'required' => FALSE,
          'remember' => FALSE,
          'multiple' => FALSE,
          'remember_roles' => [
            'authenticated' => 'authenticated',
            'anonymous' => '0',
            'administrator' => '0',
            'contentmanager' => '0',
            'sitemanager' => '0',
          ],
        ],
        'is_grouped' => FALSE,
        'group_info' => [
          'label' => '',
          'description' => '',
          'identifier' => '',
          'optional' => TRUE,
          'widget' => 'select',
          'multiple' => FALSE,
          'remember' => FALSE,
          'default_group' => 'All',
          'default_group_multiple' => [],
          'group_items' => [],
        ],
        'plugin_id' => 'search_api_boolean',
      ];
    }

    $config_name = 'message.template.create_topic_gc';

    if (in_array($config_name, $names, FALSE)) {
      $overrides[$config_name]['third_party_settings']['activity_logger']['activity_bundle_entities'] =
        [
          'group_content-challenge-group_node-topic' => 'group_content-challenge-group_node-topic',
        ];
    }

    $this->inOverride = FALSE;

    return $overrides;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheSuffix(): string {
    return 'SocialChallengeConfigOverride';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata($name): CacheableMetadata {
    return new CacheableMetadata();
  }

  /**
   * {@inheritdoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

}
