<?php

namespace Drupal\social_challenge\Controller;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Cache\CacheableDependencyInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\social_challenge\ChallengeWrapper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Social challenge controller.
 *
 * @package Drupal\social_challenge\Controller
 */
class SocialChallengeController extends ControllerBase {

  /**
   * The challenge wrapper.
   */
  protected ChallengeWrapper $challengeWrapper;

  /**
   * SocialChallengeController constructor.
   *
   * @param \Drupal\social_challenge\ChallengeWrapper $challenge_wrapper
   *   The challenge wrapper.
   */
  public function __construct(ChallengeWrapper $challenge_wrapper) {
    $this->challengeWrapper = $challenge_wrapper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('social_challenge.wrapper')
    );
  }

  /**
   * Redirect to default group home page.
   */
  public function redirectToGroupHomePage(GroupInterface $group): RedirectResponse {
    // Redirect non-members of challenge group to the about page.
    // Redirect to the group stream by default.
    $route = 'social_group.stream';
    $course_bundles = [
      'course_advanced',
      'course_basic',
    ];
    $challenge_bundles = [
      'challenge',
      'cc',
    ];
    if (in_array($group->bundle(), $course_bundles) || (in_array($group->bundle(), $challenge_bundles) && !$group->getMember($this->currentUser()))) {
      $route = 'view.group_information.page_group_about';
    }

    return $this->redirect($route, [
      'group' => $group->id(),
    ]);
  }

  /**
   * Controller action.
   *
   * @return mixed
   *   Renderable drupal array.
   */
  public function content() {
    $content = [];
    $bundles = $this->challengeWrapper->getAvailableBundles();

    foreach ($this->entityTypeManager()->getStorage('group_type')->loadMultiple() as $type) {
      if (in_array($type->id(), $bundles)) {
        $content[$type->id()] = $type;
      }
    }
    arsort($content);
    if (count($content) == 1) {
      $type = array_shift($content);
      return $this->redirect('entity.group.add_form', ['group_type' => $type->id()]);
    }

    return [
      '#theme' => 'challenge_add_list',
      '#content' => $content,
    ];
  }

  /**
   * Determines if user has access to course creation page.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   *   The access result.
   */
  public function access(AccountInterface $account): CacheableDependencyInterface {
    $bundles = $this->challengeWrapper->getAvailableBundles();

    foreach ($bundles as $bundle) {
      if ($account->hasPermission("create {$bundle} group")) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();
  }

}
