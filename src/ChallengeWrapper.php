<?php

namespace Drupal\social_challenge;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

/**
 * Challenge wrapper.
 *
 * @package Drupal\social_challenge
 */
class ChallengeWrapper implements ChallengeWrapperInterface {

  /**
   * The entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The current user.
   */
  protected AccountInterface $currentUser;

  /**
   * The date formatter service.
   */
  protected DateFormatterInterface $dateFormatter;

  /**
   * The current group entity.
   */
  protected ?GroupInterface $group = NULL;

  /**
   * List of available to handle bundles.
   */
  protected static array $bundles = ['challenge', 'cc', 'sc'];

  /**
   * ChallengeWrapper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, AccountInterface $current_user, DateFormatterInterface $date_formatter) {
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $current_user;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public function setChallenge(GroupInterface $group): self {
    if (!in_array($group->bundle(), self::$bundles, TRUE)) {
      throw new InvalidArgumentException(sprintf('%s bundle is not allowed. Allowed bundles: %s', $group->bundle(), implode(', ', self::$bundles)));
    }

    $this->group = $group;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getChallenge(): ?GroupInterface {
    return $this->group;
  }

  /**
   * {@inheritdoc}
   */
  public static function setAvailableBundles(array $bundles): void {
    if (!$bundles) {
      throw new InvalidArgumentException('$bundles argument cannot be empty');
    }

    self::$bundles = $bundles;
  }

  /**
   * {@inheritdoc}
   */
  public static function getAvailableBundles(): array {
    return self::$bundles;
  }

  /**
   * {@inheritdoc}
   */
  public function getPhases(): array {
    $phases = [];
    $challenge = $this->getChallenge();

    if ($challenge instanceof GroupInterface && !$challenge->get('field_challenge_phases')->isEmpty()) {
      $ids = [];
      foreach ($challenge->get('field_challenge_phases')->getValue() as $item) {
        $ids[] = $item['target_id'];
      }
      $storage = $this->entityTypeManager->getStorage('phase');
      $phases = $storage->loadMultiple($ids);
    }
    return $phases;
  }

  /**
   * {@inheritdoc}
   */
  public function getActivePhase(): ?EntityInterface {
    $phases = $this->getPhases();
    foreach ($phases as $phase) {
      if ($phase->isActive()) {
        return $phase;
      }
    }
    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function getActiveOrLastPhase(): ?EntityInterface {
    $last_active = NULL;
    $phases = $this->getPhases();
    foreach ($phases as $phase) {
      // If we have active phase we are done.
      if ($phase->isActive()) {
        return $phase;
      }

      if ($phase->isPast()) {
        // Compare which phase was latest if we have few of them.
        if ($last_active) {
          $current_end_date = $phase->get('field_phase_date')->end_date->getTimestamp();
          $last_end_date = $last_active->get('field_phase_date')->end_date->getTimestamp();
          if ($current_end_date > $last_end_date) {
            $last_active = $phase;
          }
        }
        else {
          $last_active = $phase;
        }
      }
    }
    return $last_active;
  }

  /**
   * {@inheritdoc}
   */
  public function getActivePhaseName(): ?string {
    $name = NULL;
    if ($phase = $this->getActivePhase()) {
      $name = $phase->label();
    }
    return $name;
  }

  /**
   * {@inheritdoc}
   */
  public function getActivePhaseDate() {
    $formatted_date = NULL;
    if ($phase = $this->getActivePhase()) {
      $field_date = $phase->get('field_phase_date')->getValue();

      if (!empty($field_date[0])) {

        $formatter = $this->dateFormatter;
        $start_time = strtotime($field_date[0]['value']);
        $end_time = strtotime($field_date[0]['end_value']);

        if (date('Y', $start_time) !== date('Y', $end_time)) {
          $date = [
            $formatter->format($start_time, 'social_medium_date'),
            $formatter->format($end_time, 'social_medium_date'),
          ];
        }
        elseif ($field_date[0]['value'] === $field_date[0]['end_value']) {
          $date = [
            $formatter->format($end_time, 'social_medium_date'),
          ];
        }
        else {
          $date = [
            $formatter->format($start_time, 'day_month'),
            $formatter->format($end_time, 'social_medium_date'),
          ];
        }

        $formatted_date = implode(' - ', $date);
      }
    }
    return $formatted_date;
  }

  /**
   * {@inheritdoc}
   */
  public function getChallengesByPhase(string $phaseId): array {
    $groupStorage = $this->entityTypeManager->getStorage('group');
    $challengesIds = $groupStorage->getQuery()
      ->condition('type', ChallengeWrapper::getAvailableBundles(), 'IN')
      ->condition('field_challenge_phases', $phaseId)
      ->execute();

    return $challengesIds
      ? $groupStorage->loadMultiple($challengesIds)
      : [];
  }

}
