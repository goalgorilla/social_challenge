<?php

namespace Drupal\social_challenge\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Route subscriber.
 *
 * @package Drupal\social_challenge\Routing
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  public function alterRoutes(RouteCollection $collection): void {
    // Route the group view page to group stream or group about page.
    if ($route = $collection->get('entity.group.canonical')) {
      $route->setPath('/group/{group}');
      $route->setDefault('_controller', '\Drupal\social_challenge\Controller\SocialChallengeController::redirectToGroupHomePage');
    }
    // Set additional access to create ideas in challenge group.
    if ($route = $collection->get('entity.group_content.create_form')) {
      $route->setRequirement('_social_challenge_create_idea_access', 'TRUE');
    }

  }

}
