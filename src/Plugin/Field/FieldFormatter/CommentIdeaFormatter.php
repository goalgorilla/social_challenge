<?php

namespace Drupal\social_challenge\Plugin\Field\FieldFormatter;

use Drupal\comment\Plugin\Field\FieldFormatter\CommentDefaultFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'comment_idea' formatter.
 *
 * @FieldFormatter(
 *   id = "comment_idea",
 *   label = @Translation("Comment on idea"),
 *   field_types = {
 *     "comment"
 *   }
 * )
 */
class CommentIdeaFormatter extends CommentDefaultFormatter {

  /**
   * The challenge wrapper.
   */
  protected ?object $challengeWrapper = NULL;

  /**
   * The challenge phase permissions service.
   */
  protected ?object $challengePhasePermissions = NULL;

  /**
   * TRUE if the request is a XMLHttpRequest.
   */
  private bool $isXmlHttpRequest;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->challengeWrapper = $container->get('social_challenge.wrapper');
    $instance->challengePhasePermissions = $container->get('social_challenge_phase.permissions');
    $instance->isXmlHttpRequest = $container->get('request_stack')->getCurrentRequest()->isXmlHttpRequest();

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $output = parent::viewElements($items, $langcode);
    $entity = $items->getEntity();

    // Exclude entities without the id.
    if (!empty($entity->id())) {
      $group_contents = GroupContent::loadByEntity($entity);
    }

    if (!empty($output[0]['comments']) && !$this->isXmlHttpRequest) {
      $comment_settings = $this->getFieldSettings();
      $output[0]['comments'] = [
        '#lazy_builder' => [
          'social_comment.lazy_renderer:renderComments',
          [
            $entity->getEntityTypeId(),
            $entity->id(),
            $comment_settings['default_mode'],
            $items->getName(),
            $comment_settings['per_page'],
            $this->getSetting('pager_id'),
          ],
        ],
        '#create_placeholder' => TRUE,
      ];
    }

    if (!empty($group_contents)) {
      $group = reset($group_contents)->getGroup();
      if ($group instanceof GroupInterface && $group->bundle() === 'challenge') {
        $account = $this->currentUser;
        $is_phase_admin = $group->hasPermission('administer phase permissions', $account);

        $this->challengeWrapper->setChallenge($group);
        $phase = $this->challengeWrapper->getActivePhase();
        $is_comment_allowed = $phase && $this->challengePhasePermissions->hasPermissionValue('comment in phase', $phase);

        if ($is_phase_admin || $is_comment_allowed) {
          return $output;
        }
        else {
          $output[0]['comment_form'] = [
            '#prefix' => '<hr>',
            '#markup' => $this->t('Comments are closed.'),
          ];
        }
      }
    }

    return $output;
  }

}
