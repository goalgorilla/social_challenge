<?php

namespace Drupal\social_challenge\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\social_challenge\ChallengeWrapper;
use Drupal\social_challenge_phase\Service\SocialChallengePhasePermissions;

/**
 * Provides a 'GroupAddIdeaBlock' block.
 *
 * @Block(
 *   id = "group_add_idea_block",
 *   admin_label = @Translation("Group add idea"),
 *   context_definitions = {
 *     "group" = @ContextDefinition("entity:group")
 *   }
 * )
 */
class GroupAddIdeaBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The challenge wrapper.
   */
  protected ChallengeWrapper $challengeWrapper;

  /**
   * The challenge phase permissions service.
   */
  protected SocialChallengePhasePermissions $challengePhasePermissions;

  /**
   * The current user.
   */
  protected AccountInterface $currentUser;

  /**
   * ProfileHeroBlock constructor.
   *
   * @param array $configuration
   *   The given configuration.
   * @param string $plugin_id
   *   The given plugin id.
   * @param mixed $plugin_definition
   *   The given plugin definition.
   * @param \Drupal\social_challenge\ChallengeWrapper $challenge_wrapper
   *   The challenge wrapper.
   * @param \Drupal\social_challenge_phase\Service\SocialChallengePhasePermissions $challenge_phase_permissions
   *   The social challenge phase permissions service.
   * @param \Drupal\Core\Session\AccountInterface $current_user
   *   The current user.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ChallengeWrapper $challenge_wrapper, SocialChallengePhasePermissions $challenge_phase_permissions, AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->challengeWrapper = $challenge_wrapper;
    $this->challengePhasePermissions = $challenge_phase_permissions;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('social_challenge.wrapper'),
      $container->get('social_challenge_phase.permissions'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   *
   * Custom access logic to display the block.
   */
  public function accessButton(): bool {
    $access = FALSE;
    $account = $this->currentUser;

    $group = $this->getContextValue('group');
    $this->challengeWrapper->setChallenge($group);

    if (!$group->hasPermission('create group_node:social_idea entity', $account)) {
      $access = FALSE;
    }

    if ($group->hasPermission('administer phase permissions', $account)) {
      $access = TRUE;
    }
    elseif ($phase = $this->challengeWrapper->getActivePhase()) {
      if ($this->challengePhasePermissions->hasPermissionValue('create idea in phase', $phase)) {
        $access = TRUE;
      }
    }
    return $access;
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [];
    $group = $this->getContextValue('group');

    $link_options = [
      'attributes' => [
        'class' => [
          'btn',
          'btn-primary',
          'btn-raised',
          'waves-effect',
        ],
      ],
    ];

    if (!$this->accessButton()) {
      $link_options['attributes']['disabled'] = TRUE;
      $link_options['attributes']['onclick'] = "event.preventDefault()";
      $link_options['attributes']['title'] = $this->t('Creating an idea is not made possible in this phase.');
    }

    $build['content'] = _social_challenge_add_idea_button($group, $this->currentUser, $link_options);
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheTags(): array {
    $group = $this->getContextValue('group');

    return Cache::mergeTags(parent::getCacheTags(), ['group:' . $group->id()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts(): array {
    return Cache::mergeContexts(parent::getCacheContexts(), ['url.path']);
  }

}
