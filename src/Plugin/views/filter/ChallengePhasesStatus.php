<?php

namespace Drupal\social_challenge\Plugin\views\filter;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Query\SelectInterface;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\StringTranslation\TranslationInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\views\Plugin\views\display\DisplayPluginBase;
use Drupal\views\Plugin\views\filter\InOperator;
use Drupal\views\ViewExecutable;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Challenges ending status (active || inactive).
 *
 *  Challenge Phases Status depends on phases ending date. If phase end date is
 *  in the past that means a challenge is "inactive" and vise versa -
 *  if in future then a challenge is "active".
 *
 * @ingroup views_filter_handlers
 *
 * @ViewsFilter("challenge_phases_status")
 */
class ChallengePhasesStatus extends InOperator {

  /**
   * The default database connection.
   */
  protected Connection $connection;

  /**
   * The date formatter object.
   */
  protected DateFormatter $dateFormatter;

  /**
   * The time object.
   */
  protected TimeInterface $time;

  /**
   * Flag to indicate active challenges.
   */
  const ACTIVE = 'active';

  /**
   * Flag to indicate inactive challenges.
   */
  const INACTIVE = 'inactive';

  /**
   * Constructs a new ChallengeStatus object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param string $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\StringTranslation\TranslationInterface $string_translation
   *   The string translation manager.
   * @param \Drupal\Core\Database\Connection $connection
   *   The default database connection.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The DateFormatter definition.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    TranslationInterface $string_translation,
    Connection $connection,
    DateFormatter $date_formatter,
    TimeInterface $time
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->setStringTranslation($string_translation);
    $this->connection = $connection;
    $this->dateFormatter = $date_formatter;
    $this->time = $time;

    $this->valueFormType = 'radios';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('string_translation'),
      $container->get('database'),
      $container->get('date.formatter'),
      $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL): void {
    parent::init($view, $display, $options);
    $this->valueTitle = $this->t('Allowed values');
    $this->definition['options callback'] = [$this, 'generateOptions'];
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    $value = is_array($this->value) ? current($this->value) : $this->value;

    if (!in_array($value, $this->generateOptions(TRUE))) {
      return;
    }

    $this->ensureMyTable();

    // Current time in "Y-m-dTH:i:s" format.
    $now = $this->dateFormatter->format($this->time->getCurrentTime(), 'custom', DateTimeItemInterface::DATETIME_STORAGE_FORMAT);

    // We consider challenge as an active if it has a phase with date values
    // compatible with this condition: (start < now) AND (now < end).
    $activeChallengesSubQuery = $this->getBaseSubQuery();
    // Start Date in the past.
    $activeChallengesSubQuery->condition('pfpd.field_phase_date_value', $now, '<');
    // End Date in the future.
    $activeChallengesSubQuery->condition('pfpd.field_phase_date_end_value', $now, '>');

    /** @var \Drupal\views\Plugin\views\query\Sql $filter_query */
    $filter_query = $this->query;

    if ($value === self::ACTIVE) {
      $filter_query->addWhere($this->options['group'], 'groups_field_data.id', $activeChallengesSubQuery, 'IN');
    }

    // We consider challenge as inactive if it has a phase with date values
    // compatible with this condition: (start > now) OR (now < end).
    if ($value === self::INACTIVE) {
      $inactiveChallengesSubQuery = $this->getBaseSubQuery();
      $or = $inactiveChallengesSubQuery->orConditionGroup();
      // Start Date in the future.
      $or->condition('pfpd.field_phase_date_value', $now, '>');
      // End Date in the past.
      $or->condition('pfpd.field_phase_date_end_value', $now, '<');
      $inactiveChallengesSubQuery->condition($or);

      // Additionally, we add a subQuery with active challenges to exclude them.
      $inactiveChallengesSubQuery->condition('groups_field_data.id', $activeChallengesSubQuery, 'NOT IN');
      $filter_query->addWhere($this->options['group'], 'groups_field_data.id', $inactiveChallengesSubQuery, 'IN');
    }
  }

  /**
   * Helper method to build base sub-query with challenges phases status.
   *
   *   The query object.
   */
  protected function getBaseSubQuery(): SelectInterface {
    $query = $this->connection->select('group__field_challenge_phases', 'gfcp');
    $query->addField('gfd', 'id');
    $query->leftJoin(
      'phase__field_phase_date',
      'pfpd',
      'pfpd.entity_id = gfcp.field_challenge_phases_target_id'
    );
    $query->leftJoin(
      'groups_field_data',
      'gfd',
      'gfd.id = gfcp.entity_id'
    );
    return $query;
  }

  /**
   * Skip validation if no options have been chosen.
   */
  public function validate(): void {
    if (!empty($this->value)) {
      parent::validate();
    }
  }

  /**
   * Retrieves the allowed values for the filter.
   *
   * @param bool $inverse
   *   Set "TRUE" to get inverse array.
   *
   * @return array
   *   An array of allowed values in the form key => label.
   */
  public function generateOptions(bool $inverse = FALSE): array {
    $options = [
      self::ACTIVE => $this->t('Active'),
      self::INACTIVE => $this->t('Inactive'),
    ];

    return $inverse ? array_keys($options) : $options;
  }

}
