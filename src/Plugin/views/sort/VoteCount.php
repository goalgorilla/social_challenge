<?php

namespace Drupal\social_challenge\Plugin\views\sort;

use Drupal\views\Plugin\views\sort\SortPluginBase;
use Drupal\views\Plugin\ViewsHandlerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sort handler to sort by number of votes node has.
 *
 * @ingroup views_sort_handlers
 *
 * @ViewsSort("vote_count")
 */
class VoteCount extends SortPluginBase {

  /**
   * Views handler manager for adding the join.
   */
  protected ViewsHandlerManager $viewsHandler;

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\views\Plugin\ViewsHandlerManager $views_handler
   *   Database Service Object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ViewsHandlerManager $views_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->viewsHandler = $views_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('plugin.manager.views.join'));
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    $definition = [
      'table' => 'votingapi_result',
      'field' => 'entity_id',
      'left_table' => 'node_field_data_group_content_field_data',
      'left_field' => 'nid',
      'extra' => [
        [
          'field' => 'entity_type',
          'value' => 'node',
        ],
        [
          'field' => 'function',
          'value' => 'vote_count',
        ],
      ],
    ];
    $join = $this->viewsHandler->createInstance('standard', $definition);
    $this->query->addRelationship('votingapi_result', $join, 'node_field_data');
    $this->query->addOrderBy('votingapi_result', 'value', $this->options['order']);

  }

}
