<?php

namespace Drupal\social_challenge\Plugin\views\field;

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\Plugin\ViewsHandlerManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler to display number of votes node has.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("vote_count")
 */
class VoteCount extends FieldPluginBase {

  /**
   * Views handler manager for adding the join.
   */
  protected ViewsHandlerManager $viewsHandler;

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\views\Plugin\ViewsHandlerManager $views_handler
   *   Database Service Object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ViewsHandlerManager $views_handler) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->viewsHandler = $views_handler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new static($configuration, $plugin_id, $plugin_definition, $container->get('plugin.manager.views.join'));
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    $definition = [
      'table' => 'votingapi_result',
      'field' => 'entity_id',
      'left_table' => 'node_field_data_group_content_field_data',
      'left_field' => 'nid',
      'extra' => [
        [
          'field' => 'entity_type',
          'value' => 'node',
        ],
        [
          'field' => 'function',
          'value' => 'vote_count',
        ],
      ],
    ];
    $join = $this->viewsHandler->createInstance('standard', $definition);
    $this->query->addRelationship('votingapi_result', $join, 'node_field_data');
    $this->field_alias = $this->query->addField('votingapi_result', 'value');

  }

}
