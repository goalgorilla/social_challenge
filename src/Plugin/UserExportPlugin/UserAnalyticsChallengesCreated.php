<?php

namespace Drupal\social_challenge\Plugin\UserExportPlugin;

use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\social_user_export\Plugin\UserExportPluginBase;
use Drupal\user\UserInterface;

/**
 * Provides a 'UserAnalyticsChallengesCreated' user export row.
 *
 * @UserExportPlugin(
 *  id = "user_challenges_created",
 *  label = @Translation("Challenges created"),
 *  weight = -236,
 * )
 */
class UserAnalyticsChallengesCreated extends UserExportPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getHeader(): TranslatableMarkup {
    return $this->t('Challenges created');
  }

  /**
   * {@inheritdoc}
   */
  public function getValue(UserInterface $entity): string {
    $query = $this->database->select('groups_field_data', 'g');
    $query
      ->condition('g.uid', (string) $entity->id())
      ->condition('g.type', 'challenge');

    /** @var \Drupal\Core\Database\StatementInterface $result */
    $result = $query->countQuery()->execute();
    return $result->fetchField();
  }

}
