<?php

namespace Drupal\social_challenge\Plugin\search_api\processor;

use Drupal\search_api\Datasource\DatasourceInterface;
use Drupal\search_api\IndexInterface;
use Drupal\search_api\Item\ItemInterface;
use Drupal\search_api\Processor\ProcessorPluginBase;
use Drupal\search_api\Processor\ProcessorProperty;
use Drupal\social_challenge\ChallengeWrapper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Challenge status by phases (retrieving from phases "date" field values).
 *
 * @SearchApiProcessor(
 *   id = "challenge_phases_status",
 *   label = @Translation("Challenge Phases Status"),
 *   description = @Translation("Challenge status by phases."),
 *   stages = {
 *     "add_properties" = 0,
 *   },
 *   locked = true,
 *   hidden = true,
 * )
 */
class ChallengePhasesStatus extends ProcessorPluginBase {

  /**
   * The challenge wrapper.
   */
  protected ChallengeWrapper $challengeWrapper;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    /** @var static $processor */
    $processor = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $processor->setChallengeWrapper($container->get('social_challenge.wrapper'));
    return $processor;
  }

  /**
   * Sets the challenge wrapper.
   *
   * @param \Drupal\social_challenge\ChallengeWrapper $challenge_wrapper
   *   The challenge wrapper service.
   *
   * @return $this
   */
  public function setChallengeWrapper(ChallengeWrapper $challenge_wrapper): ChallengePhasesStatus {
    $this->challengeWrapper = $challenge_wrapper;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function supportsIndex(IndexInterface $index): bool {
    foreach ($index->getDatasources() as $datasource) {
      if (in_array($datasource->getEntityTypeId(), ['group'])) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function getPropertyDefinitions(DatasourceInterface $datasource = NULL) {
    $properties = [];

    if (!$datasource) {
      $definition = [
        'label' => $this->t('Challenge Phases Status'),
        'description' => $this->t('Challenge "active/inactive" status by phases.'),
        'type' => 'boolean',
        'processor_id' => $this->getPluginId(),
      ];
      $properties['challenge_phases_status'] = new ProcessorProperty($definition);
    }

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function addFieldValues(ItemInterface $item): void {
    // Process only challenges.
    $dataSource = $item->getDatasource();
    if ($dataSource->getEntityTypeId() !== 'group') {
      return;
    }

    /** @var \Drupal\Core\TypedData\ComplexDataInterface $original_object */
    $original_object = $item->getOriginalObject();
    $group = $original_object->getValue();

    if (!in_array($group->bundle(), $this->challengeWrapper::getAvailableBundles(), TRUE)) {
      return;
    }

    $phases = $group->hasField('field_challenge_phases')
      ? $group->get('field_challenge_phases')->referencedEntities()
      : [];

    // Do not process challenges without phases.
    if (empty($phases)) {
      return;
    }

    $active = FALSE;
    foreach ($phases as $phase) {
      /** @var \Drupal\social_challenge_phase\Entity\Phase $phase */
      if ($phase->isActive()) {
        $active = TRUE;
        // No need continue looping.
        break;
      }
    }

    $fields = $item->getFields();
    $fields = $this->getFieldsHelper()
      ->filterForPropertyPath($fields, NULL, 'challenge_phases_status');
    foreach ($fields as $field) {
      $field->addValue($active);
    }
  }

}
