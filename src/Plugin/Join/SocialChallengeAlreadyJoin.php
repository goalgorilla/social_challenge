<?php

namespace Drupal\social_challenge\Plugin\Join;

use Drupal\social_challenge\ChallengeWrapperInterface;
use Drupal\social_group\EntityMemberInterface;
use Drupal\social_group\Plugin\Join\SocialGroupAlreadyJoin;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a join plugin instance for members.
 */
class SocialChallengeAlreadyJoin extends SocialGroupAlreadyJoin {

  use SocialChallengeJoinTrait;

  /**
   * The challenge wrapper.
   */
  private ChallengeWrapperInterface $wrapper;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ): self {
    /** @var self $instance */
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition,
    );

    $instance->wrapper = $container->get('social_challenge.wrapper');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function actions(EntityMemberInterface $entity, array &$variables): array {
    $items = parent::actions($entity, $variables);

    if (
      $entity->getEntityTypeId() !== 'group' ||
      !in_array($entity->bundle(), $this->wrapper::getAvailableBundles())
    ) {
      return $items;
    }

    if (!empty($items)) {
      /** @var \Drupal\Core\Link $link */
      $link = $items[1];

      $items = [
        [
          'label' => $this->t('Unfollow'),
          'url' => $link->getUrl(),
        ],
      ];
    }

    /** @var \Drupal\social_group\SocialGroupInterface $entity */
    return $this->alter($entity, $items);
  }

}
