<?php

namespace Drupal\social_challenge\Plugin\Join;

use Drupal\social_group\EntityMemberInterface;
use Drupal\social_group\Plugin\Join\SocialGroupDirectJoin;

/**
 * Provides a join plugin instance for joining directly.
 */
class SocialChallengeDirectJoin extends SocialGroupDirectJoin {

  use SocialChallengeJoinTrait;

  /**
   * {@inheritdoc}
   */
  public function actions(EntityMemberInterface $entity, array &$variables): array {
    if (!empty($items = parent::actions($entity, $variables))) {
      $items[0]['label'] = $this->t('Follow');
    }

    /** @var \Drupal\social_group\SocialGroupInterface $entity */
    return $this->alter($entity, $items);
  }

}
