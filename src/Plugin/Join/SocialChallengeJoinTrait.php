<?php

namespace Drupal\social_challenge\Plugin\Join;

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Link;
use Drupal\social_group\SocialGroupInterface;

/**
 * Provides a trait for processing a list of join method actions.
 */
trait SocialChallengeJoinTrait {

  /**
   * Add a button for creating an idea in the first position of the list.
   *
   * @param \Drupal\social_group\SocialGroupInterface $group
   *   The group entity object.
   * @param array $items
   *   The clickable elements list.
   */
  public function alter(SocialGroupInterface $group, array $items): array {
    if (
      (empty($items) || isset($items[0]['url'])) &&
      _social_challenge_has_permission('create idea in phase', $group)
    ) {
      $link = _social_challenge_add_idea_button($group, $this->currentUser);

      $item = [
        'label' => $link['#title'],
        'url' => $link['#url'],
      ];

      if (!empty($items)) {
        $items[0] = Link::fromTextAndUrl($items[0]['label'], $items[0]['url']);
        $items = array_merge([$item], $items);
      }
    }

    if (!empty($items)) {
      $parents = [0, 'attributes', 'class'];

      if (NestedArray::keyExists($items, $parents)) {
        $classes = NestedArray::getValue($items, $parents);
      }
      else {
        $classes = [];
      }

      $classes = array_unique(array_merge(
        $classes,
        ['btn-accent', 'btn-lg', 'btn-raised', 'waves-effect'],
      ));

      NestedArray::setValue($items, $parents, $classes);
    }

    return $items;
  }

}
