<?php

namespace Drupal\social_challenge\Plugin\MultipleContentBlock;

use Drupal\social_content_block\MultipleContentBlockBase;

/**
 * Provides a content block for challenges.
 *
 * @MultipleContentBlock(
 *   id = "challenge_content",
 *   label = @Translation("Challenge"),
 *   entity_type = "group",
 *   bundle = "challenge"
 * )
 */
class ChallengeContent extends MultipleContentBlockBase {

}
