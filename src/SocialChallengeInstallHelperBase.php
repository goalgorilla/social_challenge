<?php

namespace Drupal\social_challenge;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a base helper class, sets permissions, creates menu links.
 */
class SocialChallengeInstallHelperBase implements ContainerInjectionInterface {

  /**
   * Entity type manager.
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs SocialChallengeInstallHelper.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * Set default permissions.
   */
  public function setPermissions(): void {
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();

    /** @var \Drupal\user\Entity\Role $role */
    foreach ($roles as $role) {
      if ($role->id() === 'administrator') {
        continue;
      }

      $permissions = $this->getPermissions((string) $role->id());
      user_role_grant_permissions($role->id(), $permissions);
    }
  }

  /**
   * Get default permissions.
   */
  public function getPermissions(string $role): array {
    return [];
  }

}
