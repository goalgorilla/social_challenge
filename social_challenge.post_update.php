<?php

/**
 * @file
 * Contains post update hook implementations.
 */

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityStorageException;

/**
 * Fix search index "social_groups"; Trigger re-index entities.
 */
function social_challenge_post_update_0001_fix_search_indexes() {
  // If the search module is enabled trigger updating of the indexes as this
  // required for applying changes added with config overrides.
  try {
    if (\Drupal::moduleHandler()->moduleExists('social_search')) {
      social_search_resave_search_indexes(['social_groups']);
    }
  }
  catch (EntityStorageException $e) {
    \Drupal::logger('social_challenge')->info($e->getMessage());
  }

  // Safely trigger a search_api re-index.
  try {
    /** @var \Drupal\search_api\Entity\SearchApiConfigEntityStorage $storage */
    $storage = \Drupal::entityTypeManager()->getStorage("search_api_index");
  }
  // If the search_api is not available then we're done.
  catch (PluginNotFoundException $e) {
    return;
  }

  /** @var \Drupal\search_api\Entity\Index $index */
  $index = $storage->load('social_groups');

  if (!empty($index) && $index->status()) {
    $index->clear();
    $index->reindex();
  }
}
