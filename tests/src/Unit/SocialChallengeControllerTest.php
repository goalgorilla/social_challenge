<?php

namespace Drupal\Tests\social_challenge\Unit;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultForbidden;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Social challenge controller test.
 *
 * @coversDefaultClass \Drupal\social_challenge\Controller\SocialChallengeController
 * @group social_challenge
 */
class SocialChallengeControllerTest extends UnitTestCase {

  /**
   * The mocked entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;


  /**
   * The mocked account.
   *
   * @var \Drupal\Core\Session\AccountInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $account;

  /**
   * The mocked url generator.
   *
   * @var \Drupal\Core\Routing\UrlGeneratorInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $urlGenerator;

  /**
   * The mocked challenge wrapper.
   *
   * @var \Drupal\social_challenge\ChallengeWrapper|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $challengeWrapper;

  /**
   * The mocked social challenge controller.
   *
   * @var \Drupal\Tests\social_challenge\Unit\TestSocialChallengeController|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $controller;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();
    $this->entityTypeManager = $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface');
    $this->account = $this->createMock('Drupal\Core\Session\AccountInterface');
    $this->urlGenerator = $this->createMock('Drupal\Core\Routing\UrlGeneratorInterface');
    $this->challengeWrapper = $this->createMock('Drupal\social_challenge\ChallengeWrapper');

    $container = new ContainerBuilder();
    $container->set('entity_type.manager', $this->entityTypeManager);
    $container->set('current_user', $this->account);
    $container->set('url_generator', $this->urlGenerator);
    $container->set('social_challenge.wrapper', $this->challengeWrapper);
    \Drupal::setContainer($container);

    $this->controller = new TestSocialChallengeController($this->challengeWrapper);
  }

  /**
   * @covers ::redirectToGroupHomePage
   */
  public function testRedirectToGroupStreamPage() {
    $group_id = 1;
    $expected_group_stream_page_url = '/group/' . $group_id . '/stream';

    $this->urlGenerator
      ->expects($this->once())
      ->method('generateFromRoute')
      ->with('social_group.stream', ['group' => $group_id])
      ->willReturn($expected_group_stream_page_url);

    $group = $this->createMock('\Drupal\group\Entity\GroupInterface');
    $group
      ->expects($this->once())
      ->method('id')
      ->willReturn($group_id);

    $redirect_response = $this->controller->redirectToGroupHomePage($group);

    $this->assertInstanceOf(
      RedirectResponse::class,
      $redirect_response
    );

    $this->assertEquals(
      $expected_group_stream_page_url,
      $redirect_response->getTargetUrl()
    );
  }

  /**
   * @covers ::redirectToGroupHomePage
   */
  public function testRedirectNonMembersOfChallengeToAboutPage() {
    $group_id = 1;
    $expected_challenge_about_page_url = '/group/' . $group_id . '/about';

    $this->urlGenerator
      ->expects($this->once())
      ->method('generateFromRoute')
      ->with('view.group_information.page_group_about', ['group' => $group_id])
      ->willReturn($expected_challenge_about_page_url);

    $group = $this->createMock('\Drupal\group\Entity\GroupInterface');
    $group
      ->expects($this->once())
      ->method('id')
      ->willReturn($group_id);
    $group
      ->expects($this->any())
      ->method('bundle')
      ->willReturn('challenge');
    $group
      ->expects($this->any())
      ->method('getMember')
      ->willReturn(FALSE);

    $redirect_response = $this->controller->redirectToGroupHomePage($group);

    $this->assertInstanceOf(
      RedirectResponse::class,
      $redirect_response
    );

    $this->assertEquals(
      $expected_challenge_about_page_url,
      $redirect_response->getTargetUrl()
    );
  }

  /**
   * @covers ::content
   */
  public function testReturnChallengeAddListTheme() {
    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->any())
      ->method('loadMultiple')
      ->willReturn([]);

    $this->entityTypeManager
      ->expects($this->any())
      ->method('getStorage')
      ->willReturn($storage);

    $this->assertEquals(
      [
        '#theme' => 'challenge_add_list',
        '#content' => [],
      ],
      $this->controller->content()
    );
  }

  /**
   * @covers ::content
   */
  public function testRedirectToGroupAddForm() {
    $group_type_id = 'challenge';
    $expected_challenge_add_form_url = '/group/add/' . $group_type_id;

    $this->urlGenerator
      ->expects($this->once())
      ->method('generateFromRoute')
      ->with('entity.group.add_form', ['group_type' => $group_type_id])
      ->willReturn($expected_challenge_add_form_url);

    $group_type = $this->createMock('Drupal\group\Entity\GroupTypeInterface');
    $group_type
      ->expects($this->any())
      ->method('id')
      ->willReturn($group_type_id);

    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->any())
      ->method('loadMultiple')
      ->willReturn([$group_type]);

    $this->entityTypeManager
      ->expects($this->any())
      ->method('getStorage')
      ->willReturn($storage);

    $redirect_response = $this->controller->content();

    $this->assertInstanceOf(
      RedirectResponse::class,
      $redirect_response
    );

    $this->assertEquals(
      $expected_challenge_add_form_url,
      $redirect_response->getTargetUrl()
    );
  }

  /**
   * @covers ::access
   */
  public function testAccessForbidden() {
    $this->assertInstanceOf(
      AccessResultForbidden::class,
      $this->controller->access(
        $this->account
      )
    );
  }

  /**
   * @covers ::access
   */
  public function testAccessAllowed() {
    $account = $this->createMock('\Drupal\Core\Session\AccountInterface');
    $account
      ->expects($this->any())
      ->method('hasPermission')
      ->will($this->returnValueMap([
        ['create challenge group', TRUE],
      ]));

    $this->assertInstanceOf(
      AccessResultAllowed::class,
      $this->controller->access(
        $account
      )
    );
  }

}
