<?php

namespace Drupal\Tests\social_challenge\Unit;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\group\Entity\GroupInterface;
use Drupal\social_challenge\ChallengeWrapper;
use Symfony\Component\Validator\Exception\InvalidArgumentException;

/**
 * Challenge wrapper test.
 *
 * @coversDefaultClass \Drupal\social_challenge\ChallengeWrapper
 * @group social_challenge
 */
class ChallengeWrapperTest extends UnitTestCase {

  /**
   * The mocked group.
   *
   * @var \\Drupal\group\Entity\GroupInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $group;

  /**
   * The mocked phase one.
   *
   * @var \Drupal\social_challenge_phase\Entity\Phase|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $phase1;

  /**
   * The mocked phase two.
   *
   * @var \Drupal\social_challenge_phase\Entity\Phase|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $phase2;

  /**
   * The mocked entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $entityTypeManager;

  /**
   * The mocked account.
   *
   * @var \Drupal\Core\Session\AccountInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $account;

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $dateFormatter;

  /**
   * The challenge wrapper.
   *
   * @var \Drupal\social_challenge\ChallengeWrapper
   */
  protected $challengeWrapper;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->group = $this->createMock('\Drupal\group\Entity\GroupInterface');
    $this->group
      ->expects($this->any())
      ->method('bundle')
      ->willReturn('challenge');
    $this->phase1 = $this->createMock('Drupal\social_challenge_phase\Entity\Phase');
    $this->phase1
      ->expects($this->any())
      ->method('label')
      ->willReturn('dummy_phase_label1');
    $this->phase2 = $this->createMock('Drupal\social_challenge_phase\Entity\Phase');
    $this->phase2
      ->expects($this->any())
      ->method('label')
      ->willReturn('dummy_phase_label2');

    $this->entityTypeManager = $this->createMock('Drupal\Core\Entity\EntityTypeManagerInterface');
    $this->account = $this->createMock('Drupal\Core\Session\AccountInterface');
    $this->dateFormatter = $this->createMock('Drupal\Core\Datetime\DateFormatterInterface');

    $this->challengeWrapper = new ChallengeWrapper($this->entityTypeManager, $this->account, $this->dateFormatter);
    $container = new ContainerBuilder();
    \Drupal::setContainer($container);
  }

  /**
   * @covers ::setChallenge
   */
  public function testSetChallengeReturnsInvalidArgument() {
    $group = $this->createMock('\Drupal\group\Entity\GroupInterface');
    $group
      ->expects($this->any())
      ->method('bundle')
      ->willReturn('wrong_bundle');

    $this->expectException(InvalidArgumentException::class);
    $this->challengeWrapper->setChallenge($group);
  }

  /**
   * @covers ::setChallenge
   */
  public function testSetChallenge() {
    $this->assertInstanceOf(
      ChallengeWrapper::class,
      $this->challengeWrapper->setChallenge($this->group),
    );
  }

  /**
   * @covers ::getChallenge
   */
  public function testGetChallenge() {
    $this->challengeWrapper->setChallenge($this->group);

    $this->assertInstanceOf(
      GroupInterface::class,
      $this->challengeWrapper->getChallenge($this->group)
    );
  }

  /**
   * @covers ::getPhases
   */
  public function testGetPhasesRetunsEmpty() {
    $this->assertEmpty($this->challengeWrapper->getPhases());
  }

  /**
   * @covers ::getPhases
   */
  public function testGetPhasesReturnsPhases() {
    $this->challengeWrapper->setChallenge($this->group);

    $entity_reference_item = $this->createMock('Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem');
    $entity_reference_item
      ->expects($this->any())
      ->method('isEmpty')
      ->willReturn(FALSE);
    $entity_reference_item
      ->expects($this->any())
      ->method('getValue')
      ->willReturn([
        ['target_id' => 'dummy_phase_id'],
      ]);

    $this->group
      ->expects($this->any())
      ->method('get')
      ->with('field_challenge_phases')
      ->willReturn($entity_reference_item);

    $phase = $this->createMock('Drupal\social_challenge_phase\Entity\Phase');
    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->any())
      ->method('loadMultiple')
      ->willReturn([$phase]);

    $this->entityTypeManager
      ->expects($this->any())
      ->method('getStorage')
      ->willReturn($storage);

    $this->assertEquals(
      [$phase],
      $this->challengeWrapper->getPhases()
    );
  }

  /**
   * @covers ::getActivePhase
   */
  public function testGetActivePhaseReturnsNull() {
    $this->challengeWrapper->setChallenge($this->group);

    $entity_reference_item = $this->createMock('Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem');
    $entity_reference_item
      ->expects($this->any())
      ->method('isEmpty')
      ->willReturn(FALSE);
    $entity_reference_item
      ->expects($this->any())
      ->method('getValue')
      ->willReturn([
        ['target_id' => 'dummy_phase_id'],
      ]);

    $this->group
      ->expects($this->any())
      ->method('get')
      ->with('field_challenge_phases')
      ->willReturn($entity_reference_item);

    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->any())
      ->method('loadMultiple')
      ->willReturn([$this->phase1, $this->phase2]);

    $this->entityTypeManager
      ->expects($this->any())
      ->method('getStorage')
      ->willReturn($storage);

    $this->assertNull($this->challengeWrapper->getActivePhase());
  }

  /**
   * @covers ::getActivePhase
   */
  public function testGetActivePhaseReturnsTheActivePhase() {
    $this->challengeWrapper->setChallenge($this->group);

    $entity_reference_item = $this->createMock('Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem');
    $entity_reference_item
      ->expects($this->any())
      ->method('isEmpty')
      ->willReturn(FALSE);
    $entity_reference_item
      ->expects($this->any())
      ->method('getValue')
      ->willReturn([
        ['target_id' => 'dummy_phase_id'],
      ]);

    $this->group
      ->expects($this->any())
      ->method('get')
      ->with('field_challenge_phases')
      ->willReturn($entity_reference_item);

    $this->phase2
      ->expects($this->any())
      ->method('isActive')
      ->willReturn(TRUE);

    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->any())
      ->method('loadMultiple')
      ->willReturn([$this->phase1, $this->phase2]);

    $this->entityTypeManager
      ->expects($this->any())
      ->method('getStorage')
      ->willReturn($storage);

    $this->assertEquals(
      'dummy_phase_label2',
      $this->challengeWrapper->getActivePhase()->label()
    );
  }

  /**
   * @covers ::getActiveOrLastPhase
   */
  public function testGetActiveOrLastPhaseReturnsTheActivePhase() {
    $this->challengeWrapper->setChallenge($this->group);

    $entity_reference_item = $this->createMock('Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem');
    $entity_reference_item
      ->expects($this->any())
      ->method('isEmpty')
      ->willReturn(FALSE);
    $entity_reference_item
      ->expects($this->any())
      ->method('getValue')
      ->willReturn([
        ['target_id' => 'dummy_phase_id'],
      ]);

    $this->group
      ->expects($this->any())
      ->method('get')
      ->with('field_challenge_phases')
      ->willReturn($entity_reference_item);

    $this->phase2
      ->expects($this->any())
      ->method('isActive')
      ->willReturn(TRUE);

    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->any())
      ->method('loadMultiple')
      ->willReturn([$this->phase1, $this->phase2]);

    $this->entityTypeManager
      ->expects($this->any())
      ->method('getStorage')
      ->willReturn($storage);

    $this->assertEquals(
      'dummy_phase_label2',
      $this->challengeWrapper->getActiveOrLastPhase()->label()
    );
  }

  /**
   * @covers ::getActivePhaseName
   */
  public function testGetActivePhaseNameReturnsNull() {
    $this->challengeWrapper->setChallenge($this->group);

    $entity_reference_item = $this->createMock('Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem');
    $entity_reference_item
      ->expects($this->any())
      ->method('isEmpty')
      ->willReturn(FALSE);
    $entity_reference_item
      ->expects($this->any())
      ->method('getValue')
      ->willReturn([
        ['target_id' => 'dummy_phase_id'],
      ]);

    $this->group
      ->expects($this->any())
      ->method('get')
      ->with('field_challenge_phases')
      ->willReturn($entity_reference_item);

    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->any())
      ->method('loadMultiple')
      ->willReturn([$this->phase1, $this->phase2]);

    $this->entityTypeManager
      ->expects($this->any())
      ->method('getStorage')
      ->willReturn($storage);

    $this->assertNull($this->challengeWrapper->getActivePhaseName());
  }

  /**
   * @covers ::getActivePhaseName
   */
  public function testGetActivePhaseNameReturnsActivePhaseLabel() {
    $this->challengeWrapper->setChallenge($this->group);

    $entity_reference_item = $this->createMock('Drupal\Core\Field\Plugin\Field\FieldType\EntityReferenceItem');
    $entity_reference_item
      ->expects($this->any())
      ->method('isEmpty')
      ->willReturn(FALSE);
    $entity_reference_item
      ->expects($this->any())
      ->method('getValue')
      ->willReturn([
        ['target_id' => 'dummy_phase_id'],
      ]);

    $this->group
      ->expects($this->any())
      ->method('get')
      ->with('field_challenge_phases')
      ->willReturn($entity_reference_item);

    $this->phase2
      ->expects($this->any())
      ->method('isActive')
      ->willReturn(TRUE);

    $storage = $this->createMock('Drupal\Core\Entity\EntityStorageInterface');
    $storage
      ->expects($this->any())
      ->method('loadMultiple')
      ->willReturn([$this->phase1, $this->phase2]);

    $this->entityTypeManager
      ->expects($this->any())
      ->method('getStorage')
      ->willReturn($storage);

    $this->assertEquals(
      'dummy_phase_label2',
      $this->challengeWrapper->getActivePhaseName()
    );
  }

}
