<?php

namespace Drupal\Tests\social_challenge\Unit;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Tests\UnitTestCase;
use Drupal\social_challenge\SocialChallengeConfigOverride;
use Drupal\social_tagging\SocialTaggingService;

/**
 * Social challenge config override test.
 *
 * @coversDefaultClass \Drupal\social_challenge\SocialChallengeConfigOverride
 * @group social_challenge
 */
class SocialChallengeConfigOverrideTest extends UnitTestCase {

  use ProphecyTrait;
  /**
   * Default configuration.
   *
   * @var array[]
   */
  protected array $defaultConfig = [
    'views.view.search_all' => [
      'display' => [
        'default' => [
          'display_options' => [
            'row' => [
              'options' => [
                'view_modes' => [
                  'entity:group' => [
                    'test_group' => 'teaser',
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
    ],
    'views.view.search_groups' => [
      'display' => [
        'default' => [
          'display_options' => [
            'row' => [
              'options' => [
                'view_modes' => [
                  'entity:group' => [
                    'test_group' => 'teaser',
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
    ],
    'block.block.socialblue_profile_hero_block' => [
      'visibility' => [
        'request_path' => [
          'pages' => '/test-page',
        ],
      ],
    ],
    'block.block.socialblue_pagetitleblock_content' => [
      'visibility' => [
        'request_path' => [
          'pages' => '/test-page',
        ],
      ],
    ],
  ];

  /**
   * SocialChallengeConfigOverride.
   */
  protected SocialChallengeConfigOverride $configOverrides;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $module_handler = $this->prophesize(ModuleHandlerInterface::class)->reveal();
    $tagging_service = $this->prophesize(SocialTaggingService::class)->reveal();
    parent::setUp();
    $this->configOverrides = new SocialChallengeConfigOverride($this->getConfigFactoryStub($this->defaultConfig), $module_handler, $tagging_service);
  }

  /**
   * @covers ::loadOverrides
   */
  public function testLoadOverrides(): void {
    // There is no override if the config name is wrong.
    $names = [
      'test',
    ];

    $this->assertArrayEquals([], $this->configOverrides->loadOverrides($names));

    // Prepare expected arguments.
    $social_group_types = [
      'open_group',
      'closed_group',
      'public_group',
      'secret_group',
      'flexible_group',
      'sc',
      'cc',
    ];
    $expected_arguments = [
      'gid' => [
        'specify_validation' => TRUE,
        'validate' => [
          'type' => 'entity:group',
        ],
        'validate_options' => [
          'access' => '',
          'bundles' => array_combine($social_group_types, $social_group_types),
          'multiple' => FALSE,
          'operation' => 'view',
        ],
      ],
    ];

    // Assert that group_events view config gets overriden correctly.
    $names = [
      'views.view.group_events',
    ];
    $expected_filters_value = [];
    foreach ($social_group_types as $key) {
      $expected_filters_value["{$key}-group_node-event"] = "{$key}-group_node-event";
    }

    $this->assertArrayEquals(
      [
        'views.view.group_events' => [
          'display' => [
            'default' => [
              'display_options' => [
                'arguments' => $expected_arguments,
                'filters' => [
                  'type' => [
                    'value' => $expected_filters_value,
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    // Assert that group_members view config gets overriden correctly.
    $names = [
      'views.view.group_members',
    ];
    $expected_filters_value = [];
    foreach ($social_group_types as $key) {
      $expected_filters_value["{$key}-group_membership"] = "{$key}-group_membership";
    }

    $this->assertArrayEquals(
      [
        'views.view.group_members' => [
          'display' => [
            'default' => [
              'display_options' => [
                'arguments' => $expected_arguments,
                'filters' => [
                  'type' => [
                    'value' => $expected_filters_value,
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    // Assert that group_topics view config gets overriden correctly.
    $names = [
      'views.view.group_topics',
    ];
    $expected_filters_value = [];
    foreach ($social_group_types as $key) {
      $expected_filters_value["{$key}-group_node-topic"] = "{$key}-group_node-topic";
    }

    $this->assertArrayEquals(
      [
        'views.view.group_topics' => [
          'display' => [
            'default' => [
              'display_options' => [
                'arguments' => $expected_arguments,
                'filters' => [
                  'type' => [
                    'value' => $expected_filters_value,
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    // Assert that group_manage_members view config gets overriden correctly.
    $names = [
      'views.view.group_manage_members',
    ];

    $this->assertArrayEquals(
      [
        'views.view.group_manage_members' => [
          'display' => [
            'default' => [
              'display_options' => [
                'filters' => [
                  'type' => [
                    'value' => [
                      'challenge-group_membership' => 'challenge-group_membership',
                      'cc-group_membership' => 'cc-group_membership',
                      'sc-group_membership' => 'sc-group_membership',
                    ],
                  ],
                ],
              ],
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    // Assert view mode for search_all and search_groups views the config gets
    // overriden correctly.
    $names = [
      'views.view.search_all',
      'views.view.search_groups',
    ];
    foreach ($names as $name) {
      $expected_bundles = [
        'test_group' => 'teaser',
        'challenge' => 'teaser',
      ];

      $overridden_config = $this->configOverrides->loadOverrides([$name]);

      $this->assertArrayEquals(
        [
          'entity:group' => $expected_bundles,
        ],
        $overridden_config[$name]['display']['default']['display_options']['row']['options']['view_modes']
      );
    }

    // Assert page visibility for block socialblue_profile_hero_block.
    $names = [
      'block.block.socialblue_profile_hero_block',
    ];
    $expected_pages = "/test-page\r\n/user/*/challenges\r\n/user/*/ideas\r\n/user/*/discussions";

    $this->assertArrayEquals(
      [
        'block.block.socialblue_profile_hero_block' => [
          'visibility' => [
            'request_path' => [
              'pages' => $expected_pages,
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    // Assert page visibility for block socialblue_pagetitleblock_content.
    $names = [
      'block.block.socialblue_pagetitleblock_content',
    ];
    $expected_pages = "/test-page\r\n/user/*/challenges\r\n/user/*/ideas\r\n/user/*/discussions\r\n/group/*/moderators\r\n/group/*/ideas";

    $this->assertArrayEquals(
      [
        'block.block.socialblue_pagetitleblock_content' => [
          'visibility' => [
            'request_path' => [
              'pages' => $expected_pages,
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );

    // Assert formatter for social_idea comment field.
    $names = [
      'core.entity_view_display.node.social_idea.default',
    ];

    $this->assertArrayEquals(
      [
        'core.entity_view_display.node.social_idea.default' => [
          'content' => [
            'field_social_idea_comments' => [
              'type' => 'comment_idea',
            ],
          ],
        ],
      ],
      $this->configOverrides->loadOverrides($names)
    );
  }

}
