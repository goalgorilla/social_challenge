<?php

namespace Drupal\Tests\social_challenge\Unit;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\social_challenge\Controller\SocialChallengeController;

/**
 * Override certain methods for testing purposes.
 */
class TestSocialChallengeController extends SocialChallengeController {

  /**
   * Override parent::content().
   *
   * For testing purposes ONLY.
   */
  public function content() {
    $content = [];
    $bundles = [
      'challenge',
    ];

    foreach ($this->entityTypeManager()->getStorage('group_type')->loadMultiple() as $type) {
      if (in_array($type->id(), $bundles)) {
        $content[$type->id()] = $type;
      }
    }
    arsort($content);
    if (count($content) == 1) {
      $type = array_shift($content);
      return $this->redirect('entity.group.add_form', ['group_type' => $type->id()]);
    }

    return [
      '#theme' => 'challenge_add_list',
      '#content' => $content,
    ];
  }

  /**
   * Override parent::access().
   *
   * For testing purposes ONLY.
   */
  public function access(AccountInterface $account) {
    $bundles = [
      'challenge',
    ];

    foreach ($bundles as $bundle) {
      if ($account->hasPermission("create {$bundle} group")) {
        return AccessResult::allowed();
      }
    }

    return AccessResult::forbidden();
  }

}
