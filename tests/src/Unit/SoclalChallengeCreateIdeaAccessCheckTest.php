<?php

namespace Drupal\Tests\social_challenge\Unit;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Access\AccessResultNeutral;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;
use Drupal\social_challenge\Access\SocialChallengeCreateIdeaAccessCheck;

/**
 * Social challenge create idea access test.
 *
 * @coversDefaultClass \Drupal\social_challenge\Access\SoclalChallengeCreateIdeaAccessCheck
 * @group social_challenge
 */
class SoclalChallengeCreateIdeaAccessCheckTest extends UnitTestCase {

  /**
   * The mocked challenge wrapper.
   *
   * @var \Drupal\social_challenge\ChallengeWrapper|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $challengeWrapper;

  /**
   * The social challenge phase permissions service.
   *
   * @var \Drupal\social_challenge_phase\Service\SocialChallengePhasePermissions|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $challengePhasePermissions;


  /**
   * The soclal challenge create idea access check.
   *
   * @var \Drupal\social_challenge\Access\SocialChallengeCreateIdeaAccessCheck
   */
  protected $soclalChallengeCreateIdeaAccessCheck;

  /**
   * {@inheritdoc}
   */
  protected function setUp() {
    parent::setUp();

    $this->challengeWrapper = $this->createMock('Drupal\social_challenge\ChallengeWrapper');
    $this->challengePhasePermissions = $this->createMock('Drupal\social_challenge_phase\Service\SocialChallengePhasePermissions');

    $this->soclalChallengeCreateIdeaAccessCheck = new SocialChallengeCreateIdeaAccessCheck($this->challengeWrapper, $this->challengePhasePermissions);
    $container = new ContainerBuilder();
    \Drupal::setContainer($container);
  }

  /**
   * @covers ::access
   */
  public function testAccessReturnsAllowedAccess() {
    $route = $this->createMock('Symfony\Component\Routing\Route');
    $account = $this->createMock('Drupal\Core\Session\AccountInterface');
    $group = $this->createMock('Drupal\group\Entity\GroupInterface');
    $plugin_id = 'dummy_plugin_id';

    $this->assertInstanceOf(
      AccessResultAllowed::class,
      $this->soclalChallengeCreateIdeaAccessCheck->access(
        $route,
        $account,
        $group,
        $plugin_id
      )
    );
  }

  /**
   * @covers ::access
   */
  public function testAccessReturnsNeutralAccessWithoutNeedsAccess() {
    $route = $this->createMock('Symfony\Component\Routing\Route');

    $account = $this->createMock('Drupal\Core\Session\AccountInterface');

    $group = $this->createMock('Drupal\group\Entity\GroupInterface');
    $group
      ->expects($this->once())
      ->method('bundle')
      ->willReturn('challenge');
    $group
      ->expects($this->once())
      ->method('hasPermission')
      ->willReturn(TRUE);

    $plugin_id = 'dummy_plugin_id';

    $this->assertInstanceOf(
      AccessResultNeutral::class,
      $this->soclalChallengeCreateIdeaAccessCheck->access(
        $route,
        $account,
        $group,
        $plugin_id
      )
    );
  }

  /**
   * @covers ::access
   */
  public function testAccessReturnsAllowedAccessWithNeedsAccess() {
    $route = $this->createMock('Symfony\Component\Routing\Route');
    $route
      ->expects($this->once())
      ->method('getRequirement')
      ->with('_social_challenge_create_idea_access')
      ->willReturn(FALSE);

    $account = $this->createMock('Drupal\Core\Session\AccountInterface');

    $group = $this->createMock('Drupal\group\Entity\GroupInterface');
    $group
      ->expects($this->once())
      ->method('bundle')
      ->willReturn('challenge');
    $group
      ->expects($this->once())
      ->method('hasPermission')
      ->willReturn(FALSE);

    $plugin_id = 'dummy_plugin_id';

    $this->assertInstanceOf(
      AccessResultAllowed::class,
      $this->soclalChallengeCreateIdeaAccessCheck->access(
        $route,
        $account,
        $group,
        $plugin_id
      )
    );
  }

  /**
   * @covers ::access
   */
  public function testAccessReturnsNeutralAccessWithCreateIdeaPermission() {
    $route = $this->createMock('Symfony\Component\Routing\Route');

    $account = $this->createMock('Drupal\Core\Session\AccountInterface');

    $group = $this->createMock('Drupal\group\Entity\GroupInterface');
    $group
      ->expects($this->once())
      ->method('bundle')
      ->willReturn('challenge');
    $group
      ->expects($this->once())
      ->method('hasPermission')
      ->willReturn(FALSE);

    $plugin_id = 'dummy_plugin_id';

    $phase = $this->createMock('Drupal\social_challenge_phase\Entity\Phase');

    $this->challengeWrapper
      ->expects($this->once())
      ->method('getActivePhase')
      ->willReturn($phase);

    $this->challengePhasePermissions
      ->expects($this->once())
      ->method('hasPermissionValue')
      ->with('create idea in phase', $phase)
      ->willReturn(TRUE);

    $this->assertInstanceOf(
      AccessResultNeutral::class,
      $this->soclalChallengeCreateIdeaAccessCheck->access(
        $route,
        $account,
        $group,
        $plugin_id
      )
    );
  }

  /**
   * @covers ::access
   */
  public function testAccessReturnsAllowedAccessWithCreateIdeaPermission() {
    $route = $this->createMock('Symfony\Component\Routing\Route');
    $route
      ->expects($this->once())
      ->method('getRequirement')
      ->with('_social_challenge_create_idea_access')
      ->willReturn(FALSE);

    $account = $this->createMock('Drupal\Core\Session\AccountInterface');

    $group = $this->createMock('Drupal\group\Entity\GroupInterface');
    $group
      ->expects($this->once())
      ->method('bundle')
      ->willReturn('challenge');
    $group
      ->expects($this->once())
      ->method('hasPermission')
      ->willReturn(FALSE);

    $plugin_id = 'dummy_plugin_id';

    $phase = $this->createMock('Drupal\social_challenge_phase\Entity\Phase');

    $this->challengeWrapper
      ->expects($this->once())
      ->method('getActivePhase')
      ->willReturn($phase);

    $this->challengePhasePermissions
      ->expects($this->once())
      ->method('hasPermissionValue')
      ->with('create idea in phase', $phase)
      ->willReturn(FALSE);

    $this->assertInstanceOf(
      AccessResultAllowed::class,
      $this->soclalChallengeCreateIdeaAccessCheck->access(
        $route,
        $account,
        $group,
        $plugin_id
      )
    );
  }

}
